
const ActorApi = require('actor-api');


class MeasureOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.reuse = false;
    this.page = null;
    this.headless = true;
    this.slowMo = 0;
    this.useProxy = true;
  }
  
  *data() {
    this.reuse = this.getTestDataBoolean('reuse', this.reuse);
    this.headless = this.getTestDataBoolean('headless', this.headless);
    this.slowMo = this.getTestDataNumber('slowMo', this.slowMo);
    this.useProxy = this.getTestDataBoolean('useProxy', this.useProxy);
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer', {
      reuse: this.reuse,
      connectionOptions: {
        launchOptions: {
          headless: this.headless,
          slowMo: this.slowMo
        },
        proxy: this.useProxy
      }
    });
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    yield page.waitForFunction(() => {const button = document.getElementById("mainStartTest"); return button ? button.innerText === "Starta mätningen!" : false;});
    yield page.click('#mainStartTest');
    yield page.waitForFunction(() => {return document.getElementById("mainStartTest").innerText === "Mät igen";});
  }
  
  *exit() {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = MeasureOrig;
