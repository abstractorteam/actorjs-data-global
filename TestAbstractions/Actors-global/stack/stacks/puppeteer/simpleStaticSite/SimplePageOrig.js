
const ActorApi = require('actor-api');


class SimplePageOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.page = null;
    this.headless = true;
    this.slowMo = 0;
  }

  *data() {
    this.headless = this.getTestDataBoolean('headless', this.headless);
    this.slowMo = this.getTestDataNumber('slowMo', this.slowMo);
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer', {
      reuse: true,
      connectionOptions: {
        launchOptions: {
          headless: this.headless,
          slowMo: this.slowMo,
          timeout: 3000
        },
        proxy: true
      }
    });
    this.delay(10000);
  }
  
  *run() {
  }
  
  *exit() {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = SimplePageOrig;
