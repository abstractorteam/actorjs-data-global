
'use strict';

const HttpApi = require('http-stack-api');


class HttpGet200OkSiteResponse extends HttpApi.Response {
  constructor(site, contentType, contentSize) {
    super(HttpApi.Version.HTTP_1_1, HttpApi.StatusCode.OK, HttpApi.ReasonPhrase.OK);
    this.addHeader(HttpApi.Header.CONTENT_TYPE, contentType);
    this.addHeader(HttpApi.Header.CONTENT_LENGTH, contentSize);
    this.addBody(site);
  }
}

module.exports = HttpGet200OkSiteResponse;
