
const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');
const HttpGet200OkSiteResponse = require('./msg/HttpGet200OkSiteResponse');


class HttpStaticTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.site = `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>title</title>
  </head>
  <body>
    <p>
      MAMMA
    </p>
  </body>
</html>`;
  }

  *data() {
  }
    
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
    
    this.httpConnection.send(new HttpGet200OkSiteResponse(this.site, 'text/html', this.site.length));
  }
  
  *exit() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = HttpStaticTerm;
