
const ActorApi = require('actor-api');


class SocketReceiveLineProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.socketConnectionServer = null;
    this.socketConnectionClient = null;
  }
  
  *initServer() {
    this.socketConnectionServer = this.createServer('socket');
  }
  
  *initClient() {
    this.socketConnectionClient = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnectionServer.accept();
    let request = yield this.socketConnectionServer.receiveLine();
    VERIFY_VALUE('Hello World', request);
    
    this.socketConnectionClient.sendLine(request);
    let response = yield this.socketConnectionClient.receiveLine();
    VERIFY_VALUE('Hello Europe', response);
    
    this.socketConnectionServer.sendLine(response);
  }
  
  *exit() {
    this.socketConnectionClient.close();
    this.socketConnectionServer.close();
  }
}

module.exports = SocketReceiveLineProxy;
