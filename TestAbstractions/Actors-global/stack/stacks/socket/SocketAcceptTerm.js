
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketAcceptTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.networkType = StackApi.NetworkType.TCP;
    this.acceptBeforeConnect = false;
  }
  
  *data() {
    this.networkType = this.getTestDataNumber('network_type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
    this.acceptBeforeConnect = this.getTestDataBoolean('accept_before_connect', false);
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: this.networkType
    });
  }
    
  *run() {
    if(this.acceptBeforeConnect) {
      this.setSharedData('ACCEPTED');
    }
    else {
      this.waitForSharedData('CONNECTED');
    }
    this.socketConnection.accept();
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = SocketAcceptTerm;
