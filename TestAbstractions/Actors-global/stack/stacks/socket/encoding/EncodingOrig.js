
const ActorApi = require('actor-api');


class EncodingOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
    this.encoding = '';
    this.text = '';
  }
  
  *data() {
    this.encoding = this.getTestDataString('encoding');
    this.text = this.getTestDataString('text');
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnection.sendLine(this.text);
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = EncodingOrig;
