
const ActorApi = require('actor-api');


class SocketObjectEmptySut extends ActorApi.ActorSut {
  constructor() {
    super();
  }
    
  *run() {
    this.logWarning('RUN');
  }
}

module.exports = SocketObjectEmptySut;
