
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketObjectSut extends ActorApi.ActorSut {
  constructor() {
    super();
    this.socketServerConnection = null;
    this.socketClientConnection = null;
    this.networkTypeServer = StackApi.NetworkType.TCP;
    this.networkTypeClient = StackApi.NetworkType.TCP;
  }
  
  *data() {
    this.networkTypeServer = this.getTestDataNumber('network-type-server', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
    this.networkTypeClient = this.getTestDataNumber('network-type-client', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
  }
  
  *initServer() {
    this.socketServerConnection = this.createServer('socket', {
      networkType: this.networkTypeServer
    });
  }
  
  *initClient() {
    this.socketClientConnection = this.createConnection('socket', {
      networkType: this.networkTypeClient
    });
  }
  
  *run() {
    this.socketServerConnection.accept();
    const cmd = this.socketServerConnection.receiveObject();
    
    VERIFY_VALUE('login', cmd.cmd);
    VERIFY_VALUE('Mercedes-Benz', cmd.name);
    
    this.socketClientConnection.sendObject(cmd);
    
    const result = this.socketClientConnection.receiveObject();
    VERIFY_VALUE('ok', result.login);
    
    this.socketServerConnection.sendObject(result);
  }
  
  *exit() {
    this.closeConnection(this.socketClientConnection);
    this.closeConnection(this.socketServerConnection);
  }
}

module.exports = SocketObjectSut;
