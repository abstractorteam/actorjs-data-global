
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class DefaultAddressesOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
    this.networkType = StackApi.NetworkType.TCP;
  }
  
  *data() {
    this.networkType = this.getTestDataNumber('network-type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP,
      mc: StackApi.NetworkType.MC
    });
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket', {
      networkType: this.networkType
    });
  }
  
  *run() {
    this.socketConnection.sendLine('HELLO');
    
    const request = this.socketConnection.receiveLine();
    VERIFY_VALUE('WORLD', request);
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = DefaultAddressesOrig;
