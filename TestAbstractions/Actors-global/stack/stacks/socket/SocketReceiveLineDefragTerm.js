
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketReceiveLineDefragTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.serverAcceptDelay = [[0]];
    this.serverMsg = [['']];
    this.serverSendDelays = [[-1]];
    this.serverReceiveDelays = [[-1]];
    this.serverNetworkType = StackApi.NetworkType.TCP;
    this.expectedClientMsg = [['']];
  }
  
  *data() {
    this.serverNetworkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
    this.serverAcceptDelay = this.getTestDataNumber('acceptDelay', -1);
    
    this.serverMsg = this.getTestDataArrayStrings('serverMsg');
    this.serverSendDelays = this.getTestDataArrayNumbers('serverSendDelays', () => {
      let arrays = [];
      this.serverMsg.forEach((array, index) => {
        arrays.push(Array(this.serverMsg[index].length).fill(-1));
      });
      return arrays;
    });    
    this.expectedClientMsg = this.getVerificationData('clientMsg');
    this.serverReceiveDelays = this.getTestDataArrayNumbers('serverReceiveDelays', () => {
      let arrays = [];
      this.expectedClientMsg.forEach((array, index) => {
        arrays.push(Array(this.expectedClientMsg[index].length).fill(-1));
      });
      return arrays;
    });
  }
 
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: this.serverNetworkType
    });
  }
  
  *run() {
    this.delay(this.serverAcceptDelay);
    this.socketConnection.accept();
    
    let iter = 0;
    let sendDone = false;
    let receiveDone = false;
    while(!sendDone || !receiveDone) {
      if(iter < this.expectedClientMsg.length) {
        for(let e = 0; e < this.expectedClientMsg[iter].length; ++e) {
          this.delay(this.serverReceiveDelays[iter][e]);
          let request = this.socketConnection.receiveLine();
          VERIFY_MANDATORY('clientMsg', request, iter, e);
        }
      }
      else {
        receiveDone = true;
      }
      if(iter < this.serverMsg.length) {
        for(let j = 0; j < this.serverMsg[iter].length; ++j) {
          this.delay(this.serverSendDelays[iter][j]);
          this.socketConnection.send(this.serverMsg[iter][j]);
        }  
      }
      else {
        sendDone = true;
      }
      ++iter;
    }
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = SocketReceiveLineDefragTerm;
