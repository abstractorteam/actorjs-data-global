
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class DefaultAddressesTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.networkType = StackApi.NetworkType.TCP;
  }
  
  *data() {
    this.networkType = this.getTestDataNumber('network-type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP,
      mc: StackApi.NetworkType.MC
    });
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
    const request = this.socketConnection.receiveLine();
    VERIFY_VALUE('HELLO', request);
    
    this.socketConnection.sendLine('WORLD');
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = DefaultAddressesTerm;
