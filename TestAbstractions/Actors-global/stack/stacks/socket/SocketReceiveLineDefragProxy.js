
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketReceiveLineDefragProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.socketServerConnection = null;
    this.serverAcceptDelay = [[0]];
    this.serverReceiveDelays = [[-1]];
    this.serverNetworkType = StackApi.NetworkType.TCP;
    this.expectedClientMsg = [['']];
    
    this.socketClientConnection = null;
    this.clientNetworkType = StackApi.NetworkType.TCP;
    this.expectedServerMsg = [['']];
  }
  
  *data() {
    this.serverNetworkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
    this.serverAcceptDelay = this.getTestDataNumber('acceptDelay', -1);
    this.expectedClientMsg = this.getVerificationData('clientMsg');
    this.serverReceiveDelays = this.getTestDataArrayNumbers('serverReceiveDelays', () => {
      let arrays = [];
      this.expectedClientMsg.forEach((array, index) => {
        arrays.push(Array(this.expectedClientMsg[index].length).fill(-1));
      });
      return arrays;
    });

    this.clientNetworkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
    this.expectedServerMsg = this.getVerificationData('serverMsg');
    this.clientReceiveDelays = this.getTestDataNumbers('clientReceiveDelays', () => {
      let arrays = [];
      this.expectedServerMsg.forEach((array, index) => {
        arrays.push(Array(this.expectedServerMsg[index].length).fill(-1));
      });
      return arrays;
    });
  }
  
  *initServer() {
    this.socketServerConnection = this.createServer('socket', {
      networkType: this.serverNetworkType
    });
  }
  
  *initClient() {
    this.socketClientConnection = this.createConnection('socket', {
      networkType: this.clientNetworkType
    });
  }
  
  *run() {
    this.delay(this.serverAcceptDelay);
    this.socketServerConnection.accept();
    
    let iter = 0;
    let sendDone = false;
    let receiveDone = false;
    while(!sendDone || !receiveDone) {
      if(iter < this.expectedClientMsg.length) {
         for(let e = 0; e < this.expectedClientMsg[iter].length; ++e) {
           this.delay(this.serverReceiveDelays[iter][e]);
           let request = this.socketServerConnection.receiveLine();
           VERIFY_MANDATORY('clientMsg', request, iter, e);
           this.socketClientConnection.sendLine(request);
         }
      }
      else {
        receiveDone = true;
      }
      if(iter < this.expectedServerMsg.length) {
         for(let e = 0; e < this.expectedServerMsg[iter].length; ++e) {
           this.delay(this.clientReceiveDelays[iter][e]);
           let request = this.socketClientConnection.receiveLine();
           VERIFY_MANDATORY('serverMsg', request, iter, e);
           this.socketServerConnection.sendLine(request);
         }
      }
      else {
        sendDone = true;
      }
      ++iter;
    }
  }
  
  *exit() {
    this.closeConnection(this.socketClientConnection);
    this.closeConnection(this.socketServerConnection);
  }
}


module.exports = SocketReceiveLineDefragProxy;
