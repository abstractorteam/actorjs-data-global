
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketLogOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
    this.command = '';
    this.networkType = StackApi.NetworkType.TCP;
  }
  
  *data() {
    this.command = this.getTestDataString('command');
    this.testdata = this._getCommandTestData();
    this.networkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket', {
      networkType: this.networkType
    });
  }
  
  *run() {
    switch(this.command) {
      case 'sendLine':
        this.socketConnection.sendLine(this.testdata);
        break;
      case 'sendObject':
        this.socketConnection.sendObject(this.testdata);
        break;
    }
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
  
  _getCommandTestData() {
    switch(this.command) {
      case 'sendLine':
        return this.getTestDataString('data');
      case 'sendObject':
        return this.getTestDataObject('data'); 
    }
  }
}

module.exports = SocketLogOrig;
