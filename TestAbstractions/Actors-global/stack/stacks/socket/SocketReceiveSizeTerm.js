
const ActorApi = require('actor-api');


class SocketReceiveSizeTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.serverMsg = '';
  }
  
  *data() {
    this.serverMsg = this.getTestDataStrings('serverMsg', ['default server message.']);
    this.serverSendDelays = this.getTestDataNumbers('serverSendDelays', () => {
      let arrays = [];
      arrays.fill(-1, 0, this.serverMsg.length);
      return arrays;
    });
  }
  
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
 
    let responseSize = this.socketConnection.receiveSize(4);
    let size = responseSize.readUInt32BE(0);
    if(0 !== size) {
      let responseData = this.socketConnection.receiveSize(size);
      VERIFY_MANDATORY('clientMsg', responseData);
    }
    if(0 == this.serverMsg.length) {
      let buffer = Buffer.allocUnsafe(4);
      buffer.writeUInt32BE(0, 0); 
      this.socketConnection.send(buffer);
    }
    else {
      let length = this.serverMsg.reduce((sum, currentValue) => {
        return sum + currentValue.length;
      }, 0);
      let buffer = Buffer.allocUnsafe(4 + this.serverMsg[0].length);
      buffer.writeUInt32BE(length, 0);
      buffer.write(this.serverMsg[0], 4);
      this.delay(this.serverSendDelays[0]);
      this.socketConnection.send(buffer);
      for(let i = 1; i < this.serverMsg.length; ++i) {
        let buffer = Buffer.allocUnsafe(this.serverMsg[i].length);
        buffer.write(this.serverMsg[i], 0);
        this.delay(this.serverSendDelays[i]);
        this.socketConnection.send(buffer);
      }
    }
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = SocketReceiveSizeTerm;
