
const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');
const HttpMsgGetRequest = require('./msg/HttpMsgGetRequest');


class HttpGetOkClientProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.httpClientConnection = null;
    this.requistUri = '';
    this.sendDelay = -1;
  }
  
  *data() {
    this.requistUri = this.getTestDataString('Request-URI', '/demo');
    this.sendDelay = this.getTestDataNumber('send-delay', this.sendDelay);
  }
  
  *initClient() {
    this.httpClientConnection = this.createConnection('http');
  }
  
  *run() {
    this.delay(this.sendDelay);
    this.httpClientConnection.send(new HttpMsgGetRequest(this.requistUri));
    
    const response = this.httpClientConnection.receive();
    VERIFY_VALUE(HttpApi.StatusCode.OK, response.statusCode, ' HTTP response line status code:');
    VERIFY_CONTENT_OPTIONAL('content-name', response.getHeaderNumber(HttpApi.Header.CONTENT_LENGTH), response.getBody());
  }
  
  *exit() {
    this.closeConnection(this.httpClientConnection);
  }
}

module.exports = HttpGetOkClientProxy;
