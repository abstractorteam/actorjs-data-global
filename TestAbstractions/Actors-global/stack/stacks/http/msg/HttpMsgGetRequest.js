
'use strict';

const HttpApi = require('http-stack-api');


class HttpMsgGetRequest extends HttpApi.Request {
  constructor(requestUri) {
    super(HttpApi.Method.GET, requestUri, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, 'www.example.com');
    this.addHeader(HttpApi.Header.CONTENT_LENGTH, 0);
  }
}

module.exports = HttpMsgGetRequest;
