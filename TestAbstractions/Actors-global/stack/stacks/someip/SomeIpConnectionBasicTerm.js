
const ActorApi = require('actor-api');


class SomeIpConnectionBasicTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.someipConnection = null;
  }
  
  *initServer() {
    this.someipConnection = this.createServer('someip');
  }
  
  *run() {
    this.someipConnection.accept();
    const request = this.someipConnection.receive();
    const payload = JSON.parse(request.payload);
    VERIFY_VALUE('testname', payload.name);
    VERIFY_VALUE(1, payload.id);
  }
  
  *exit() {
    this.closeConnection(this.someipConnection);
  }
}

module.exports = SomeIpConnectionBasicTerm;
