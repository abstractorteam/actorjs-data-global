
'use strict';

const SomeipApi = require('someip-stack-api');


class SomeipMsgGetDataRequest extends SomeipApi.SomeipMsgRequest {
  constructor(sessionId, payload) {
    super(undefined, 0x0001, undefined, sessionId, undefined, undefined, payload);
  }
}

module.exports = SomeipMsgGetDataRequest;
