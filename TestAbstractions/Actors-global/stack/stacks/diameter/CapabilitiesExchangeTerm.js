
const ActorApi = require('actor-api');
const DiameterApi = require('diameter-stack-api');
const CapabilitiesExchangeAnswer = require('./msg/CapabilitiesExchangeAnswer');


class CapabilitiesExchangeTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.diameterConnection = null;
  }

  *data() {
  }
    
  *initServer() {
    this.diameterConnection = this.createServer('diameter');
  }
  
  *run() {
    this.diameterConnection.accept();
    
    const request = this.diameterConnection.receive();
    
    this.diameterConnection.send(new CapabilitiesExchangeAnswer(DiameterApi.DiameterConstResultCodes.DIAMETER_SUCCESS, 'hss.actor.net', 'actor.net'));
  }
  
  *exit() {
    this.closeConnection(this.diameterConnection);
  }
}

module.exports = CapabilitiesExchangeTerm;
