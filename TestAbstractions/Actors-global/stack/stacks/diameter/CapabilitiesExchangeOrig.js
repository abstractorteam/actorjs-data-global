
const ActorApi = require('actor-api');
const CapabilitiesExchangeRequest = require('./msg/CapabilitiesExchangeRequest');


class CapabilitiesExchangeOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.diameterConnection = null;
  }

  *data() {
  }
  
  *initClient() {
    this.diameterConnection = this.createConnection('diameter');
  }
  
  *run() {
    this.diameterConnection.send(new CapabilitiesExchangeRequest('my_service.actor.net', 'actor.net'));
    
    const answer = this.diameterConnection.receive();
  }
  
  *exit() {
    this.closeConnection(this.diameterConnection);
  }
}

module.exports = CapabilitiesExchangeOrig;
