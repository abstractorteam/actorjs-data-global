
const DiameterApi = require('diameter-stack-api');


class CapabilitiesExchangeAnswer extends DiameterApi.msg.CapabilitiesExchangeAnswer {
  constructor(resultCode, originHost, originRealm, hostIPAddress, vendorId, productName) {
    super(16777216);
    this.addAvpResultCode(resultCode);
    this.addAvpOriginHost(originHost);
    this.addAvpOriginRealm(originRealm);
    this.addAvpHostIPAddress(hostIPAddress);
    this.addAvpVendorId(vendorId);
    this.addAvpProductName(productName);
  }
}

module.exports = CapabilitiesExchangeAnswer;
