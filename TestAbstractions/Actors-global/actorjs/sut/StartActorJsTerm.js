
const ActorApi = require('actor-api');
const Path = require('path');
const ChildProcess = require('child_process');


class StartActorJsTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.localPath = '../Test/ActorJs';
    this.localGitPath = Path.normalize(`${this.localPath}/actorjs`);
    this.httpPort = 9020;
    this.httpHost = 'localhost';
    this.wsPort = 9021;
    this.wsHost = 'localhost';
    this.parentPort = 9022;
    this.parentHost = 'localhost';
    this.httpsPort = 9023;
    this.httpsHost = 'localhost';
  }
  
  *data() {
    const dstAddress = this.getDstAddress(0);
    this.httpPort = this.getTestDataNumber('http-port', dstAddress.port);
    this.httpHost = this.getTestDataString('http-host', dstAddress.host);
    this.wsPort = this.getTestDataNumber('ws-port', this.wsPort);
    this.wsHost = this.getTestDataString('ws-host', dstAddress.host);
    this.getTestDataNumber('actor-js-port', this.parentPort);
    this.getTestDataString('actor-js-host', this.parentHost);
    this.httpsPort = this.getTestDataNumber('https-port', this.httpsPort);
    this.httpsHost = this.getTestDataString('https-host', this.httpsHost);
  }
  
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      port: this.parentPort,
      host: this.parentHost
    });
  }
  
  *run() {
    let childProcess = null;
    let handleExit = true;
    this.callback((cb) => {
      this.logGui(`aj --port ${this.httpPort} --host ${this.httpHost} --sport ${this.httpsPort} --shost ${this.httpsHost} --wsp ${this.wsPort} --wsh ${this.wsHost} --pp ${this.parentPort} --ph ${this.parentHost}`);
      childProcess = ChildProcess.exec(`aj --port ${this.httpPort} --host ${this.httpHost} --sport ${this.httpsPort} --shost ${this.httpsHost} --wsp ${this.wsPort} --wsh ${this.wsHost} --pp ${this.parentPort} --ph ${this.parentHost}`, {
        cwd: this.localGitPath
      }, (err) => {
        if(handleExit) {
          cb(err);
        }
      });
      childProcess.stdout.on('data', (data) => {
        const logs = data.split('\n');
        logs.forEach((log) => {
          if('' !== log.trim()) {
            this.logIp('stdout:' + log);
          }
        });
      });
    
      childProcess.stderr.on('data', (data) => {
        const logs = data.split('\n');
        logs.forEach((log) => {
          if('' !== log.trim()) {
            this.logIp('stderr:' + log);
          }
        });
      });
      handleExit = false;
      cb();
    });
    
    this.setSharedExecutionData('ActorJsShellProcess', childProcess);
    this.socketConnection.accept();
    const response = this.socketConnection.receiveObject();
    VERIFY_VALUE('success', response.result);
    
    this.logGui('ActorJs started.');
    this.setSharedExecutionData('ActorJsGulpPid', response.pid);
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = StartActorJsTerm;
