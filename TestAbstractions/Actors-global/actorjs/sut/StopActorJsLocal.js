
const ActorApi = require('actor-api');
const Process = require('process');


class StopActorJsLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *data() {
  }
  
  *run() {
    const childProcess = this.getSharedExecutionData('ActorJsShellProcess');
    const gulpPid = this.getSharedExecutionData('ActorJsGulpPid');

    childProcess.on('error', (err) => {
      console.log('[xx-xx-xx] *********************', 'ws error, ', err);
    });
    //childProcess.on('exit', (code) => {
    //  console.log(`*********************** Child exited with code ${code}`);
    //});
    
    
    process.stdout.pause();
    process.stderr.pause();
    
    process.kill(gulpPid, 'SIGINT');
    process.kill(childProcess.pid, 'SIGINT');
    this.logGui('ActorJs stopped.');
    //childProcess.kill('SIGINT');*/
  }
}

module.exports = StopActorJsLocal;
