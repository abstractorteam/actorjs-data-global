
const ActorApi = require('actor-api');


class LoginOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.reuse = false;
    this.page = null;
    this.headless = true;
    this.slowMo = 0;
    this.useProxy = true;
  }
  
  *data() {
    this.reuse = this.getTestDataBoolean('reuse', this.reuse);
    this.headless = this.getTestDataBoolean('headless', this.headless);
    this.slowMo = this.getTestDataNumber('slowMo', this.slowMo);
    this.useProxy = this.getTestDataBoolean('useProxy', this.useProxy);
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer', {
      reuse: this.reuse,
      connectionOptions: {
        launchOptions: {
          headless: this.headless,
          slowMo: this.slowMo
        },
        proxy: this.useProxy
      }
    });
  }
  
  *run() {
/*    const page = this.puppeteerConnection.page;
    this.delay(4000);
    yield page.goto('http://actorjs:9020/test-cases/Actor/Demo/DemoLog001');
    yield page.waitForFunction(() => {return null !== document.getElementById("test_case_start_log");});
    this.delay(2000);
    yield page.click('#test_case_start_log');
    this.delay(60000);
/*    yield page.waitForFunction(() => {return null !== document.getElementById("from_start_to_login");});
    this.delay(2000);
    yield page.click('#from_start_to_login');
    yield page.keyboard.down('Shift');
    this.delay(2000);*/
   // yield page.goto('http://actorjs:9020/test-cases/Actor/Demo/DemoLog001');
  }
  
  *exit() {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = LoginOrig;
