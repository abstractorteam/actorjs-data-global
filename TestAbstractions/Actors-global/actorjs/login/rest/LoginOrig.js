
const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');
const HttpPostRequest = require('./msg/HttpPostRequest');


class LoginOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpPostRequest());
    
    const response = this.httpConnection.receive();
    VERIFY_VALUE(HttpApi.StatusCode.OK, response.statusCode, ' HTTP response line status code:');
  }
  
  *exit() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = LoginOrig;
