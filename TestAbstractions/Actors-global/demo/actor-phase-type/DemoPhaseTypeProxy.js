
const ActorApi = require('actor-api');


class DemoPhaseTypeProxy extends ActorApi.ActorProxy {
  *data() {
  }
  
  *initServer() {
  }
  
  *initClient() {
  }
  
  *run() {
  }
  
  *exit() {
  }
}

module.exports = DemoPhaseTypeProxy;
