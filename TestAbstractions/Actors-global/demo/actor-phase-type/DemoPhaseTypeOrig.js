
const ActorApi = require('actor-api');


class DemoPhaseTypeOrig extends ActorApi.ActorOriginating {
  *data() {
  }
  
  *initClient() {
  }
  
  *run() {
  }
  
  *exit() {
  }
}

module.exports = DemoPhaseTypeOrig;
