
const ActorApi = require('actor-api');


class DemoPhaseTypeTerm extends ActorApi.ActorTerminating {
  *data() {
  }
  
  *initServer() {
  }
  
  *run() {
  }
  
  *exit() {
  }
}

module.exports = DemoPhaseTypeTerm;
