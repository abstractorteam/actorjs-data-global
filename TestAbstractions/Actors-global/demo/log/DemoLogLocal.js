
const ActorApi = require('actor-api');


class DemoLogLocal extends ActorApi.ActorLocal {
  *run() {
    this.logEngine('Demo engine log.');
    this.logDebug('Demo debug log.');
    this.logError('Demo error log.');
    this.logWarning('Demo warning log.');
    this.logIp('Demo ip log.');
    this.logGui('Demo gui log.');
    this.logVerifySuccess('Demo verify success log.');
    this.logVerifyFailure('Demo verify failure log.');
    this.logTestData('Demo test data log.');
  }
}


module.exports = DemoLogLocal;
