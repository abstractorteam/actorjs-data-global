

class HttpRequest {
  constructor(methodOrObject, requestUri) {
    if(typeof methodOrObject === "object") {
      this.requestLine = methodOrObject.requestLine;
      this.headers = methodOrObject.headers;
    }
    else {
      this.requestLine = {
        method: methodOrObject,
        requestUri: requestUri,
        httpVersion: HttpRequest.HTTP_1_1
      };
      this.headers = [];
    }
  }

  addHeader(name, value) {
    this.headers.push({
      name: name,
      value: value
    });
    return this;
  }
  
  getHeaderValue(name) {
    let header = this.headers.find((header) => {
      return header.name === name;
    });
    return header ? header.value : undefined;
  }
}

HttpRequest.HTTP_1_1 = 'HTTP/1.1';

module.exports = HttpRequest;
