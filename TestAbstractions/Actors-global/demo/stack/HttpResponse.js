

class HttpResponse {
  constructor(statusCodeOrObject, reasonPhrase) {
    if(typeof statusCodeOrObject === "object") {
      this.statusLine = statusCodeOrObject.statusLine;
      this.headers = statusCodeOrObject.headers;
    }
    else {
      this.statusLine = {
        httpVersion: HttpResponse.HTTP_1_1,
        statusCode: statusCodeOrObject,
        reasonPhrase: reasonPhrase
      };
      this.headers = [];
    }
  }

  addHeader(name, value) {
    this.headers.push({
      name: name,
      value: value
    });
    return this;
  }
  
  getHeaderValue(name) {
    let header = this.headers.find((header) => {
      return header.name === name;
    });
    return header ? header.value : undefined;
  }
}

HttpResponse.HTTP_1_1 = 'HTTP/1.1';

module.exports = HttpResponse;
