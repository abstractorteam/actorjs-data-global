
const ActorApi = require('actor-api');
const HttpResponse = require('./HttpResponse');
const HttpRequest = require('./HttpRequest');


class HttpTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
    this.content = this.getContent('content-name', 'image');
  }
  
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
    
    const request = new HttpRequest(this.socketConnection.receiveObject());
    VERIFY_VALUE('GET', request.requestLine.method);
    VERIFY_MANDATORY('accept', request.getHeaderValue('Accept'));
    VERIFY_VALUE(this.content.path, request.requestLine.requestUri);
    VERIFY_MANDATORY('uri', request.requestLine.requestUri);
    
    this.socketConnection.sendObject(new HttpResponse('200', 'OK').addHeader('Content-Type', this.content.mime).addHeader('Content-Length', this.content.size));
    this.socketConnection.send(this.content);
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = HttpTerm;
