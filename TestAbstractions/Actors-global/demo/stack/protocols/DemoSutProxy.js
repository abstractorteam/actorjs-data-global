
const ActorApi = require('actor-api');
const UserAuthorizationRequest = require('./msg/UserAuthorizationRequest');
const RadiusAccessAccept = require('./msg/RadiusAccessAccept');


class DemoSutProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.radiusMobileServerConnection = null;
    this.httpMobileServerConnection = null;
    this.diameterHssClientConnection = null;
    this.socketRightsClientConnection = null;
    this.httpWwwClientConnection = null;
    
  }

  *data() {
  }
  
  *initServer() {
    this.radiusMobileServerConnection = this.createServer('radius');
    this.httpMobileServerConnection = this.createServer('http', {srvIndex: 1});
  }
  
  *initClient() {
    this.diameterHssClientConnection = this.createConnection('diameter-cx');
    this.socketRightsClientConnection = this.createConnection('socket', {srcIndex: 1, dstIndex: 1});
  }
  
  *run() {
    this.radiusMobileServerConnection.accept();
    
    const loginRequest = this.radiusMobileServerConnection.receive();
    this.diameterHssClientConnection.send(new UserAuthorizationRequest('Gunnar'));
    const loginResponse = this.diameterHssClientConnection.receive();
    this.radiusMobileServerConnection.send(new RadiusAccessAccept());
    
    this.httpMobileServerConnection.accept();
    const httpRequest = this.httpMobileServerConnection.receive();
    this.socketRightsClientConnection.sendObject({blacklist: 'get'});
    const blackListResponse = this.socketRightsClientConnection.receiveObject();
    
    this.httpWwwClientConnection = this.createConnection('http', {srcIndex: 2, dstIndex: 2});
    this.httpWwwClientConnection.send(httpRequest);
    const httpResponse = this.httpWwwClientConnection.receive();
    this.httpMobileServerConnection.send(httpResponse);
  }
  
  *exit() {
    this.closeConnection(this.httpWwwClientConnection);
    this.closeConnection(this.socketRightsClientConnection);
    this.closeConnection(this.diameterHssClientConnection);
    this.closeConnection(this.httpMobileServerConnection);
    this.closeConnection(this.radiusMobileServerConnection);
  }
}

module.exports = DemoSutProxy;
