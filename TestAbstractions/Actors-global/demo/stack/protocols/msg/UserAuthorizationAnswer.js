
'use strict';

const DiameterCxApi = require('diameter-cx-stack-api');


class UserAuthorizationAnswer extends DiameterCxApi.msg.UserAuthorizationAnswer {
  constructor(resultCode) {
    super(16777216);
    this.addAvpResultCode(resultCode);
  }
}

module.exports = UserAuthorizationAnswer;
