
'use strict';

const HttpApi = require('http-stack-api');


class HttpGetImageReq extends HttpApi.Request {
  constructor(requestUri, accept) {
    super(HttpApi.Method.GET, requestUri, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, 'www.example.com');
    this.addHeader(HttpApi.Header.ACCEPT, accept);
    this.addHeader(HttpApi.Header.CONTENT_LENGTH, 0);
  }
}

module.exports = HttpGetImageReq;
