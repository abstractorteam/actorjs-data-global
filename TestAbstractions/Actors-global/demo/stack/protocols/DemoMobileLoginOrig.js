
const ActorApi = require('actor-api');
const RadiusAccessRequest = require('./msg/RadiusAccessRequest');


class DemoMobileLoginOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.radiusConnection = null;
  }

  *data() {
    this.setSharedData('LOGIN-INFO', true);
  }
  
  *initClient() {
    this.radiusConnection = this.createConnection('radius');
  }
  
  *run() {
    this.radiusConnection.send(new RadiusAccessRequest());
    const response = this.radiusConnection.receive();
    
    this.setSharedData('LOGIN OK');
  }
  
  *exit() {
    this.closeConnection(this.radiusConnection);
  }
}

module.exports = DemoMobileLoginOrig;
