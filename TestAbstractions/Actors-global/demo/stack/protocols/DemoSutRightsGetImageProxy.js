
const ActorApi = require('actor-api');


class DemoSutRightsGetImageProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.httpMobileServerConnection = null;
    this.socketRightsClientConnection = null;
    this.httpWwwClientConnection = null;
  }

  *data() {
  }
  
  *initServer() {
    this.httpMobileServerConnection = this.createServer('http');
  }
  
  *initClient() {
    this.socketRightsClientConnection = this.createConnection('socket');
  }
  
  *run() {
    this.httpMobileServerConnection.accept();
    const httpRequest = this.httpMobileServerConnection.receive();
    this.socketRightsClientConnection.sendObject({blacklist: 'get'});
    const blackListResponse = this.socketRightsClientConnection.receiveObject();
    
    this.httpWwwClientConnection = this.createConnection('http', {srcIndex: 1, dstIndex: 1});
    this.httpWwwClientConnection.send(httpRequest);
    const httpResponse = this.httpWwwClientConnection.receive();
    this.httpMobileServerConnection.send(httpResponse);
  }
  
  *exit() {
    this.closeConnection(this.httpWwwClientConnection);
    this.closeConnection(this.socketRightsClientConnection);
    this.closeConnection(this.httpMobileServerConnection);
  }
}

module.exports = DemoSutRightsGetImageProxy;
