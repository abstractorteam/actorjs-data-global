
const ActorApi = require('actor-api');
const RadiusAccessAccept = require('./msg/RadiusAccessAccept');
const UserAuthorizationRequest = require('./msg/UserAuthorizationRequest');


class DemoSutLoginProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.radiusServerConnection = null;
    this.diameterClientConnection = null;
  }
  
  *data() {
  }
  
  *initServer() {
    this.radiusServerConnection = this.createServer('radius');
  }
  
  *initClient() {
    this.diameterClientConnection = this.createConnection('diameter-cx');
  }
  
  *run() {
    this.radiusServerConnection.accept();
    
    const request = this.radiusServerConnection.receive();
    this.diameterClientConnection.send(new UserAuthorizationRequest('Gunnar'));
    const response = this.diameterClientConnection.receive();
    this.radiusServerConnection.send(new RadiusAccessAccept());
  }
  
  *exit() {
    this.closeConnection(this.diameterClientConnection);
    this.closeConnection(this.radiusServerConnection);
  }
}

module.exports = DemoSutLoginProxy;
