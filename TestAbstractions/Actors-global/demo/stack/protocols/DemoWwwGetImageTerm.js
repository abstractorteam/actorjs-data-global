
const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');
const HttpGetImage200OkResp = require('./msg/HttpGetImage200OkResp');


class DemoWwwGetImageTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.content = null;
  }

  *data() {
    this.content = this.getContent('content-name');
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
    this.httpConnection.send(new HttpGetImage200OkResp(this.content));
  }
  
  *exit() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = DemoWwwGetImageTerm;
