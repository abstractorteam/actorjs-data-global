
const ActorApi = require('actor-api');
const DiameterRequest = require('./DiameterRequest');
const HttpResponse = require('./HttpResponse');


class DiameterHttpProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.socketWwwServerConnection = null;
    this.socketHssServerConnection = null;
    this.socketRightsClientConnection = null;
    this.socketHssClientConnection = null;
    this.socketWwwClientConnection = null;
  }
  
  *initServer() {
    this.socketHssServerConnection = this.createServer('socket');
    this.socketWwwServerConnection = this.createServer('socket', {srvIndex: 1});
  }
  
  *initClient() {
    this.socketHssClientConnection = this.createConnection('socket');
    this.socketRightsClientConnection = this.createConnection('socket', {dstIndex: 1});
  }
  
  *run() {
    this.socketHssServerConnection.accept();
    
    const diamRequest = this.socketHssServerConnection.receiveObject();
    this.socketHssClientConnection.sendObject(diamRequest);
    
    const diamHssResponse = this.socketHssClientConnection.receiveObject();
    this.socketHssServerConnection.sendObject(diamHssResponse);
    
    this.socketWwwServerConnection.accept();
    
    const httpRequest = this.socketWwwServerConnection.receiveObject();
    
    this.socketRightsClientConnection.sendObject(new DiameterRequest('allowed'));
    
    const diamRightsResponse = this.socketRightsClientConnection.receiveObject();
    
    this.socketWwwClientConnection = this.createConnection('socket', {dstIndex: 2});
    this.socketWwwClientConnection.sendObject(httpRequest);
    
    const httpResponse = new HttpResponse(this.socketWwwClientConnection.receiveObject());
    this.socketWwwServerConnection.sendObject(httpResponse);
    
    const httpResponseData = this.socketWwwClientConnection.receiveSize(httpResponse.getHeaderValue('Content-Length'));
    this.socketWwwServerConnection.send(httpResponseData);
    this.socketWwwClientConnection.close();
  }
  
  *exit() {
    this.closeConnection(this.socketWwwServerConnection);
    this.closeConnection(this.socketHssServerConnection);
    this.closeConnection(this.socketRightsClientConnection);
    this.closeConnection(this.socketHssClientConnection);
    this.closeConnection(this.socketWwwClientConnection);
  }
}

module.exports = DiameterHttpProxy;
