

class DiameterResponse {
  constructor(reqType, result) {
    this.reqType = reqType;
    this.result = result;
  }
}

module.exports = DiameterResponse;
