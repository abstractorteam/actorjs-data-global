
const ActorApi = require('actor-api');
const HttpRequest = require('./HttpRequest');
const HttpResponse = require('./HttpResponse');


class HttpOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
  }

  *data() {
    this.content = this.getContent('content-name', 'image');
    this.acceptHeader = this.getTestDataString('accept');
  }
  
  *initClient() {
    this.waitForSharedData('LOGIN OK');
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnection.sendObject(new HttpRequest('GET', 'hockey/nylander.jpg').addHeader('Accept', this.acceptHeader));
    
    const response = new HttpResponse(this.socketConnection.receiveObject());
    VERIFY_VALUE('200', response.statusLine.statusCode);
    VERIFY_VALUE('OK', response.statusLine.reasonPhrase);
    VERIFY_VALUE(this.content.size, response.getHeaderValue('Content-Length'));
    
    const responseData = this.socketConnection.receiveSize(response.getHeaderValue('Content-Length'));
    VERIFY_CONTENT_VALUE(this.content, response.getHeaderValue('Content-Length'), responseData);
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = HttpOrig;
