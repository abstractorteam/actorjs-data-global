
const ActorApi = require('actor-api');


class DemoDelayLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    this.delay(0);
  }
}

module.exports = DemoDelayLocal;
