
'use strict';

const TcpServer = require('./tcpServer');

const host = process.argv[2];
const port = Number.parseInt(process.argv[3]);
const tcpServer = new TcpServer(host, port);


process.on('message', (msg) => {
  if('close' === msg.cmd) {
    tcpServer.close(() => {
      process.exit();
    });
  }
});

tcpServer.start((err) => {
  process.send({cmd: 'started'});
});

