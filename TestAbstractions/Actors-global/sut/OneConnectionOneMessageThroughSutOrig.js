
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class OneConnectionOneMessageThroughSutOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.networkType = StackApi.NetworkType.TCP;
    this.socketConnection = null;
  }

  *data() {
    this.networkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket', {
      networkType: this.networkType
    });
  }
  
  *run() {
    const address = this.getSharedData(`address${this.instanceIndex}`);
    this.socketConnection.sendObject({
      cmd: 'msg',
      reqCmd: 'name',
      host: address.host,
      port: address.port
    });
    const response = this.socketConnection.receiveObject();
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = OneConnectionOneMessageThroughSutOrig;
