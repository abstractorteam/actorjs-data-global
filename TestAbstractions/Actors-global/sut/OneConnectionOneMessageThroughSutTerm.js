
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class OneConnectionOneMessageThroughSutTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.networkType = StackApi.NetworkType.TCP;
  }

  *data() {
    this.networkType = this.getTestDataNumber('network-type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
  }
  
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: this.networkType
    });
    this.setSharedData(`address${this.instanceIndex}`, this.getSrvAddress());
  }
  
  *run() {
    this.socketConnection.accept();
    
    const cmd = this.socketConnection.receiveObject();
    
    VERIFY_VALUE('name', cmd.reqCmd);
    
    this.socketConnection.sendObject({login: 'gunnar'});
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = OneConnectionOneMessageThroughSutTerm;
