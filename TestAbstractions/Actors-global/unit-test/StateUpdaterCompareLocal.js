
const ActorApi = require('actor-api');
const StateUpdater = require('../../../client/state-updater');


class StateUpdaterCompareLocal extends ActorApi.ActorLocal {
  *data() {
  }
  
  *run() {
    this.shallowCompareNumeric();
  }
  
  shallowCompareNumeric() {
    VERIFY_VALUE(1, 1);
  }
}

module.exports = StateUpdaterCompareLocal;
