
const ActorApi = require('actor-api');
const TestStore = require('./testStore');
const ActionTestNumeric = require('./actions/ActionTestNumeric');
const ActionTestObjectNumeric = require('./actions/ActionTestObjectNumeric');
const ActionTestObjectObjectObjectNumeric = require('./actions/ActionTestObjectObjectObjectNumeric');
const ActionTestMap = require('./actions/ActionTestMap');
const ActionTestNestedMap = require('./actions/ActionTestNestedMap');


class StoreUpdateLocal extends ActorApi.ActorLocal {
  *data() {
  }
  
  *run() {
    this.store = new TestStore();
    this.test_numeric();
    this.test_object_numeric();
    this.test_equal_object_numeric();
    this.test_object_object_object_numeric();
    this.test_map();
    this.test_nested_map();
  }
  
  test_numeric() {
    this.store.dispatch(new ActionTestNumeric(7));
    VERIFY_VALUE(7, this.store.state.numeric);
  }
  
  test_object_numeric() {
    this.store.dispatch(new ActionTestObjectNumeric(8));
    VERIFY_VALUE(8, this.store.state.object.numeric);
    
    let previusStateObject = this.store.state.object;
    this.store.dispatch(new ActionTestObjectNumeric(9));
    VERIFY_VALUE(9, this.store.state.object.numeric);
    VERIFY_VALUE(previusStateObject, this.store.state.object, undefined, '!==');
  }
  
  test_equal_object_numeric() {
    this.store.dispatch(new ActionTestObjectNumeric(10));
    VERIFY_VALUE(10, this.store.state.object.numeric);
    
    let previusStateObject = this.store.state.object;
    this.store.dispatch(new ActionTestObjectNumeric(previusStateObject.numeric));
    VERIFY_VALUE(10, this.store.state.object.numeric);
    VERIFY_VALUE(previusStateObject, this.store.state.object);
  }
  
  test_object_object_object_numeric() {
    let previusStateObject = this.store.state.object;
    let previusStateObjectObject = this.store.state.object.object;
    let previusStateObjectObjectObject = this.store.state.object.object.object;
    
    this.store.dispatch(new ActionTestObjectObjectObjectNumeric(37));
    
    VERIFY_VALUE(37, this.store.state.object.object.object.numeric);
    VERIFY_VALUE(previusStateObjectObjectObject, this.store.state.object.object, undefined, '!==');
    VERIFY_VALUE(previusStateObjectObject, this.store.state.object.object, undefined, '!==');
    VERIFY_VALUE(previusStateObject, this.store.state.object, undefined, '!==');
  }
  
  test_map() {
    let previusStateMap = this.store.state.map;
    this.store.dispatch(new ActionTestMap(1, 'add'));
    VERIFY_VALUE('add', this.store.state.map.get(1));
    VERIFY_VALUE(previusStateMap, this.store.state.map, undefined, '!==');
     
    previusStateMap = this.store.state.map;
    this.store.dispatch(new ActionTestMap(2, 'minus'));
    VERIFY_VALUE('minus', this.store.state.map.get(2));
    VERIFY_VALUE(previusStateMap, this.store.state.map, undefined, '!==');

    previusStateMap = this.store.state.map;
    this.store.dispatch(new ActionTestMap(2, 'multiply'));
    VERIFY_VALUE('multiply', this.store.state.map.get(2));
    VERIFY_VALUE(previusStateMap, this.store.state.map, undefined, '!==');

    previusStateMap = this.store.state.map;
    this.store.dispatch(new ActionTestMap(2, 'multiply'));
    VERIFY_VALUE('multiply', this.store.state.map.get(2));
    VERIFY_VALUE(previusStateMap, this.store.state.map, undefined, '===');
  }
  
  test_nested_map() {
    let previusStateMap = this.store.state.map;
    let object = {
      string: 'a',
      numeric: 3
    };
    this.store.dispatch(new ActionTestMap(12, object));
    VERIFY_VALUE(object, this.store.state.map.get(12));
    VERIFY_VALUE(previusStateMap, this.store.state.map, undefined, '!==');

    this.store.dispatch(new ActionTestNestedMap(12, 'q'));
    VERIFY_VALUE('q', this.store.state.map.get(12).string);
  }
}

module.exports = StoreUpdateLocal;
