
const ActorApi = require('actor-api');
const {BitByte} = require('stack-api');


class GetBitLocal extends ActorApi.ActorLocal {
  *run() {
    const value1 = 255;
    VERIFY_VALUE(1, BitByte.getBit(value1, 0));
    VERIFY_VALUE(1, BitByte.getBit(value1, 7));
    const value2 = 0;
    VERIFY_VALUE(0, BitByte.getBit(value2, 0));
    VERIFY_VALUE(0, BitByte.getBit(value2, 7));
    const value3 = 126;
    VERIFY_VALUE(0, BitByte.getBit(value3, 0));
    VERIFY_VALUE(1, BitByte.getBit(value3, 1));
    VERIFY_VALUE(1, BitByte.getBit(value3, 6));
    VERIFY_VALUE(0, BitByte.getBit(value3, 7));
    const value4 = 129;
    VERIFY_VALUE(1, BitByte.getBit(value4, 0));
    VERIFY_VALUE(0, BitByte.getBit(value4, 1));
    VERIFY_VALUE(0, BitByte.getBit(value4, 6));
    VERIFY_VALUE(1, BitByte.getBit(value4, 7));
  }
}

module.exports = GetBitLocal;
