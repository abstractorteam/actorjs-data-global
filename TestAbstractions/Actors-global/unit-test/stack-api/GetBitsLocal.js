
const ActorApi = require('actor-api');
const {BitByte} = require('stack-api');


class GetBitsLocal extends ActorApi.ActorLocal {
  *run() {
    const value1 = 255;
    VERIFY_VALUE(15, BitByte.getBits(value1, 4,7));
    VERIFY_VALUE(7, BitByte.getBits(value1, 4,6));
    VERIFY_VALUE(31, BitByte.getBits(value1, 2,6));
    const value2 = 21;
    VERIFY_VALUE(1, BitByte.getBits(value2, 6,7));
    VERIFY_VALUE(5, BitByte.getBits(value2, 5,7));
    VERIFY_VALUE(5, BitByte.getBits(value2, 4,7));
    VERIFY_VALUE(21, BitByte.getBits(value2, 3,7));
  }
}

module.exports = GetBitsLocal;
