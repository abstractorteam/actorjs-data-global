
const ActorApi = require('actor-api');


class DelayOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
  }
  
  *run() {
    this.delay(0);
  }
}

module.exports = DelayOrig;
