
const ActorApi = require('actor-api');


class GetTestDataArrayStringsBOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.b = [[]];
  }
  
  *data() {
    this.b = this.getTestDataArrayStrings('demoKey2');
    this.logDebug(this.b);
  }
  
  *run() {
    VERIFY_VALUE([['Good evening', 'sir'], ['Is', 'all', 'well']], this.b);
  }
}

module.exports = GetTestDataArrayStringsBOrig;
