
const ActorApi = require('actor-api');


class GetTestDataStringDOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = '';
    this.d = '';
  }
  
  *data() {
    this.a = this.getTestDataString('demoKey');
    this.d = this.getTestDataString('demoKeyX', () => {
      return this.a + ' friend';
    });
    this.logDebug(this.a);
    this.logDebug(this.d);
  }
  
  *run() {
    VERIFY_VALUE('Hello', this.a);
    VERIFY_VALUE('Hello friend', this.d);
  }   
}

module.exports = GetTestDataStringDOrig;
