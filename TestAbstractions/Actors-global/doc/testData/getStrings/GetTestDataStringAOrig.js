
const ActorApi = require('actor-api');


class GetTestDataStringAOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = '';
  }
  
  *data() {
    this.a = this.getTestDataString('demoKey');
    this.logDebug(this.a);
  }
  
  *run() {
    VERIFY_VALUE('Hello', this.a);
  }
}

module.exports = GetTestDataStringAOrig;
