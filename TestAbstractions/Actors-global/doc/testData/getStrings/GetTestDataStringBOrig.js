
const ActorApi = require('actor-api');


class GetTestDataStringBOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.b = '';
  }
  
  *data() {
    this.b = this.getTestDataString('demoKey', 'good bye');
    this.logDebug(this.b);
  }
  
  *run() {
    VERIFY_VALUE('Hello', this.b);
  }
}

module.exports = GetTestDataStringBOrig;
