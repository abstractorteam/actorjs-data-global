
const ActorApi = require('actor-api');


class GetTestDataStringsBOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.b = [];
  }
  
  *data() {
    this.b = this.getTestDataStrings('demoKey2');
    this.logDebug(this.b);
  }
  
  *run() {
    VERIFY_VALUE(['Hello', 'Good day'], this.b);
  }
}

module.exports = GetTestDataStringsBOrig;
