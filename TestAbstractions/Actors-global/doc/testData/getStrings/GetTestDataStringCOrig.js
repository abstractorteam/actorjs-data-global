
const ActorApi = require('actor-api');


class GetTestDataStringCOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.c = '';
  }
  
  *data() {
    this.c = this.getTestDataString('demoKeyX', 'good bye');
    this.logDebug(this.c);
  }
  
  *run() {
    VERIFY_VALUE('good bye', this.c);
  }
}

module.exports = GetTestDataStringCOrig;
