
const ActorApi = require('actor-api');


class GetTestDataStringsEOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.e = [];
    this.d = [];
  }
  
  *data() {
    this.d = this.getTestDataStrings('demoStringsKeyX', ['Bye']);
    this.e = this.getTestDataStrings('demoStringsKeyX', () => {
      const x = this.d.slice();
      x.push('Friend');
      return x;
    });
    this.logDebug(this.d);
    this.logDebug(this.e);
  }
  
  *run() {
    VERIFY_VALUE(['Bye'], this.d);
    VERIFY_VALUE(['Bye', 'Friend'], this.e);
  }
}

module.exports = GetTestDataStringsEOrig;
