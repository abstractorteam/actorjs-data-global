
const ActorApi = require('actor-api');


class GetTestDataStringsDOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.d = [];
  }
  
  *data() {
    this.d = this.getTestDataStrings('demoKeyX', ['Bye']);
    this.logDebug(this.d);
  }
  
  *run() {
    VERIFY_VALUE(['Bye'], this.d);
  }
}

module.exports = GetTestDataStringsDOrig;
