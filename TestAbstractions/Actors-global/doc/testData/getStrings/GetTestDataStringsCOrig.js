
const ActorApi = require('actor-api');


class GetTestDataStringsCOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.c = [];
  }
  
  *data() {
    this.c = this.getTestDataStrings('demoKey2', ['Bye']);
    this.logDebug(this.c);
  }
  
  *run() {
    VERIFY_VALUE(['Hello', 'Good day'], this.c);
  }
}

module.exports = GetTestDataStringsCOrig;
