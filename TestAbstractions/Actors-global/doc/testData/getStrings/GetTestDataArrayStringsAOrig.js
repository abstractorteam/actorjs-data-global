
const ActorApi = require('actor-api');


class GetTestDataArrayStringsAOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = [[]];
  }
  
  *data() {
    this.a = this.getTestDataArrayStrings('demoKey1');
    this.logDebug(this.a);
  }
  
  *run() {
    VERIFY_VALUE([['Hi', 'there'], ['How', 'are', 'you']], this.a);
  }
}

module.exports = GetTestDataArrayStringsAOrig;
