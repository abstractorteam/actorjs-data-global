
const ActorApi = require('actor-api');


class GetTestDataArrayStringsDOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.d = [[]];
  }
  
  *data() {
    this.d = this.getTestDataArrayStrings('demoKeyX', [['Fare', 'well'], ['Take', 'care']]);
    this.logDebug(this.d);
  }
  
  *run() {
    VERIFY_VALUE([['Fare', 'well'], ['Take', 'care']], this.d);
  }
}

module.exports = GetTestDataArrayStringsDOrig;
