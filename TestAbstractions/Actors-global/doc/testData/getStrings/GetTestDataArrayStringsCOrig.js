
const ActorApi = require('actor-api');


class GetTestDataArrayStringsCOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.c = [[]];
  }
  
  *data() {
    this.c = this.getTestDataArrayStrings('demoKey2', [['Fare', 'well'], ['Take', 'care']]);
    this.logDebug(this.c);
  }
  
  *run() {
    VERIFY_VALUE([['Good evening', 'sir'], ['Is', 'all', 'well']], this.c);
  }
}

module.exports = GetTestDataArrayStringsCOrig;
