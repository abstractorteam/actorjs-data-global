
const ActorApi = require('actor-api');


class GetTestDataStringsAOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = [];
  }
  
  *data() {
    this.a = this.getTestDataStrings('demoKey1');
    this.logDebug(this.a);
  }
  
  *run() {
    VERIFY_VALUE(['Hello', 'Hi'], this.a);
  }
}

module.exports = GetTestDataStringsAOrig;
