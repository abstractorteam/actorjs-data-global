
const ActorApi = require('actor-api');


class GetTestDataNumberBOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.b = 0.0;
  }
  
  *data() {
    this.b = this.getTestDataNumber('demoKey2');
    this.logDebug(this.b);
  }
  
  *run() {
    VERIFY_VALUE(30.001, this.b);
  }
}

module.exports = GetTestDataNumberBOrig;
