
const ActorApi = require('actor-api');


class GetTestDataArrayNumbersAOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = [[]];
  }
  
  *data() {
    this.a = this.getTestDataArrayNumbers('demoKey1');
    this.logDebug(this.a);
  }
  
  *run() {
    VERIFY_VALUE([[7, 30], [16, 0]], this.a);
  }
}

module.exports = GetTestDataArrayNumbersAOrig;
