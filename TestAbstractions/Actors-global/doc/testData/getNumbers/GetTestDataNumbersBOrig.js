
const ActorApi = require('actor-api');


class GetTestDataNumbersBOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.b = [];
  }
  
  *data() {
    this.b = this.getTestDataNumbers('demoKey2');
    this.logDebug(this.b);
  }
  
  *run() {
    VERIFY_VALUE([37.865, -119.538], this.b);
  }
}

module.exports = GetTestDataNumbersBOrig;
