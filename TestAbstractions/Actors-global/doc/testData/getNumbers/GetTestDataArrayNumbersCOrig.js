
const ActorApi = require('actor-api');


class GetTestDataArrayNumbersCOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.c = [[]];
  }
  
  *data() {
    this.c = this.getTestDataArrayNumbers('demoKey1', [[8, 15], [16, 45]]);
    this.logDebug(this.c);
  }
  
  *run() {
    VERIFY_VALUE([[7, 30], [16, 0]], this.c);
  }
}

module.exports = GetTestDataArrayNumbersCOrig;
