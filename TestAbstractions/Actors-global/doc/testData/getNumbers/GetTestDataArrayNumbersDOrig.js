
const ActorApi = require('actor-api');


class GetTestDataArrayNumbersDOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.d = [[]];
  }
  
  *data() {
    this.d = this.getTestDataArrayNumbers('demoKeyX', [[8, 15], [16, 45]]);
    this.logDebug(this.d);
  }
  
  *run() {
    VERIFY_VALUE([[8, 15], [16, 45]], this.d);
  }
}

module.exports = GetTestDataArrayNumbersDOrig;
