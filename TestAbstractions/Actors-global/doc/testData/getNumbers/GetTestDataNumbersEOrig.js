
const ActorApi = require('actor-api');


class GetTestDataNumbersEOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.e = [];
  }
  
  *data() {
    this.e = this.getTestDataNumbers('demoKeyX', [78.405, 13.295]);
    this.logDebug(this.e);
  }
  
  *run() {
    VERIFY_VALUE([78.405, 13.295], this.e);
  }
}

module.exports = GetTestDataNumbersEOrig;
