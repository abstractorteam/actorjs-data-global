
const ActorApi = require('actor-api');


class GetTestDataArrayNumbersEOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.e = [[]];
  }
  
  *data() {
    this.e = this.getTestDataArrayNumbers('demoKeyX', [[8, 45], [17, 0]]);
    this.logDebug(this.e);
  }
  
  *run() {
    VERIFY_VALUE([[8, 45], [17, 0]], this.e);
  }
}

module.exports = GetTestDataArrayNumbersEOrig;
