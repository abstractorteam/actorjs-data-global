
const ActorApi = require('actor-api');


class GetTestDataArrayNumbersGOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.g = [[]];
  }
  
  *data() {
    this.g = this.getTestDataArrayNumbers('demoKeyObject', 'UNKNOWN', {
      BREAKFAST: [[9, 30], [9, 45]],
      LUNCH: [[12, 0], [12, 30]]
    });
    this.logDebug(this.g);
  }
  
  *run() {
    VERIFY_VALUE([[12, 0], [12, 30]], this.g);
  }
}

module.exports = GetTestDataArrayNumbersGOrig;
