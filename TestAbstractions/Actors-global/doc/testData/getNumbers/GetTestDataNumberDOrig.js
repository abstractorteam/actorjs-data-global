
const ActorApi = require('actor-api');


class GetTestDataNumberDOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.d = 0;
  }
  
  *data() {
    this.d = this.getTestDataNumber('demoKeyX', 34);
    this.logDebug(this.d);
  }
  
  *run() {
    VERIFY_VALUE(34, this.d);
  }
}

module.exports = GetTestDataNumberDOrig;
