
const ActorApi = require('actor-api');


class GetTestDataArrayNumbersBOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.b = [[]];
  }
  
  *data() {
    this.b = this.getTestDataNumbers('demoKey2');
    this.logDebug(this.b);
  }
  
  *run() {
    VERIFY_VALUE([[9, 0], [17, 30]], this.b);
  }
}

module.exports = GetTestDataArrayNumbersBOrig;
