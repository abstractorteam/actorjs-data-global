
const ActorApi = require('actor-api');


class GetTestDataArrayNumbersFOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = [[]];
    this.f = [[]];
  }
  
  *data() {
    this.a = this.getTestDataArrayNumbers('demoKey1');
    this.f = this.getTestDataArrayNumbers('demoKeyX', () => {
      const x = this.a.slice();
      x.push([18, 30]);
      return x;
    });
    this.logDebug(this.a);
    this.logDebug(this.f);
  }
  
  *run() {
    VERIFY_VALUE([[7, 30], [16, 0], [18, 30]], this.f);
  }
}

module.exports = GetTestDataArrayNumbersFOrig;
