
const ActorApi = require('actor-api');


class GetTestDataNumbersCOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.c = [];
  }
  
  *data() {
    this.c = this.getTestDataNumbers('demoKey1', [-32.859, -71.237]);
    this.logDebug(this.c);
  }
  
  *run() {
    VERIFY_VALUE([56.833, 13.941], this.c);
  }
}

module.exports = GetTestDataNumbersCOrig;
