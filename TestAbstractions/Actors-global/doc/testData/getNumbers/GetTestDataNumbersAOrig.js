
const ActorApi = require('actor-api');


class GetTestDataNumbersAOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = [];
  }
  
  *data() {
    this.a = this.getTestDataNumbers('demoKey1');
    this.logDebug(this.a);
  }
  
  *run() {
    VERIFY_VALUE([56.833, 13.941], this.a);
  }
}

module.exports = GetTestDataNumbersAOrig;
