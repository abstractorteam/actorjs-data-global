
const ActorApi = require('actor-api');


class GetTestDataNumberEOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.e = 0.0;
  }
  
  *data() {
    this.e = this.getTestDataNumber('demoKeyX', -4.999);
    this.logDebug(this.e);
  }
  
  *run() {
    VERIFY_VALUE(-4.999, this.e);
  }
}

module.exports = GetTestDataNumberEOrig;
