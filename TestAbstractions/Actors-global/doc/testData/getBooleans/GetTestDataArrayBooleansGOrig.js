
const ActorApi = require('actor-api');


class GetTestDataArrayBooleansGOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.g = [[]];
  }
  
  *data() {
    this.g = this.getTestDataArrayBooleans('demoKeyObject', 'UNKNOWN', {
      ALL_TRUE: [[true, true], [true, true]],
      ALL_FALSE: [[false, false], [false, false]]
    });
    this.logDebug(this.g);
  }
  
  *run() {
    VERIFY_VALUE([[false, false], [false, false]], this.g);
  }
}

module.exports = GetTestDataArrayBooleansGOrig;
