
const ActorApi = require('actor-api');


class GetTestDataArrayBooleansCOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.c = [[]];
  }
  
  *data() {
    this.c = this.getTestDataArrayBooleans('demoKey1', [[false, false], [true, true]]);
    this.logDebug(this.c);
  }
  
  *run() {
    VERIFY_VALUE([[true, true], [false, false]], this.c);
  }
}

module.exports = GetTestDataArrayBooleansCOrig;
