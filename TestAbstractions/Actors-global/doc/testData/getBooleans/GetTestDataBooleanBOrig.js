
const ActorApi = require('actor-api');


class GetTestDataBooleanBOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.b = true;
  }
  
  *data() {
    this.b = this.getTestDataBoolean('demoKey2');
    this.logDebug(this.b);
  }
  
  *run() {
    VERIFY_VALUE(false, this.b);
  }
}

module.exports = GetTestDataBooleanBOrig;
