
const ActorApi = require('actor-api');


class GetTestDataBooleansCOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.c = [];
  }
  
  *data() {
    this.c = this.getTestDataBooleans('demoKey1', [false, true]);
    this.logDebug(this.c);
  }
  
  *run() {
    VERIFY_VALUE([true, false], this.c);
  }
}

module.exports = GetTestDataBooleansCOrig;
