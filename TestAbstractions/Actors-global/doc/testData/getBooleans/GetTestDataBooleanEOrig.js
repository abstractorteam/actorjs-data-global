
const ActorApi = require('actor-api');


class GetTestDataBooleanEOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.e = true;
  }
  
  *data() {
    this.e = this.getTestDataBoolean('demoKeyX', true);
    this.logDebug(this.e);
  }
  
  *run() {
    VERIFY_VALUE(true, this.e);
  }
}

module.exports = GetTestDataBooleanEOrig;
