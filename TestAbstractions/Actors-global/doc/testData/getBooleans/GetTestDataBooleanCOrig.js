
const ActorApi = require('actor-api');


class GetTestDataBooleanCOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.c = true;
  }
  
  *data() {
    this.c = this.getTestDataBoolean('demoKey1', false);
    this.logDebug(this.c);
  }
  
  *run() {
    VERIFY_VALUE(true, this.c);
  }
}

module.exports = GetTestDataBooleanCOrig;
