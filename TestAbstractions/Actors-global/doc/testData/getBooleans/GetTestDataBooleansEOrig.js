
const ActorApi = require('actor-api');


class GetTestDataBooleansEOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.e = [];
  }
  
  *data() {
    this.e = this.getTestDataBooleans('demoKeyX', [true, false]);
    this.logDebug(this.e);
  }
  
  *run() {
    VERIFY_VALUE([true, false], this.e);
  }
}

module.exports = GetTestDataBooleansEOrig;
