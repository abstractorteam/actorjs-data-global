
const ActorApi = require('actor-api');


class GetTestDataArrayBooleansDOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.d = [[]];
  }
  
  *data() {
    this.d = this.getTestDataArrayBooleans('demoKeyX', [[false, false], [true, true]]);
    this.logDebug(this.d);
  }
  
  *run() {
    VERIFY_VALUE([[false, false], [true, true]], this.d);
  }
}

module.exports = GetTestDataArrayBooleansDOrig;
