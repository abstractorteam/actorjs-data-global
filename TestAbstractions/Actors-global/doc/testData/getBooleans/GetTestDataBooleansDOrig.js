
const ActorApi = require('actor-api');


class GetTestDataBooleansDOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.d = [];
  }
  
  *data() {
    this.d = this.getTestDataBooleans('demoKeyX', [false, true]);
    this.logDebug(this.d);
  }
  
  *run() {
    VERIFY_VALUE([false, true], this.d);
  }
}

module.exports = GetTestDataBooleansDOrig;
