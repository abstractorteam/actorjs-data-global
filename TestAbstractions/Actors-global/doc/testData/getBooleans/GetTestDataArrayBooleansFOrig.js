
const ActorApi = require('actor-api');


class GetTestDataArrayBooleansFOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = [[]];
    this.f = [[]];
  }
  
  *data() {
    this.a = this.getTestDataArrayBooleans('demoKey1');
    this.f = this.getTestDataArrayBooleans('demoKeyX', () => {
      const x = this.a.slice();
      x.push([true, true]);
      return x;
    });
    this.logDebug(this.a);
    this.logDebug(this.f);
  }
  
  *run() {
    VERIFY_VALUE([[true, true], [false, false]], this.a);
    VERIFY_VALUE([[true, true], [false, false], [true, true]], this.f);
  }
}

module.exports = GetTestDataArrayBooleansFOrig;
