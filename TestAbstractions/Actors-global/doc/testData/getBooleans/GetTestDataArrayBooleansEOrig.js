
const ActorApi = require('actor-api');


class GetTestDataArrayBooleansEOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.e = [[]];
  }
  
  *data() {
    this.e = this.getTestDataArrayBooleans('demoKeyX', [[false, true], [false, true]]);
    this.logDebug(this.e);
  }
  
  *run() {
    VERIFY_VALUE([[false, true], [false, true]], this.e);
  }
}

module.exports = GetTestDataArrayBooleansEOrig;
