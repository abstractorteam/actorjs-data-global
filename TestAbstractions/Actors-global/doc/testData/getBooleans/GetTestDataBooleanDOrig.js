
const ActorApi = require('actor-api');


class GetTestDataBooleanDOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.d = true;
  }
  
  *data() {
    this.d = this.getTestDataBoolean('demoKeyX', false);
    this.logDebug(this.d);
  }
  
  *run() {
    VERIFY_VALUE(false, this.d);
  }
}

module.exports = GetTestDataBooleanDOrig;
