
const ActorApi = require('actor-api');


class GetTestDataBooleansGOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.g = [];
  }
  
  *data() {
    this.g = this.getTestDataBooleans('demoKeyObject', 'UNKNOWN', {
      ALL_TRUE: [true, true],
      ALL_FALSE: [false, false]
    });
    this.logDebug(this.g);
  }
  
  *run() {
    VERIFY_VALUE([true, true], this.g);
  }
}

module.exports = GetTestDataBooleansGOrig;
