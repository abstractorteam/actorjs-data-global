
const ActorApi = require('actor-api');


class GetTestDataObjectEOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.e = null;
  }
  
  *data() {
    this.e = this.getTestDataObject('demoKeyObject', 'HELSINKI', {
      OSLO: {
        name: 'Oslo',
        population: 1
      },
      HELSINKI: {
        name: 'Helsinki',
        population: 1.3
      }
    });
    this.logDebug(this.e);
  }
  
  *run() {
    VERIFY_VALUE('Oslo', this.e.name);
    VERIFY_VALUE(1, this.e.population);
  }
}

module.exports = GetTestDataObjectEOrig;
