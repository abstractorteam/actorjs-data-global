
const ActorApi = require('actor-api');


class GetTestDataObjectAOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = null;
  }
  
  *data() {
    this.a = this.getTestDataObject('demoKey');
    this.logDebug(this.a);
  }
  
  *run() {
    VERIFY_VALUE('Stockholm', this.a.name);
    VERIFY_VALUE(1.6, this.a.population);
  }
}

module.exports = GetTestDataObjectAOrig;
