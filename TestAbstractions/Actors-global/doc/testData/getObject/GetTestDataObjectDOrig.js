
const ActorApi = require('actor-api');


class GetTestDataObjectDOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = null;
    this.d = null;
  }
  
  *data() {
    this.a = this.getTestDataObject('demoKey');
    this.d = this.getTestDataObject('demoKeyX', () => {
      const x = this.a;
      x.founded = 1252;
      return x;
    });
    this.logDebug(this.a);
    this.logDebug(this.d);
  }
  
  *run() {
    VERIFY_VALUE('Stockholm', this.d.name);
    VERIFY_VALUE(1.6, this.d.population);
    VERIFY_VALUE(1252, this.d.founded);

  }
}

module.exports = GetTestDataObjectDOrig;
