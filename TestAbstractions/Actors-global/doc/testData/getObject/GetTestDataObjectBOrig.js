
const ActorApi = require('actor-api');


class GetTestDataObjectBOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.b = null;
  }
  
  *data() {
    this.b = this.getTestDataObject('demoKey', {
      name: 'Copenhagen',
		  population: 1.3
	  });
    this.logDebug(this.b);
  }
  
  *run() {
    VERIFY_VALUE('Stockholm', this.b.name);
    VERIFY_VALUE(1.6, this.b.population);
  }
}

module.exports = GetTestDataObjectBOrig;
