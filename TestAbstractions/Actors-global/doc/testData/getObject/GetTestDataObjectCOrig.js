
const ActorApi = require('actor-api');


class GetTestDataObjectCOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.c = null;
  }
  
  *data() {
    this.c = this.getTestDataObject('demoKeyX', {
      name: 'Copenhagen',
		  population: 1.3
	  });
    this.logDebug(this.c);
  }
  
  *run() {
    VERIFY_VALUE('Copenhagen', this.c.name);
    VERIFY_VALUE(1.3, this.c.population);
  }
}

module.exports = GetTestDataObjectCOrig;
