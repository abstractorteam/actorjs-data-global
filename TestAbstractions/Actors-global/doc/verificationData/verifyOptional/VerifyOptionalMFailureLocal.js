
const ActorApi = require('actor-api');


class VerifyOptionalMFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }

  *run() {
    VERIFY_OPTIONAL('demoKey8', true, 1);
  }
}

module.exports = VerifyOptionalMFailureLocal;
