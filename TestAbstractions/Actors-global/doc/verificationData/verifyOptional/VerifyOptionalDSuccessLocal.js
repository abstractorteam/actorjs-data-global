
const ActorApi = require('actor-api');


class VerifyOptionalDSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_OPTIONAL('demoKey7', false);
    VERIFY_OPTIONAL('demoKeyX', false);
  }
}

module.exports = VerifyOptionalDSuccessLocal;
