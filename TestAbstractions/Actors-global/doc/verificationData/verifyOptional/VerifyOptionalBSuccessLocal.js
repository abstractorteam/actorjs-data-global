
const ActorApi = require('actor-api');


class VerifyOptionalBSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_OPTIONAL('demoKey3', '33.3');
    VERIFY_OPTIONAL('demoKeyX', '33.3');
  }
}

module.exports = VerifyOptionalBSuccessLocal;
