
const ActorApi = require('actor-api');


class VerifyOptionalTFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
    
  *run() {
    VERIFY_OPTIONAL('demoKey4', [[1, 2], [4, 6]]);
  }
}

module.exports = VerifyOptionalTFailureLocal;
