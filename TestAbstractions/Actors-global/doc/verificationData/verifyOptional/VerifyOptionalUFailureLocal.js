
const ActorApi = require('actor-api');


class VerifyOptionalUFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *data() {
  }
  
  *run() {
    VERIFY_OPTIONAL('demoKey7', [[true, false], [true, false]]);
  }
}

module.exports = VerifyOptionalUFailureLocal;
