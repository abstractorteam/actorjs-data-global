
const ActorApi = require('actor-api');


class VerifyOptionalKFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_OPTIONAL('demoKey6', [1, 2]);
  }
}

module.exports = VerifyOptionalKFailureLocal;
