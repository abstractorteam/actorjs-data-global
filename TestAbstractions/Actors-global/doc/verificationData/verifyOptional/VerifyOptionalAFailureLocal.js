
const ActorApi = require('actor-api');


class VerifyOptionalAFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_OPTIONAL('demoKey2', 'Hello');
  }
}

module.exports = VerifyOptionalAFailureLocal;
