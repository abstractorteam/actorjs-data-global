
const ActorApi = require('actor-api');


class VerifyOptionalASuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_OPTIONAL('demoKey1', 'Hello');
    VERIFY_OPTIONAL('demoKeyX', 'Hello');
  }
}

module.exports = VerifyOptionalASuccessLocal;
