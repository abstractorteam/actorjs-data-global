
const ActorApi = require('actor-api');


class VerifyOptionalSFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_OPTIONAL('demoKey1', [['Hello', 'Hi'], ['Later', 'Bye']]);
  }
}

module.exports = VerifyOptionalSFailureLocal;
