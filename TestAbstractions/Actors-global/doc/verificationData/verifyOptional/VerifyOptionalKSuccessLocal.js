
const ActorApi = require('actor-api');


class VerifyOptionalKSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_OPTIONAL('demoKey3', [1, 2]);
    VERIFY_OPTIONAL('demoKey4', [1, 2]);
    VERIFY_OPTIONAL('demoKey5', [1, 2]);
    VERIFY_OPTIONAL('demoKeyX', [1, 2]);
  }
}

module.exports = VerifyOptionalKSuccessLocal;
