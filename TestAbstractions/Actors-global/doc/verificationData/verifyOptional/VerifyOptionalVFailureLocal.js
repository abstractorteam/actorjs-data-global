
const ActorApi = require('actor-api');


class VerifyOptionalVFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
    
  *run() {
    VERIFY_OPTIONAL('demoKey1', 'Later', 2, 2);

  }
}

module.exports = VerifyOptionalVFailureLocal;
