
const ActorApi = require('actor-api');


class VerifyOptionalBFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_OPTIONAL('demoKey4', '33.3');
  }
}

module.exports = VerifyOptionalBFailureLocal;
