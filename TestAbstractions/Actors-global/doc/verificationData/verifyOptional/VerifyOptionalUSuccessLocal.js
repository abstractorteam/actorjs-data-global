
const ActorApi = require('actor-api');


class VerifyOptionalUSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_OPTIONAL('demoKey7', [[true, true], [false, false]]);
    VERIFY_OPTIONAL('demoKey8', [[true, true], [false, false]]);
    VERIFY_OPTIONAL('demoKeyX', [[true, true], [false, false]]);
  }
}

module.exports = VerifyOptionalUSuccessLocal;
