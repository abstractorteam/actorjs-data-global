
const ActorApi = require('actor-api');


class VerifyOptionalCSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_OPTIONAL('demoKey5', 44.4);
    VERIFY_OPTIONAL('demoKey6', 44.4);
    VERIFY_OPTIONAL('demoKeyX', 44.4);
  }
}

module.exports = VerifyOptionalCSuccessLocal;
