
const ActorApi = require('actor-api');


class VerifyOptionalMSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_OPTIONAL('demoKey1', 'Hello', 0);
    VERIFY_OPTIONAL('demoKey2', 'Hi', 1);
    VERIFY_OPTIONAL('demoKey3', 1, 0);
    VERIFY_OPTIONAL('demoKey5', 2, 1);
    VERIFY_OPTIONAL('demoKey7', true, 0);
  }
}

module.exports = VerifyOptionalMSuccessLocal;
