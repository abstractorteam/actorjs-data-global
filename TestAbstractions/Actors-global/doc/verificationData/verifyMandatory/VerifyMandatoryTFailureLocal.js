
const ActorApi = require('actor-api');


class VerifyMandatoryTFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey6', [[1, 2], [5, 6]]);
  }
}

module.exports = VerifyMandatoryTFailureLocal;
