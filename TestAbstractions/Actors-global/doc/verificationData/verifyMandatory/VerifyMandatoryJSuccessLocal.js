
const ActorApi = require('actor-api');


class VerifyMandatoryJSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey1', ['Hello', 'Hi']); 
    VERIFY_MANDATORY('demoKey2', ['Hello', 'Hi']);
  }
}

module.exports = VerifyMandatoryJSuccessLocal;
