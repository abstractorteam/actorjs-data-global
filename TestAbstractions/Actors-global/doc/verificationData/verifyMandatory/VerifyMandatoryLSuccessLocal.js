
const ActorApi = require('actor-api');


class VerifyMandatoryLSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey7', [true, false]);
    VERIFY_MANDATORY('demoKey8', [true, false]);
  }
}

module.exports = VerifyMandatoryLSuccessLocal;
