
const ActorApi = require('actor-api');


class VerifyMandatoryCFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_MANDATORY('demoKeyX', 44.4);
  }
}

module.exports = VerifyMandatoryCFailureLocal;
