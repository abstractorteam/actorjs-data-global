
const ActorApi = require('actor-api');


class VerifyMandatoryTSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey3', [[1, 2], [5, 6]]);
    VERIFY_MANDATORY('demoKey4', [[1, 2], [5, 6]]);
    VERIFY_MANDATORY('demoKey5', [[1, 2], [5, 6]]);
    VERIFY_MANDATORY('demoKey6', [[5, 6], [1, 2]]);
  }
}

module.exports = VerifyMandatoryTSuccessLocal;
