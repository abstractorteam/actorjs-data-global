
const ActorApi = require('actor-api');


class VerifyMandatoryBFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_MANDATORY('demoKey4', '33.3');
  }
}

module.exports = VerifyMandatoryBFailureLocal;
