
const ActorApi = require('actor-api');


class VerifyMandatoryAFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_MANDATORY('demoKey2', 'Hello');
  }
}

module.exports = VerifyMandatoryAFailureLocal;
