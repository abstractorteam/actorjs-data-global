
const ActorApi = require('actor-api');


class VerifyMandatoryBSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_MANDATORY('demoKey3', '33.3');
  }
}

module.exports = VerifyMandatoryBSuccessLocal;
