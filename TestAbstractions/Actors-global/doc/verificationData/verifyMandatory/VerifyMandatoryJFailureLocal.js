
const ActorApi = require('actor-api');


class VerifyMandatoryJFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {    
    VERIFY_MANDATORY('demoKeyX', ['Hello', 'Hi']);
  }
}

module.exports = VerifyMandatoryJFailureLocal;
