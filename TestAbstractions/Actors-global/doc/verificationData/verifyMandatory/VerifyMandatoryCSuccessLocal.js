
const ActorApi = require('actor-api');


class VerifyMandatoryCSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_MANDATORY('demoKey5', 44.4);
    VERIFY_MANDATORY('demoKey6', 44.4);
  }
}

module.exports = VerifyMandatoryCSuccessLocal;
