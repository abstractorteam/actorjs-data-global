
const ActorApi = require('actor-api');


class VerifyMandatoryVSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey1', 'Later', 1, 1);
    VERIFY_MANDATORY('demoKey2', ['Hello', 'Hi'], 0);
    VERIFY_MANDATORY('demoKey3', 1, 0, 0);
    VERIFY_MANDATORY('demoKey4', [5, 6], 1);
    VERIFY_MANDATORY('demoKey7', false, 1, 1);
    VERIFY_MANDATORY('demoKey8', [false, false], 1);
  }
}

module.exports = VerifyMandatoryVSuccessLocal;
