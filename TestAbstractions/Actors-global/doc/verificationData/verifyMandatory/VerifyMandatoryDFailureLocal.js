
const ActorApi = require('actor-api');


class VerifyMandatoryDFailureLocal extends ActorApi.ActorLocal {
   constructor() {
    super();
  }
  
  *run() { 
    VERIFY_MANDATORY('demoKey8', false);
  }
}

module.exports = VerifyMandatoryDFailureLocal;
