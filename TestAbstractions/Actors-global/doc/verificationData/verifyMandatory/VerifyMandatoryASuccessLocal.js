
const ActorApi = require('actor-api');


class VerifyMandatoryASuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_MANDATORY('demoKey1', 'Hello');
  }
}

module.exports = VerifyMandatoryASuccessLocal;
