
const ActorApi = require('actor-api');


class VerifyMandatoryMFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey8', false, 0);
  }
}

module.exports = VerifyMandatoryMFailureLocal;
