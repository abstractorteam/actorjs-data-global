
const ActorApi = require('actor-api');


class VerifyMandatoryUFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey8', [[true, false], [true, false]]);
  }
}

module.exports = VerifyMandatoryUFailureLocal;
