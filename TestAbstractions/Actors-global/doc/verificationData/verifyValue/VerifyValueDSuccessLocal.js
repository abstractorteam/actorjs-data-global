
const ActorApi = require('actor-api');


class VerifyValueDSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE('1', 1, '', '==');
    VERIFY_VALUE(0, 1, '', '<');
  }
}

module.exports = VerifyValueDSuccessLocal;
