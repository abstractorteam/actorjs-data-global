
const ActorApi = require('actor-api');


class VerifyValueVSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([['1.1', '2.2'], ['3.3', '4.4']], [[1.1, 2.2], [3.3, 4.4]], '', '==');
    VERIFY_VALUE([['1.0', '2.1'], ['3.2', '4.3']], [[1.1, 2.2], [3.3, 4.4]], '', '<');
  }
}

module.exports = VerifyValueVSuccessLocal;
