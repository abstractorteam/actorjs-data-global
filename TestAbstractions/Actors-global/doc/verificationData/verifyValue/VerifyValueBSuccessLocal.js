
const ActorApi = require('actor-api');


class VerifyValueBSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE(32, 32);
  }
}

module.exports = VerifyValueBSuccessLocal;
