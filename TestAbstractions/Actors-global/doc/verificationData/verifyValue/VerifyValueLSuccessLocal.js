
const ActorApi = require('actor-api');


class VerifyValueLSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([true, false], [true, false]);
    VERIFY_VALUE([2 > 1, 2 > 3], [true, false]);
  }
}

module.exports = VerifyValueLSuccessLocal;
