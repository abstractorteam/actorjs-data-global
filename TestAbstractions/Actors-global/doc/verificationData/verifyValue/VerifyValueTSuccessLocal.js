
const ActorApi = require('actor-api');


class VerifyValueTSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([[1, 2], [3, 4]], [[1, 2], [3, 4]]);
  }
}

module.exports = VerifyValueTSuccessLocal;
