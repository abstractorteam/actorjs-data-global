
const ActorApi = require('actor-api');


class VerifyValueASuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE('Hello', 'Hello');
  }
}

module.exports = VerifyValueASuccessLocal;
