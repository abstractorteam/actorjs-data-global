
const ActorApi = require('actor-api');


class VerifyValueVFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([[1.2, 2.3], [3.4, 4.4]], [[1.1, 2.2], [3.3, 4.4]], '', '>');
  }
}

module.exports = VerifyValueVFailureLocal;
