
const ActorApi = require('actor-api');


class VerifyValueSSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([['Hello', 'Hi'], ['Bye', 'Later']], [['Hello', 'Hi'], ['Bye', 'Later']]);
  }
}

module.exports = VerifyValueSSuccessLocal;
