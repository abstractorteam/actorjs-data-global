
const ActorApi = require('actor-api');


class VerifyValueTFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([[3, 4], [1, 2]], [[1, 2], [3, 4]]);
  }
}

module.exports = VerifyValueTFailureLocal;
