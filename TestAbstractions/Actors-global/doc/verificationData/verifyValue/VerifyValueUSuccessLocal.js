
const ActorApi = require('actor-api');


class VerifyValueUSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([[false, false], [false, false]], [[false, false], [false, false]]);
    VERIFY_VALUE([[3 < 2, 2 < 1], [3 == 2, 2 == 1]], [[false, false], [false, false]]);
  }
}

module.exports = VerifyValueUSuccessLocal;
