
const ActorApi = require('actor-api');


class VerifyValueUFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([[3 > 2, 2 > 1], [3 != 2, '1' == 1]], [[false, false], [false, false]]);
  }
}

module.exports = VerifyValueUFailureLocal;
