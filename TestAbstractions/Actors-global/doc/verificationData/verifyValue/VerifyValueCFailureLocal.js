
const ActorApi = require('actor-api');


class VerifyValueCFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super(); 
  }
  
  *run() {
    VERIFY_VALUE(false, true);
  }
}

module.exports = VerifyValueCFailureLocal;
