
const ActorApi = require('actor-api');


class VerifyValueDFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE(2, 1, '', '<');
  }
}

module.exports = VerifyValueDFailureLocal;
