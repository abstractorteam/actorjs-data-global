
const ActorApi = require('actor-api');


class VerifyValueBFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE(31, 32);
  }
}

module.exports = VerifyValueBFailureLocal;
