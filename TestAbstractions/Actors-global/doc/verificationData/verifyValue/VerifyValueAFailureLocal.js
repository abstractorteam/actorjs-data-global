
const ActorApi = require('actor-api');


class VerifyValueAFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE('Hi', 'Hello');
  }
}

module.exports = VerifyValueAFailureLocal;
