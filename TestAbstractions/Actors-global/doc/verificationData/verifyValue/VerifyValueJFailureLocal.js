
const ActorApi = require('actor-api');


class VerifyValueJFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE(['Hi', 'Hello'], ['Hello', 'Hi']);
  }
}

module.exports = VerifyValueJFailureLocal;
