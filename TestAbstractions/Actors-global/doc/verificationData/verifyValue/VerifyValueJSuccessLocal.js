
const ActorApi = require('actor-api');


class VerifyValueJSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE(['Hello', 'Hi'], ['Hello', 'Hi']);
  }
}

module.exports = VerifyValueJSuccessLocal;
