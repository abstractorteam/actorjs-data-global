
const ActorApi = require('actor-api');
const DemoStackApi = require('demo-stack-api');


class DocStackApiTextTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.demoConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.demoConnection = this.createServer('demo');
  }
  
  *run() {
    this.demoConnection.accept();
    const msg = this.demoConnection.receiveText();
    VERIFY_VALUE('INVITE', msg.msgId);
    
     this.demoConnection.sendText(new DemoStackApi.DemoMsg('ACK', 'Hello I am here!'));
  }
  
  *exit() {
    this.closeConnection(this.demoConnection);
  }
}

module.exports = DocStackApiTextTerm;
