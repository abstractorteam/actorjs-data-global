
const ActorApi = require('actor-api');


class DocSharedDataSetIterateValueLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.key = 'DemoKey';
    this.values = [];
  }
  
  *data() {
    this.key = this.getTestDataString('shared-data-set-key', this.key);
    this.values = this.getTestDataNumbers('shared-data-set-values', this.values);
  }
  
  *run() {
    for(let i = 0; i < this.values.length; ++i) {
      VERIFY_VALUE_INDEX(this.values, this.setSharedData(this.key, this.values[i]), i);
    }
  }
}

module.exports = DocSharedDataSetIterateValueLocal;
