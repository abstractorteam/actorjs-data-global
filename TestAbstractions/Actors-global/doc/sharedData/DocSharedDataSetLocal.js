
const ActorApi = require('actor-api');


class DocSharedDataSetLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.key = 'DemoKey';
    this.value = 'DemoValue';
  }
  
  *data() {
    this.key = this.getTestDataString('shared-data-set-key', this.key);
    const setUndefined = this.getTestDataBoolean('shared-data-set-value-undefined', false);
    if(!setUndefined) {
      this.value = this.getTestDataString('shared-data-set-value', this.value);
    }
    else {
      this.value = undefined;
    }
  }
  
  *run() {
    VERIFY_VALUE(this.value, this.setSharedData(this.key, this.value));
  }
}

module.exports = DocSharedDataSetLocal;
