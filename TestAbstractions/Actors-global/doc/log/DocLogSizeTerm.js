
const ActorApi = require('actor-api');


class DocLogSizeTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
 
    let responseSize = this.socketConnection.receiveSize(5);
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = DocLogSizeTerm;
