
const ActorApi = require('actor-api');


class ClientStackSingleUdpTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: 1
    });
  }
  
  *run() {
    this.socketConnection.accept();
    const request = this.socketConnection.receiveLine();
    this.socketConnection.sendLine('CLIENT SERVER');
  }
  
  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ClientStackSingleUdpTerm;
