
const ActorApi = require('actor-api');


class ClientStackDoubleOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection1 = null;
    this.socketConnection2 = null;
  }
    
  *initClient() {
    this.socketConnection1 = this.createConnection('socket');
    this.socketConnection2 = this.createConnection('socket', {
      dstIndex: 1,
      srcIndex: 1
    });
  }
  
  *run() {
  }
  
  *exit() {
    this.closeConnection(this.socketConnection1);
    this.closeConnection(this.socketConnection2);
  }
}

module.exports = ClientStackDoubleOrig;
