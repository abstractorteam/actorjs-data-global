
const ActorApi = require('actor-api');


class ClientStackSingleTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
  }
  
  *exit() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = ClientStackSingleTerm;
