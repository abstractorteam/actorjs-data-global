
const ActorApi = require('actor-api');


class CallbackMemberProxy extends ActorApi.ActorProxy {
  *run() {
    const data = this.callback(this.callbackFunc.bind(this));
    VERIFY_VALUE('Callback member return value.', data);
  }
  
  callbackFunc(cb) {
    cb(undefined, 'Callback member return value.');
  }
}

module.exports = CallbackMemberProxy;
