
const ActorApi = require('actor-api');


class AsyncArrowOrig extends ActorApi.ActorOriginating {
  *run() {
    const data = this.async(async () => {
      return await this._getData();
    });
    VERIFY_VALUE('data', data);
  }
  
  _getData() {
    return new Promise((resolve, reject) => {
      process.nextTick(() => {
        resolve('data');
      });
    });
  }
}

module.exports = AsyncArrowOrig;
