	
const ActorApi = require('actor-api');


class AsyncMemberTerm extends ActorApi.ActorTerminating {
  *run() {
    const data = this.async(this.asyncFunc.bind(this));
    VERIFY_VALUE('data', data);
  }
  
  async asyncFunc() {
    return await this._getData();
  }
  
  _getData() {
    return new Promise((resolve, reject) => {
      process.nextTick(() => {
        resolve('data');
      });
    });
  }
}

module.exports = AsyncMemberTerm;
