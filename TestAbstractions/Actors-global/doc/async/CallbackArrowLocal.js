
const ActorApi = require('actor-api');


class CallbackArrowLocal extends ActorApi.ActorLocal {
  *run() {
    const data = this.callback((cb) => {
      cb(undefined, 'Callback arrow return value.');
    });
    VERIFY_VALUE('Callback arrow return value.', data);
  }
}

module.exports = CallbackArrowLocal;
