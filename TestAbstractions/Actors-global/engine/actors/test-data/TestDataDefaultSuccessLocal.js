
const ActorApi = require('actor-api');


class TestDataDefaultSuccessLocal extends ActorApi.ActorLocal {
  *data() {
    let method = this.getTestDataString('method');
    let defaultValue = this.getTestDataString('defaultValue');
    let defaultType = this.getTestDataString('defaultType');
    switch(method) {
      case 'getTestDataString':
        this.data = this.getTestDataString('data', this._getDefault(defaultValue, defaultType));
        break;
      case 'getTestDataStrings':
        this.data = this.getTestDataStrings('data', this._getDefault(defaultValue, defaultType));
        break;
      case 'getTestDataArrayStrings':
        this.data = this.getTestDataArrayStrings('data', this._getDefault(defaultValue, defaultType));
        break;
      case 'getTestDataNumber':
        this.data = this.getTestDataNumber('data', this._getDefault(defaultValue, defaultType));
        break;
      case 'getTestDataNumbers':
        this.data = this.getTestDataNumbers('data', this._getDefault(defaultValue, defaultType));
        break;
      case 'getTestDataArrayNumbers':
        this.data = this.getTestDataArrayNumbers('data', this._getDefault(defaultValue, defaultType));
        break;
      default:
        break;
    }
  }
  
  *run() {
    VERIFY_MANDATORY('testData', this.data);
  }
  
  _getDefault(defaultValue, defaultType) {
    switch(defaultType) {
      case 'string':
        return defaultValue;
      case '[string]':
        return JSON.parse(defaultValue);
      case '[[string]]':
        return JSON.parse(defaultValue);
      case 'number':
        return parseFloat(defaultValue);
      case '[number]':
        return JSON.parse(defaultValue);
      case '[[number]]':
        return JSON.parse(defaultValue);
    }
  }
}

module.exports = TestDataDefaultSuccessLocal;
