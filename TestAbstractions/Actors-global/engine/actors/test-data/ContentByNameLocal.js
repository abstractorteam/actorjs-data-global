
const ActorApi = require('actor-api');


class ContentByNameLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.contentName = '';
    this.content = null;
  }
  
  *data() {
    this.contentName = this.getTestDataString('content-name');
    this.content = this.getContentByName(this.contentName, 'image');
  }
  
  *run() {
    VERIFY_VALUE(this.content.size, this.content.getLength());
  }
}

module.exports = ContentByNameLocal;
