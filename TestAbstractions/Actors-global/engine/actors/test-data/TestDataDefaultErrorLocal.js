
const ActorApi = require('actor-api');


class TestDataDefaultErrorLocal extends ActorApi.ActorLocal {
  *data() {
    this.method = this.getTestDataString('method');
    switch(this.method) {
      case 'getTestDataString':
        this.data = this.getTestDataString('data');
        break;
      case 'getTestDataStrings':
        this.data = this.getTestDataStrings('data');
        break;
      case 'getTestDataArrayStrings':
        this.data = this.getTestDataArrayStrings('data');
        break;
      case 'getTestDataNumber':
        this.data = this.getTestDataNumber('data');
        break;
      case 'getTestDataNumbers':
        this.data = this.getTestDataNumbers('data');
        break;
      case 'getTestDataArrayNumbers':
        this.data = this.getTestDataArrayNumbers('data');
        break;
    }
  }
  
  *run() {
    VERIFY_MANDATORY('testData', this.data);
  }
}

module.exports = TestDataDefaultErrorLocal;
