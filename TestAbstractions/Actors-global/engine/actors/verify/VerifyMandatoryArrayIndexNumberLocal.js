
const ActorApi = require('actor-api');


class VerifyMandatoryArrayIndexNumberLocal extends ActorApi.ActorLocal {
  *data() {
    this.testDataMandatory = this.getTestDataNumbers('mandatory');
  }
  
  *run() {
    this.testDataMandatory.forEach((number, index) => {
      VERIFY_MANDATORY('mandatory', number, index);
    });
  }
}


module.exports = VerifyMandatoryArrayIndexNumberLocal;
