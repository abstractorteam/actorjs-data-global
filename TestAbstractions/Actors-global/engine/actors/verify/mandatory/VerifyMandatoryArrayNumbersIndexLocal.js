
const ActorApi = require('actor-api');


class VerifyMandatoryArrayNumbersIndexLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataMandatory = [[]];
  }
  
  *data() {
    this.testDataMandatory = this.getTestDataArrayNumbers('mandatory');
  }
  
  *run() {
    this.testDataMandatory.forEach((numbers, index1) => {
      VERIFY_MANDATORY('mandatory', numbers, index1);
      numbers.forEach((number, index2) => {
        VERIFY_MANDATORY('mandatory', number, index1, index2);
      });
    });
  }
}

module.exports = VerifyMandatoryArrayNumbersIndexLocal;
