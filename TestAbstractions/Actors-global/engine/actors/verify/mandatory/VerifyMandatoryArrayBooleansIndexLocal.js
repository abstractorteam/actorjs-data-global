
const ActorApi = require('actor-api');


class VerifyMandatoryArrayBooleansIndexLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataMandatory = [[]];
  }
  
  *data() {
    this.testDataMandatory = this.getTestDataArrayBooleans('mandatory');
    
  }
  
  *run() {
    this.testDataMandatory.forEach((booleans, index1) => {
      VERIFY_MANDATORY('mandatory', booleans, index1);
      booleans.forEach((boolean, index2) => {
        VERIFY_MANDATORY('mandatory', boolean, index1, index2);
      });
    });
  }
}

module.exports = VerifyMandatoryArrayBooleansIndexLocal;
