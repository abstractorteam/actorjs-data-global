
const ActorApi = require('actor-api');


class VerifyMandatoryArrayStringsIndexLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataMandatory = [[]];
  }
  
  *data() {
    this.testDataMandatory = this.getTestDataArrayStrings('mandatory');
  }
  
  *run() {
    this.testDataMandatory.forEach((strings, index1) => {
      VERIFY_MANDATORY('mandatory', strings, index1);
      strings.forEach((string, index2) => {
        VERIFY_MANDATORY('mandatory', string, index1, index2);
      });
    });
  }
}

module.exports = VerifyMandatoryArrayStringsIndexLocal;
