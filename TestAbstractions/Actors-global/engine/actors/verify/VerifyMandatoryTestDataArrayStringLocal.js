
const ActorApi = require('actor-api');


class VerifyMandatoryTestDataArrayStringLocal extends ActorApi.ActorLocal {
  *data() {
    this.testDataMandatory = this.getTestDataStrings('mandatory');
  }
  
  *run() {
    VERIFY_MANDATORY('mandatory', this.testDataMandatory);
  }
}

module.exports = VerifyMandatoryTestDataArrayStringLocal;
