
const ActorApi = require('actor-api');


class VerifyOptionalArrayNumbersIndexLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataOptional = [[]];
  }
  
  *data() {
    this.testDataOptional = this.getTestDataArrayNumbers('optional');
  }
  
  *run() {
    this.testDataOptional.forEach((numbers, index1) => {
      VERIFY_OPTIONAL('optional', numbers, index1);
      numbers.forEach((number, index2)=> {
        VERIFY_OPTIONAL('optional', number, index1, index2);
      });
    });
  }
}

module.exports = VerifyOptionalArrayNumbersIndexLocal;
