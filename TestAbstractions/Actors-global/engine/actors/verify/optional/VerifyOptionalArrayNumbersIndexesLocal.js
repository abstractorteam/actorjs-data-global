
const ActorApi = require('actor-api');


class VerifyOptionalArrayNumbersIndexesLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataOptional = [[]];
    this.index1 = 0;
    this.index2 = 0;
  }
  
  *data() {
    this.testDataOptional = this.getTestDataArrayNumbers('optional');
    this.index1 = this.getTestDataNumber('index1');
    this.index2 = this.getTestDataNumber('index2');
  }
  
  *run() {
    VERIFY_OPTIONAL('optional', this.testDataOptional, this.index1, this.index2);
  }
}

module.exports = VerifyOptionalArrayNumbersIndexesLocal;
