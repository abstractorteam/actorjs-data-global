
const ActorApi = require('actor-api');


class VerifyMandatoryStringDataOrig extends ActorApi.ActorOriginating {
  *data() {
    this.testDataMandatory = this.getTestDataString('mandatory');
  }
  
  *run() {
    VERIFY_MANDATORY('mandatory', this.testDataMandatory);
  }
}

module.exports = VerifyMandatoryStringDataOrig;
