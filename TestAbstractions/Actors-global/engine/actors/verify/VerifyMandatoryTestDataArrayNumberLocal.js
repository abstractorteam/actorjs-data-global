
const ActorApi = require('actor-api');


class VerifyMandatoryTestDataArrayNumberLocal extends ActorApi.ActorLocal {
  *data() {
    this.testDataMandatory = this.getTestDataNumbers('mandatory');
  }
  
  *run() {
    VERIFY_MANDATORY('mandatory', this.testDataMandatory);
  }
}

module.exports = VerifyMandatoryTestDataArrayNumberLocal;
