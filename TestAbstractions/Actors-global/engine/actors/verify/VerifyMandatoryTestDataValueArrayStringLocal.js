
const ActorApi = require('actor-api');


class VerifyMandatoryTestDataValueArrayStringLocal extends ActorApi.ActorLocal {
  *data() {
    this.testDataMandatory = this.getTestDataStrings('mandatory');
  }
  
  *run() {
    VERIFY_MANDATORY('mandatory', this.testDataMandatory);
  }
}

module.exports = VerifyMandatoryTestDataValueArrayStringLocal;
