
const ActorApi = require('actor-api');


class VerifyMandatoryNumberDataOrig extends ActorApi.ActorOriginating {
  *data() {
    this.testDataMandatory = this.getTestDataNumber('mandatory');
  }
  
  *run() {
    VERIFY_MANDATORY('mandatory', this.testDataMandatory);
  }
}

module.exports = VerifyMandatoryNumberDataOrig;
