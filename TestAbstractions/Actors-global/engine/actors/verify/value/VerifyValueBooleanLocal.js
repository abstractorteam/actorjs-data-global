
const ActorApi = require('actor-api');


class VerifyValueBooleanLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.expected = false;
    this.actual = false;
  }
  
  *data() {
    this.expected = this.getTestDataBoolean('expected');
    this.actual = this.getTestDataBoolean('actual');
  }
  
  *run() {
    VERIFY_VALUE(this.expected, this.actual);
  }
}

module.exports = VerifyValueBooleanLocal;
