
const ActorApi = require('actor-api');


class EmptyOrig extends ActorApi.ActorOriginating {
  *data() {
    this.getSharedDataActor('state')[0] += 1;
  }
  
  *initClient() {
    this.getSharedDataActor('state')[2] += 1;
  }
  
  *run() {
    this.getSharedDataActor('state')[3] += 1;
  }
  
  *exit() {
    this.getSharedDataActor('state')[4] += 1;
  }
}

module.exports = EmptyOrig;
