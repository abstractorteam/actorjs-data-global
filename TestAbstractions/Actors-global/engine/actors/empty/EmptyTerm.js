
const ActorApi = require('actor-api');


class EmptyTerm extends ActorApi.ActorTerminating {
  *data() {
    this.getSharedDataActor('state')[0] += 1;
  }
    
  *initServer() {
    this.getSharedDataActor('state')[1] += 1;
  }
  
  *run() {
    this.getSharedDataActor('state')[3] += 1;
  }
  
  *exit() {
    this.getSharedDataActor('state')[4] += 1;
  }
}

module.exports = EmptyTerm;
