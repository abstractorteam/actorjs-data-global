
const ActorApi = require('actor-api');


class EmptyVerifyActorStatesLocal extends ActorApi.ActorLocal {
  *data() {
    this.testDataActorNames = this.getTestDataStrings('execActorNames');
    this.testDataActorInstanceIndicies = this.getTestDataStrings('execActorInstanceIndicies');
  }
  
  *run() {
    const stateControls = [];
    for(let i = 0; i < this.testDataActorNames.length; ++i) {
      stateControls.push(this.getSharedDataActor('state', this.testDataActorNames[i], this.testDataActorInstanceIndicies[i]));
    }
    VERIFY_MANDATORY('stateControl', stateControls);
  }
}

module.exports = EmptyVerifyActorStatesLocal;
