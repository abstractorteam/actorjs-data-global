
const ActorApi = require('actor-api');


class NoneOrig extends ActorApi.ActorOriginating {
  *data() {
  }
  
  *initClient() {
  }
  
  *run() {
  }
  
  *exit() {
  }
}

module.exports = NoneOrig;
