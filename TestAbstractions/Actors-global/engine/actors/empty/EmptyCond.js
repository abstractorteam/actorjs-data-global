
const ActorApi = require('actor-api');


class EmptyCond extends ActorApi.ActorCondition {
  *data() {
    this.getSharedDataActor('state')[0] += 1;
  }
  
  *initClientPre() {
    this.getSharedDataActor('state')[2] += 1;
  }
  
  *runPre() {
    this.getSharedDataActor('state')[3] += 1;
  }
  
  *exitPre() {
    this.getSharedDataActor('state')[4] += 1;
  }
  
  *initClientPost() {
    this.getSharedDataActor('state')[6] += 1;
  }
  
  *runPost() {
    this.getSharedDataActor('state')[7] += 1;
  }
  
  *exitPost() {
    this.getSharedDataActor('state')[8] += 1;
  }
}

module.exports = EmptyCond;
