
const ActorApi = require('actor-api');


class EmptyVerifyActorStatesInitLocal extends ActorApi.ActorLocal {
  *data() {
    this.testDataActorNames = this.getTestDataStrings('execActorNames');
    this.testDataActorInstanceIndicies = this.getTestDataStrings('execActorInstanceIndicies');
    for(let i = 0; i < this.testDataActorNames.length; ++i) {
      this.setSharedDataActor('state', [0, 0, 0, 0, 0], this.testDataActorNames[i], this.testDataActorInstanceIndicies[i]);
    }
  }
}

module.exports = EmptyVerifyActorStatesInitLocal;
