
const ActorApi = require('actor-api');


class EmptyVerifyCondActorStatesInitLocal extends ActorApi.ActorLocal {
  *data() {
    this.testDataActorNames = this.getTestDataStrings('preActorNames');
    this.testDataActorInstanceIndicies = this.getTestDataNumbers('preActorInstanceIndicies');
    this.setSharedDataActorSync('state', [0, 0, 0, 0, 0], this.testDataActorNames[0], this.testDataActorInstanceIndicies[0]);
    for(let i = 1; i < this.testDataActorNames.length; ++i) {
      this.setSharedDataActorSync('state', [0, 0, 0, 0, 0, 0, 0, 0, 0], this.testDataActorNames[i], this.testDataActorInstanceIndicies[i]);
    }
  }
}

module.exports = EmptyVerifyCondActorStatesInitLocal;
