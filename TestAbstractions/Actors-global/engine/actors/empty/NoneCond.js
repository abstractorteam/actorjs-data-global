
const ActorApi = require('actor-api');


class NoneCond extends ActorApi.ActorCondition {
  *data() {
  }
  
  *initClientPre() {
  }
  
  *runPre() {
  }
  
  *exitPre() {
  }
  
  *initClientPost() {
  }
  
  *runPost() {
  }
  
  *exitPost() {
  }
}

module.exports = NoneCond;
