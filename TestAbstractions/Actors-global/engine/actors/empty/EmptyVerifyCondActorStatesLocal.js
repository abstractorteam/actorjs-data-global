
const ActorApi = require('actor-api');


class EmptyVerifyCondActorStatesLocal extends ActorApi.ActorLocal {
  *data() {
    this.testDataActorNames = this.getTestDataStrings('preActorNames');
    this.testDataActorInstanceIndicies = this.getTestDataNumbers('preActorInstanceIndicies');
  }
  
  *run() {
    let stateControls = []
    for(let i = 0; i < this.testDataActorNames.length; ++i) {
      stateControls.push(this.getSharedDataActor('state', this.testDataActorNames[i], this.testDataActorInstanceIndicies[i]));
    }
    VERIFY_MANDATORY('stateControl', stateControls);
    
    let actorResultsArray = [];
    if(this.actors.length >= 2) {
      for(let i = 1; i < this.actors.length - 1; ++i) {
        actorResultsArray.push([]);
        for(let j = 0; j < 5; ++j) {
          actorResultsArray[i - 1].push(this.actors[i].getStateResultName(j, i));
        }
      }
    }
    VERIFY_MANDATORY('stateResult', actorResultsArray);
  }
}

module.exports = EmptyVerifyCondActorStatesLocal;
