
const ActorApi = require('actor-api');


class ThrowLocal extends ActorApi.ActorLocal {
  *data() {
    this.throwState = this.getTestDataString('throwState');
    this.getSharedDataActor('state')[0] += 1;
    if('data' === this.throwState) {
      throw new Error('Simulated exception in data state.');
    }
  }
  
  *run() {
    this.getSharedDataActor('state')[3] += 1;
    if('run' === this.throwState) {
      throw new Error('Simulated exception in run state.');
    }
  }
}

module.exports = ThrowLocal;
