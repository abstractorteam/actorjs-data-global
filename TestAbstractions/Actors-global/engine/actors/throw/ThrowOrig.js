
const ActorApi = require('actor-api');


class ThrowOrig extends ActorApi.ActorOriginating {
  *data() {
    this.throwState = this.getTestDataString('throwState', '');
    this.getSharedDataActor('state')[0] += 1;
    if('data' === this.throwState) {
      throw new Error('Simulated exception in data state.');
    }
  }
  
  *initClient() {
    this.getSharedDataActor('state')[2] += 1;
    if('initClient' === this.throwState) {
      throw new Error('Simulated exception in initClient state.');
    }
  }

  *run() {
    this.getSharedDataActor('state')[3] += 1;
    if('run' === this.throwState) {
      throw new Error('Simulated exception in run state.');
    }
  }
  
  *exit() {
    this.getSharedDataActor('state')[4] += 1;
    if('exit' === this.throwState) {
      throw new Error('Simulated exception in exit state.');
    }
  }
}

module.exports = ThrowOrig;
