
const ActorApi = require('actor-api');


class ThrowTerm extends ActorApi.ActorTerminating {
  *data() {
    this.throwState = this.getTestDataString('throwState');
    this.getSharedDataActor('state')[0] += 1;
    if('data' === this.throwState) {
      throw new Error('Simulated exception in data state.');
    }
  }
  
  *initServer() {
    this.getSharedDataActor('state')[1] += 1;
    if('initServer' === this.throwState) {
      throw new Error('Simulated exception in initServer state.');
    }
  }
  
  *run() {
    this.getSharedDataActor('state')[3] += 1;
    if('run' === this.throwState) {
      throw new Error('Simulated exception in run state.');
    }
  }
  
  *exit() {
    this.getSharedDataActor('state')[4] += 1;
    if('exit' === this.throwState) {
      throw new Error('Simulated exception in exit state.');
    }
  }
}

module.exports = ThrowTerm;
