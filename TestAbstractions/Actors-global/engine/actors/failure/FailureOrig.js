
const ActorApi = require('actor-api');


class FailureOrig extends ActorApi.ActorOriginating {
  *data() {
    this.failState = this.getTestData('failState');
    this.getSharedDataActor('state')[0] += 1;
    if('data' === this.failState) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *initClient() {
    this.getSharedDataActor('state')[2] += 1;
    if('initClient' === this.failState) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *run() {
    this.getSharedDataActor('state')[3] += 1;
    if('run' === this.failState) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *exit() {
    this.getSharedDataActor('state')[4] += 1;
    if('exit' === this.failState) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
}

module.exports = FailureOrig;
