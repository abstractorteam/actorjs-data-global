
const ActorApi = require('actor-api');


class FailureCond extends ActorApi.ActorCondition {
  constructor() {
    super();
    this.failState = '';
    this.failIndices = [1];
    this.pendingState = '';
    this.shouldFail = false;
  }
  
  *data() {
    this.failState = this.getTestDataString('failState');
    this.failIndices = this.getTestDataNumbers('failIndices', [1]);
    this.pendingState = this.getTestDataString('pendingState', '');
    this.shouldFail = -1 !== this.failIndices.findIndex(index => index === this.getInstanceIndex());
    
    this.getSharedDataActor('state')[0] += 1;
    if('data' === this.pendingState && !this.shouldFail) {
      this.delay(0);
    }
    if('data' === this.failState && this.shouldFail) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *initClientPre() {
    this.getSharedDataActor('state')[2] += 1;
    if('initClientPre' === this.pendingState && !this.shouldFail) {
      this.delay(0);
    }
    if('initClientPre' === this.failState && this.shouldFail) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *runPre() {
    this.getSharedDataActor('state')[3] += 1;
    if('runPre' === this.pendingState && !this.shouldFail) {
      this.delay(0);
    }
    if('runPre' === this.failState && this.shouldFail) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *exitPre() {
    this.getSharedDataActor('state')[4] += 1;
    if('exitPre' === this.pendingState && !this.shouldFail) {
      this.delay(0);
    }
    if('exitPre' === this.failState && this.shouldFail) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *initClientPost() {
    this.getSharedDataActor('state')[6] += 1;
    if('initClientPost' === this.pendingState && !this.shouldFail) {
      this.delay(0);
    }
    if('initClientPost' === this.failState && this.shouldFail) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *runPost() {
    this.getSharedDataActor('state')[7] += 1;
    if('runPost' === this.pendingState && !this.shouldFail) {
      this.delay(0);
    }
    if('runPost' === this.failState && this.shouldFail) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *exitPost() {
    this.getSharedDataActor('state')[8] += 1;
    if('exitPost' === this.pendingState && !this.shouldFail) {
      this.delay(0);
    }
    if('exitPost' === this.failState && this.shouldFail) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
}

module.exports = FailureCond;
