
const ActorApi = require('actor-api');


class FailureLocal extends ActorApi.ActorLocal {
  *data() {
    this.failState = this.getTestData('failState');
    this.getSharedDataActor('state')[0] += 1;
    if('data' === this.failState) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *run() {
    this.getSharedDataActor('state')[3] += 1;
    if('run' === this.failState) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
}

module.exports = FailureLocal;
