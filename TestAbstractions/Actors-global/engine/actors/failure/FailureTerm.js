
const ActorApi = require('actor-api');


class FailureTerm extends ActorApi.ActorTerminating {
  *data() {
    this.failState = this.getTestData('failState');
    this.getSharedDataActor('state')[0] += 1;
    if('data' === this.failState) {
      VERIFY_VALUE('ok', 'nok');
    }
  }

  *initServer() {
    this.getSharedDataActor('state')[1] += 1;
    if('initServer' === this.failState) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *run() {
    this.getSharedDataActor('state')[3] += 1;
    if('run' === this.failState) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
  
  *exit() {
    this.getSharedDataActor('state')[4] += 1;
    if('exit' === this.failState) {
      VERIFY_VALUE('ok', 'nok');
    }
  }
}

module.exports = FailureTerm;
