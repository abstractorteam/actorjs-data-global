
const ActorApi = require('actor-api');
const SuccessValuePost = require('./part/SuccessValuePost');


class PostPartOneSuccessTerm extends ActorApi.ActorTerminating {
  constructor() {
    super(SuccessValuePost);
  }

  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PostPartOneSuccessTerm;
