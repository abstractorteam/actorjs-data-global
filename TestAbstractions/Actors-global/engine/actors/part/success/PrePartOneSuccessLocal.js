
const ActorApi = require('actor-api');
const SuccessValuePre = require('./part/SuccessValuePre');


class PrePartOneSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super(SuccessValuePre);
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PrePartOneSuccessLocal;
