
const ActorApi = require('actor-api');
const SuccessValuePre = require('./part/SuccessValuePre');


class PrePartOneSuccessProxy extends ActorApi.ActorProxy {
  constructor() {
    super(SuccessValuePre);
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PrePartOneSuccessProxy;
