
const ActorApi = require('actor-api');
const SuccessValuePost = require('./part/SuccessValuePost');


class PostPartOneSuccessOrig extends ActorApi.ActorOriginating {
  constructor() {
    super(SuccessValuePost);
  }

  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PostPartOneSuccessOrig;
