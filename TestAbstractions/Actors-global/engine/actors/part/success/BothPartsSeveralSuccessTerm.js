
const ActorApi = require('actor-api');
const SuccessValuePre = require('./part/SuccessValuePre');
const SuccessValuePost = require('./part/SuccessValuePost');


class BothPartsSeveralSuccessTerm extends ActorApi.ActorTerminating {
  constructor() {
    super([SuccessValuePre, SuccessValuePre, SuccessValuePost, SuccessValuePost]);
  }

  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = BothPartsSeveralSuccessTerm;
