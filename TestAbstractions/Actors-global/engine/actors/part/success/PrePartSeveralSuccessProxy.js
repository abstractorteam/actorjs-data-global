
const ActorApi = require('actor-api');
const SuccessValuePre = require('./part/SuccessValuePre');


class PrePartSeveralSuccessProxy extends ActorApi.ActorProxy {
  constructor() {
    super([SuccessValuePre, SuccessValuePre, SuccessValuePre, SuccessValuePre]);
  }

  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PrePartSeveralSuccessProxy;
