
const ActorApi = require('actor-api');
const SuccessValuePre = require('./part/SuccessValuePre');


class PrePartOneSuccessOrig extends ActorApi.ActorOriginating {
  constructor() {
    super(SuccessValuePre);
  }

  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PrePartOneSuccessOrig;
