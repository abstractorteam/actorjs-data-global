
const ActorApi = require('actor-api');
const SuccessValuePost = require('./part/SuccessValuePost');


class PostPartSeveralSuccessProxy extends ActorApi.ActorProxy {
  constructor() {
    super([SuccessValuePost, SuccessValuePost, SuccessValuePost, SuccessValuePost]);
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PostPartSeveralSuccessProxy;
