
const ActorApi = require('actor-api');


class SuccessValuePost extends ActorApi.ActorPartPost {
  constructor() {
    super();
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.name}[${this.index}]`);
  }
}

module.exports = SuccessValuePost;
