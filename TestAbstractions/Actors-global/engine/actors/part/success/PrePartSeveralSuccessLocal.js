
const ActorApi = require('actor-api');
const SuccessValuePre = require('./part/SuccessValuePre');


class PrePartSeveralSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super([SuccessValuePre, SuccessValuePre, SuccessValuePre, SuccessValuePre]);
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PrePartSeveralSuccessLocal;
