
const ActorApi = require('actor-api');
const SuccessValuePost = require('./part/SuccessValuePost');


class PostPartSeveralSuccessTerm extends ActorApi.ActorTerminating {
  constructor() {
    super([SuccessValuePost, SuccessValuePost, SuccessValuePost, SuccessValuePost]);
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PostPartSeveralSuccessTerm;
