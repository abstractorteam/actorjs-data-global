
const ActorApi = require('actor-api');
const SuccessValuePre = require('./part/SuccessValuePre');


class PrePartSeveralSuccessOrig extends ActorApi.ActorOriginating {
  constructor() {
    super([SuccessValuePre, SuccessValuePre, SuccessValuePre, SuccessValuePre]);
  }

  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}


module.exports = PrePartSeveralSuccessOrig;
