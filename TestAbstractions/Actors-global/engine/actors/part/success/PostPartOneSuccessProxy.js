
const ActorApi = require('actor-api');
const SuccessValuePost = require('./part/SuccessValuePost');


class PostPartOneSuccessProxy extends ActorApi.ActorProxy {
  constructor() {
    super(SuccessValuePost);
  }

  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PostPartOneSuccessProxy;
