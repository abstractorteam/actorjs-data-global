
const ActorApi = require('actor-api');
const SuccessValuePost = require('./part/SuccessValuePost');


class PostPartSeveralSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super([SuccessValuePost, SuccessValuePost, SuccessValuePost, SuccessValuePost]);
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PostPartSeveralSuccessLocal;
