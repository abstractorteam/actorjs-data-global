
const ActorApi = require('actor-api');
const SuccessValuePost = require('./part/SuccessValuePost');


class PostPartSeveralSuccessOrig extends ActorApi.ActorOriginating {
  constructor() {
    super([SuccessValuePost, SuccessValuePost, SuccessValuePost, SuccessValuePost]);
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PostPartSeveralSuccessOrig;
