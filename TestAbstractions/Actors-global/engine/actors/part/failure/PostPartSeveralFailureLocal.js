
const ActorApi = require('actor-api');
const FailureValuePost = require('./part/FailureValuePost');


class PostPartSeveralFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super([FailureValuePost, FailureValuePost, FailureValuePost, FailureValuePost]);
  }
  
  *data() {
    this.failureIndex = this.getTestDataNumber('failure-index');
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
    VERIFY_VALUE(1, (0 === this.failureIndex ? 2 : 1));
  }
}

module.exports = PostPartSeveralFailureLocal;
