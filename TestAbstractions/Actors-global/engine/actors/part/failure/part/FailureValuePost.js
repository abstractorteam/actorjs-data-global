
const ActorApi = require('actor-api');


class FailureValuePost extends ActorApi.ActorPartPost {
  constructor() {
    super();
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.name}[${this.index}]`);
    VERIFY_VALUE(1, (this.index === this.parent.failureIndex ? 2 : 1));
  }
}

module.exports = FailureValuePost;
