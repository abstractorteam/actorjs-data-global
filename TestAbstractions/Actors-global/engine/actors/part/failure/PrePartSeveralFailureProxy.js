
const ActorApi = require('actor-api');
const FailureValuePre = require('./part/FailureValuePre');


class PrePartSeveralFailureProxy extends ActorApi.ActorProxy {
  constructor() {
    super([FailureValuePre, FailureValuePre, FailureValuePre, FailureValuePre]);
  }

  *data() {
    this.failureIndex = this.getTestDataNumber('failure-index');
  }

  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
    VERIFY_VALUE(1, (0 === this.failureIndex ? 2 : 1));
  }
}

module.exports = PrePartSeveralFailureProxy;
