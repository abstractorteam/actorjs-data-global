
const ActorApi = require('actor-api');
const FailureValuePost = require('./part/FailureValuePost');


class PostPartOneFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super(FailureValuePost);
  }
  
  *data() {
    this.failureIndex = this.getTestDataNumber('failure-index');
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
    VERIFY_VALUE(1, (0 === this.failureIndex ? 2 : 1));
  }
}

module.exports = PostPartOneFailureLocal;
