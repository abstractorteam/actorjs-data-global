
const ActorApi = require('actor-api');
const HttpGetImage200OkResp = require('../messages/HttpGetImage200OkResp');


class GetImageTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.content = null;
  }
    
  *data() {
    this.content = this.getContent('image_name', 'image');
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
    this.httpConnection.send(new HttpGetImage200OkResp(this.content));
  }
  
  *exit() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = GetImageTerm;
