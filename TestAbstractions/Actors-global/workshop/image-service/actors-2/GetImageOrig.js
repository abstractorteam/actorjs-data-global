
const ActorApi = require('actor-api');
const HttpGetImageReq = require('../messages/HttpGetImageReq');


class GetImageOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
    this.requistUri = '/flag_sweden';
    this.accept = 'image/png';
  }
  
  *data() {
    this.requistUri = this.getTestDataString('Request-URI', this.requistUri);
    this.accept = this.getTestDataString('Accept', this.accept);
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpGetImageReq(this.requistUri, this.accept));
    
    const response = this.httpConnection.receive();
  }
  
  *exit() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = GetImageOrig;
