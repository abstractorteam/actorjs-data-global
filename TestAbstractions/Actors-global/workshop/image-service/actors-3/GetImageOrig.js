
const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');
const HttpGetImageReq = require('../messages/HttpGetImageReq');


class GetImageOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
    this.requistUri = '/image.png';
    this.accept = 'image/png';
  }
  
  *data() {
    this.requistUri = this.getTestDataString('Request-URI', this.requistUri);
    this.accept = this.getTestDataString('Accept', this.accept);
  }
  
  *initClient() {
    if(this.getSharedData('LOGIN-INFO', false)) {
      this.waitForSharedData('LOGIN OK');
    }
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpGetImageReq(this.requistUri, this.accept));
    
    const response = this.httpConnection.receive();
    VERIFY_VALUE(HttpApi.StatusCode.OK, response.statusCode, ' HTTP response line status code:');
    VERIFY_OPTIONAL('content-type', response.getHeader(HttpApi.Header.CONTENT_TYPE));
    VERIFY_CONTENT_OPTIONAL('content-name', response.getHeaderNumber(HttpApi.Header.CONTENT_LENGTH), response.getBody());
  }
  
  *exit() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = GetImageOrig;
