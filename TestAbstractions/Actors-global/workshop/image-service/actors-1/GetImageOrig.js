
const ActorApi = require('actor-api');
const HttpGetImageReq = require('../messages/HttpGetImageReq');


class GetImageOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpGetImageReq('/sweden.png', 'image/png'));
    const response = this.httpConnection.receive();
  }
  
  *exit() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = GetImageOrig;
