
const ActorApi = require('actor-api');
const DiameterApi = require('diameter-stack-api');
const UserAuthorizationAnswer = require('../messages/UserAuthorizationAnswer');


class LoginTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.diameterConnection = null;
  }

  *initServer() {
    this.diameterConnection = this.createServer('diameter-cx');
  }
  
  *run() {
    this.diameterConnection.accept();
    
    const request = this.diameterConnection.receive();
    this.diameterConnection.send(new UserAuthorizationAnswer(DiameterApi.DiameterConstResultCodes.DIAMETER_SUCCESS));
  }
  
  *exit() {
    this.closeConnection(this.diameterConnection);
  }
}

module.exports = LoginTerm;
