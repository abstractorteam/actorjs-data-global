
const ActorApi = require('actor-api');
const HttpGetImage200OkResp = require('../messages/HttpGetImage200OkResp');


class GetImageTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
  }
  
  *exit() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = GetImageTerm;
