
'use strict';

const DiameterCxApi = require('diameter-cx-stack-api');


class UserAuthorizationRequest extends DiameterCxApi.msg.UserAuthorizationRequest {
  constructor(userName) {
    super(16777216);
    this.addAvpUserName(userName);
  }
}

module.exports = UserAuthorizationRequest;
