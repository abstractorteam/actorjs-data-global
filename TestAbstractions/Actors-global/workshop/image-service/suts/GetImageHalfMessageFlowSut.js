
const ActorApi = require('actor-api');


class GetImageHalfMessageFlowSut extends ActorApi.ActorSut {
  constructor() {
    super();
    this.httpClientConnection = null;
    this.httpServerConnection = null;
  }
  
  *initServer() {
    this.httpServerConnection = this.createServer('http');
  }
  
  *initClient() {
    this.httpClientConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpServerConnection.accept();
    const request = this.httpServerConnection.receive();
    this.httpClientConnection.send(request);
  }
  
  *exit() {
    this.closeConnection(this.httpServerConnection);
    this.closeConnection(this.httpClientConnection);
  }
}

module.exports = GetImageHalfMessageFlowSut;
