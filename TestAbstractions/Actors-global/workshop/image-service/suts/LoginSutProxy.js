
const ActorApi = require('actor-api');
const RadiusAccessAccept = require('../messages/RadiusAccessAccept');
const UserAuthorizationRequest = require('../messages/UserAuthorizationRequest');


class LoginSutProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.radiusServerConnection = null;
    this.diameterClientConnection = null;
  }
  
  *initServer() {
    this.radiusServerConnection = this.createServer('radius');
  }
  
  *initClient() {
    this.diameterClientConnection = this.createConnection('diameter-cx');
  }
  
  *run() {
    this.radiusServerConnection.accept();
    
    const request = this.radiusServerConnection.receive();
    this.diameterClientConnection.send(new UserAuthorizationRequest('Gunnar'));
    const response = this.diameterClientConnection.receive();
    this.radiusServerConnection.send(new RadiusAccessAccept());
  }
  
  *exit() {
    this.closeConnection(this.diameterClientConnection);
    this.closeConnection(this.radiusServerConnection);
  }
}

module.exports = LoginSutProxy;
