
const ActorApi = require('actor-api');


class GetImageSut extends ActorApi.ActorSut {
  constructor() {
    super();
    this.httpClientConnection = null;
    this.httpServerConnection = null;
  }
  
  *initServer() {
    this.httpServerConnection = this.createServer('http');
  }
  
  *initClient() {
    this.httpClientConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpServerConnection.accept();
    const request = this.httpServerConnection.receive();
    this.httpClientConnection.send(request);
    
    const response = this.httpClientConnection.receive();
    this.httpServerConnection.send(response);
  }
  
  *exit() {
    this.closeConnection(this.httpServerConnection);
    this.closeConnection(this.httpClientConnection);
  }
}

module.exports = GetImageSut;
