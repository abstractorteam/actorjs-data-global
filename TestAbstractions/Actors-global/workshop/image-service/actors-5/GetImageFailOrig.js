
const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');
const HttpGetImageReq = require('../messages/HttpGetImageReq');


class GetImageFailOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
    this.requistUri = '/image.png';
    this.accept = 'image/png';
  }
  
  *data() {
    this.requistUri = this.getTestDataString('Request-URI', this.requistUri);
    this.accept = this.getTestDataString('Accept', this.accept);
  }
  
  *initClient() {
    if(this.getSharedData('LOGIN-INFO', false)) {
      this.waitForSharedData('LOGIN OK');
    }
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpGetImageReq(this.requistUri, this.accept));
    
    const response = this.httpConnection.receive();
    VERIFY_MANDATORY('status-code', response.statusCode);
    VERIFY_OPTIONAL('reason-phrase', response.reasonPhrase);
  }
  
  *exit() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = GetImageFailOrig;
