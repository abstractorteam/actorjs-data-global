
const ActorApi = require('actor-api');
const HttpGetImageErrorResp = require('../messages/HttpGetImageErrorResp');


class GetImageFailTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.statusCode = 0;
    this.reasonPhrase = '';
  }
    
  *data() {
    this.statusCode = this.getTestDataNumber('status-code');
    this.reasonPhrase = this.getTestDataString('reason-phrase');
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
    this.httpConnection.send(new HttpGetImageErrorResp(this.statusCode, this.reasonPhrase));
  }
  
  *exit() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = GetImageFailTerm;
