
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class MulticastOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.mcConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.mcConnection = this.createConnection('socket', {
      networkType: StackApi.NetworkType.MC
    });
  }
  
  *run() {
    const req = this.mcConnection.receiveLine();
  }
  
  *exit() {
    this.closeConnection(this.mcConnection);
  }
}

module.exports = MulticastOrig;
