
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class MulticastTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.mcConnection = null;
  }
  
  *data() {
  }
  
  *initServer() {
    this.mcConnection = this.createServer('socket', {
      networkType: StackApi.NetworkType.MC
    });
  }
  
  *run() {
    this.mcConnection.accept();
    this.mcConnection.sendLine('HELLO');
  }
  
  *exit() {
    this.closeConnection(this.mcConnection);
  }
}

module.exports = MulticastTerm;
