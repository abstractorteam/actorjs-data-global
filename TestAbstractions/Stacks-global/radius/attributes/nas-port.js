
'use strict';

const RadiusInteger = require('../attribute-types/radius-integer');


class NasPort extends RadiusInteger {
  constructor(value) {
    super(5, value);
  }
}

module.exports = NasPort;
