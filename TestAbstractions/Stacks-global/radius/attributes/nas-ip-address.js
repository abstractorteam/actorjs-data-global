
'use strict';

const RadiusAddress = require('../attribute-types/radius-address');


class NasIpAddress extends RadiusAddress {
  constructor(value) {
    super(4, value);
  }
}

module.exports = NasIpAddress;
