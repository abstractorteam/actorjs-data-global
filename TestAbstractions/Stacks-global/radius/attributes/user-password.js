
'use strict';

const RadiusString = require('../attribute-types/radius-string');


class UserPassword extends RadiusString {
  constructor(value) {
    super(2, value);
  }
}

module.exports = UserPassword;
