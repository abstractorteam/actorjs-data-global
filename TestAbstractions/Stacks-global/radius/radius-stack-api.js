
'use strict';

const RadiusAttributeHeader = require('./radius-attribute-header');

const RadiusAddress = require('./attribute-types/radius-address');
const RadiusInteger = require('./attribute-types/radius-integer');
const RadiusString = require('./attribute-types/radius-string');

const UserName = require('./attributes/user-name');

const AccessRequest = require('./msg/access-request');
const AccessAccept = require('./msg/access-accept');

const RadiusStyle = require('./radius-style');


const RadiusApi = {
  attribute: {
    UserName: UserName,
  },
  msg: {
    AccessRequest: AccessRequest,
    AccessAccept: AccessAccept
  },
  Style: RadiusStyle
};

module.exports = RadiusApi;
