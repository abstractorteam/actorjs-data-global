
'use strict';


class RadiusStyle {
  constructor(index) {
    this.index = index;
    this.textColor= 'black';
    this.textProtocolColor = 'black';
    this.protocolColor = '#FFA07A';
    this.protocolBackgroundColor = 'rgba(240, 128, 128, 0.3)';
  }
}

module.exports = RadiusStyle;
