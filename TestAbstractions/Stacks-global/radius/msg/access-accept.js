
'use strict';

const RadiusMsg = require('../radius-msg');
const RadiusConstCode = require('../radius-const-code');


class AccessAccept extends RadiusMsg {
  constructor(identifier, authenticator, length=-1) {
    super(RadiusConstCode.ACCESS_ACCEPT, identifier, authenticator, length);
    this.msg = 'Access-Accept';
  }
}

module.exports = AccessAccept;
