
'use strict';

const RadiusMsg = require('../radius-msg');
const RadiusConstCode = require('../radius-const-code');


class AccessRChallenge extends RadiusMsg {
  constructor(identifier, authenticator, length=-1) {
    super(RadiusConstCode.ACCESS_CHALLENGE, identifier, authenticator, length);
    this.msg = 'Access-Challenge';
  }
}

module.exports = AccessChallenge;
