
'use strict';

const StackApi = require('stack-api');
const RadiusConnectionClientOptions = require('./radius-connection-client-options');
const RadiusEncoder = require('./radius-encoder');
const RadiusDecoder = require('./radius-decoder');


class RadiusConnectionClient extends StackApi.ClientConnection {
  constructor(id, actor, connectionOptions, name = 'radius') {
    super(id, actor, name, StackApi.NetworkType.UDP, connectionOptions, RadiusConnectionClientOptions);
    this.encoder = null;
  }
  
  send(msg) {
    this.sendMessage(new RadiusEncoder(msg));
  }
  
  receive() {
    this.receiveMessage(new RadiusDecoder());
  }
}

module.exports = RadiusConnectionClient;
