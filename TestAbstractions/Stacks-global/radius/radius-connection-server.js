
'use strict';

const StackApi = require('stack-api');
const RadiusConnectionServerOptions = require('./radius-connection-server-options');
const RadiusEncoder = require('./radius-encoder');
const RadiusDecoder = require('./radius-decoder');


class RadiusConnectionServer extends StackApi.ServerConnection {
  constructor(id, actor, connectionOptions) {
    super(id, actor, 'radius', StackApi.NetworkType.UDP, connectionOptions, RadiusConnectionServerOptions);
  }
  
  receive() {
    this.receiveMessage(new RadiusDecoder());
  }
  
  send(msg) {
    this.sendMessage(new RadiusEncoder(msg));
  }
}

module.exports = RadiusConnectionServer;
