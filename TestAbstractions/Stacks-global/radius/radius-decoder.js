
'use strict';

const StackApi = require('stack-api');
//const SocketLoggerLine = require('./socket-logger-line');


class RadiusDecoder extends StackApi.Decoder {
  constructor(isLogIp) {
    super(isLogIp);
  }
  
  *decode() {
    const msg = yield* this.receiveLine();
    if(this.isLogIp) {
      process.nextTick(() => {
        this.caption = msg;
        this.logMessage();
      });
    }
    return {
      msg: msg
    };
  }
}

module.exports = RadiusDecoder;
