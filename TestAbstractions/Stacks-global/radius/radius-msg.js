
'use strict';

//       0                   1                   2                   3
//       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//       |     Code      |  Identifier   |            Length             |
//       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//       |                                                               |
//       |                         Authenticator                         |
//       |                                                               |
//       |                                                               |
//       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//       |  Attributes ...
//       +-+-+-+-+-+-+-+-+-+-+-+-+-

class RadiusMsg {
  constructor(code, identifier, authenticator, length) {
    this.code = code;
    this.identifier = identifier;
    this.length = 20;
    this.fakeLength = length;
    this.authenticator = authenticator;
    this.attributes = new Map();
  }

  addAttributes(attributeFactory, value) {
    if(undefined !== value) {
      if(Array.isArray(value)) {
        value.forEach((val) => {
          this._addAttribute(new attributeFactory(val));
        });
      }
      else {
        this._addAttribute(new attributeFactory(value));
      }
    }
  }
  
  _addAttribute(attribute) {
    const name = attribute.constructor.name;
    if(this.attributes.has(name)) {
      this.attributes.get(name).push(attribute);
    }
    else {
      this.attributes.set(name, [attribute]);    
    }
    this.messageLength += attribute.length;
  }
}

module.exports = RadiusMsg;
