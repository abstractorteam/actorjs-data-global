
'use strict';

const RadiusAttributeHeader = require('../radius-attribute-header');


class RadiusAddress extends RadiusAttributeHeader {
  constructor(type, value) {
    super(type, 6, value);
  }
}

module.exports = RadiusAddress;
