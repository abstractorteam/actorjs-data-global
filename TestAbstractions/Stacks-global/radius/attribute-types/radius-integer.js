
'use strict';

const RadiusAttributeHeader = require('../radius-attribute-header');


class RadiusInteger extends RadiusAttributeHeader {
  constructor(type, value) {
    super(type, 4, value);
  }
}

module.exports = RadiusInteger;
