
'use strict';

const StackApi = require('stack-api');
const SomeipConst = require('./someip-const');
const SomeipMsg = require('./someip-msg');


class SomeipDecoder extends StackApi.Decoder {
  constructor(connection, isLogIp) {
    super(connection, isLogIp);
    this.msg = new SomeipMsg();
  }
  
  *decode() {
    let msg = yield* this.receiveSize(8);
    this.msg.serviceId = msg.readUInt16BE(0);
    this.msg.methodIdEventId = msg.readUInt16BE(2);
    this.msg.length = msg.readUInt32BE(4);    
    let body = yield* this.receiveSize(this.msg.length);
    this.msg.clientId = body.readUInt16BE(0);
    this.msg.sessionId = body.readUInt16BE(2);
    this.msg.protocolVersion = body.readUInt8(4);
    this.msg.interfaceVersion = body.readUInt8(5);
    this.msg.messageType = body.readUInt8(6);
    this.msg.returnCode = body.readUInt8(7);
    this.msg.payload = body.slice(8);
    if(this.isLogIp) {
      process.nextTick(() => {
        this.log();
      });
    }
    return this.msg;
  }
    
  log() {
    let headersInnerLog = new StackApi.LogInner(`SOMEIP - [header]`);
    this.ipLogs.push(headersInnerLog);
    let messageIdLog = headersInnerLog.add(new StackApi.LogInner(`Message ID [32bit]: 0x${this.msg.serviceId.toString(16).padStart(4, '0')} ${this.msg.methodIdEventId.toString(16).padStart(4, '0')} = ${(0x10000 * this.msg.serviceId + this.msg.methodIdEventId).toString()}`));
    messageIdLog.add(new StackApi.LogInner(`Service ID [16bit]: 0x${this.msg.serviceId.toString(16).padStart(4, '0')} = ${this.msg.serviceId}`));
    let methodIdLog = messageIdLog.add(new StackApi.LogInner(`Method ID [16bit]: 0x${this.msg.methodIdEventId.toString(16).padStart(4, '0')} = ${this.msg.methodIdEventId}`));
    let firstBit = 0x8000 & this.msg.methodIdEventId;
    methodIdLog.add(new StackApi.LogInner(`[1bit]: 0x${firstBit} - ${0 === firstBit ? 0 : 1} - ${0 === firstBit ? 'Method' : 'Event'}`));
    let methodIdEventId15Bit = this.msg.methodIdEventId & 0x7fff;
    methodIdLog.add(new StackApi.LogInner(`[15bit]: 0x${(methodIdEventId15Bit).toString(16).padStart(4, '0')} = ${methodIdEventId15Bit}`));
    
    headersInnerLog.add(new StackApi.LogInner(`Length [32bit]: 0x${this.msg.length.toString(16).padStart(8, '0')} = ${this.msg.length}`));

    let requestIdLog = headersInnerLog.add(new StackApi.LogInner(`Request ID [32bit]: 0x${this.msg.clientId.toString(16).padStart(4, '0')} ${this.msg.sessionId.toString(16).padStart(4, '0')} = ${(0x10000 * this.msg.clientId + this.msg.sessionId).toString()}`));
    requestIdLog.add(new StackApi.LogInner(`ClientId ID [16bit]: 0x${this.msg.clientId.toString(16).padStart(4, '0')} = ${this.msg.clientId}`));
    requestIdLog.add(new StackApi.LogInner(`Session ID [16bit]: 0x${this.msg.sessionId.toString(16).padStart(4, '0')} = ${this.msg.sessionId}`));

    headersInnerLog.add(new StackApi.LogInner(`Protocol Version [8bit]: 0x${this.msg.protocolVersion.toString(16)} = ${this.msg.protocolVersion.toString()}`));
    headersInnerLog.add(new StackApi.LogInner(`Interface Version [8bit]: 0x${this.msg.interfaceVersion.toString(16)} = ${this.msg.interfaceVersion.toString()}`));
    headersInnerLog.add(new StackApi.LogInner(`Message Type [8bit]: 0x${this.msg.messageType.toString(16)} = ${this.msg.messageType.toString()} = ${SomeipConst.getMessageTypeConst(this.msg.messageType)}`));
    headersInnerLog.add(new StackApi.LogInner(`Return code [8bit]: 0x${this.msg.returnCode.toString(16)} = ${this.msg.returnCode.toString()} = ${SomeipConst.getReturnCodeConst(this.msg.returnCode)}`));
    
    let headersPayloadLog = new StackApi.LogInner(`SOMEIP - [payload]`);
    this.ipLogs.push(headersPayloadLog);
    headersPayloadLog.add(new StackApi.LogInner(this.msg.payload.toString()));

    this.logMessage();
  }
}

module.exports = SomeipDecoder;
