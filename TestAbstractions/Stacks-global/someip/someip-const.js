

class SomeipConst {
  constructor() {
    
  }
  
  static getMessageTypeConst(messageType) {
    switch(messageType) {
      case SomeipConst.MSG_TYPE__REQUEST:
        return 'REQUEST';
      case SomeipConst.MSG_TYPE__REQUEST_NO_RETURN:
        return 'REQUEST_NO_RETURN';
      case SomeipConst.MSG_TYPE__NOTIFICATION:
        return 'NOTIFICATION';
      case SomeipConst.MSG_TYPE__RESPONSE:
        return 'RESPONSE';
      case SomeipConst.MSG_TYPE__ERROR:
        return 'ERROR';
      case SomeipConst.MSG_TYPE__TP_REQUEST:
        return 'TP_REQUEST';
      case SomeipConst.MSG_TYPE__TP_REQUEST_NO_RETURN:
        return 'TP_REQUEST_NO_RETURN';
      case SomeipConst.MSG_TYPE__TP_NOTIFICATION:
        return 'TP_NOTIFICATION';
      case SomeipConst.MSG_TYPE__TP_RESPONSE:
        return 'TP_RESPONSE';
      case SomeipConst.MSG_TYPE__TP_ERROR:
        return 'TP_ERROR';
      default:
        return 'UNKNOWN MESSAGE TYPE';
    };
  }
  
  static getReturnCodeConst(returnCode) {
    switch(returnCode) {
      case SomeipConst.E_OK:
        return 'E_OK';
      case SomeipConst.E_NOT_OK:
        return 'E_NOT_OK';
      case SomeipConst.E_UNKNOWN_SERVICE:
        return 'E_UNKNOWN_SERVICE';
      case SomeipConst.E_UNKNOWN_METHOD:
        return 'E_UNKNOWN_METHOD';
      case SomeipConst.E_NOT_READY:
        return 'E_NOT_READY';
      case SomeipConst.E_NOT_REACHABLE:
        return 'E_NOT_REACHABLE';
      case SomeipConst.E_TIMEOUT:
        return 'E_TIMEOUT';
      case SomeipConst.E_WRONG_PROTOCOL_VERSION:
        return 'E_WRONG_PROTOCOL_VERSION';
      case SomeipConst.E_WRONG_INTERFACE_VERSION:
        return 'E_WRONG_INTERFACE_VERSION';
      case SomeipConst.E_MALFORMED_MESSAGE:
        return 'E_MALFORMED_MESSAGE';
      case SomeipConst.E_WRONG_MESSAGE_TYPE:
        return 'E_WRONG_MESSAGE_TYPE';
      default:
        return 'UNKNOWN RETURN CODE';
    };
  }
}

SomeipConst.MSG_TYPE__REQUEST = 0x00;
SomeipConst.MSG_TYPE__REQUEST_NO_RETURN = 0x01;
SomeipConst.MSG_TYPE__NOTIFICATION = 0x02;
SomeipConst.MSG_TYPE__RESPONSE = 0x80;
SomeipConst.MSG_TYPE__ERROR = 0x81;

SomeipConst.MSG_TYPE__TP_REQUEST = 0x20;
SomeipConst.MSG_TYPE__TP_REQUEST_NO_RETURN = 0x21;
SomeipConst.MSG_TYPE__TP_NOTIFICATION = 0x22;
SomeipConst.MSG_TYPE__TP_RESPONSE = 0x23;
SomeipConst.MSG_TYPE__TP_ERROR = 0x24;

SomeipConst.E_OK = 0x00;
SomeipConst.E_NOT_OK = 0x01;
SomeipConst.E_UNKNOWN_SERVICE = 0x02;
SomeipConst.E_UNKNOWN_METHOD = 0x03;
SomeipConst.E_NOT_READY = 0x04;
SomeipConst.E_NOT_REACHABLE = 0x05;
SomeipConst.E_TIMEOUT = 0x06;
SomeipConst.E_WRONG_PROTOCOL_VERSION = 0x07;
SomeipConst.E_WRONG_INTERFACE_VERSION = 0x08;
SomeipConst.E_MALFORMED_MESSAGE = 0x09;
SomeipConst.E_WRONG_MESSAGE_TYPE = 0x0a;


module.exports = SomeipConst;
