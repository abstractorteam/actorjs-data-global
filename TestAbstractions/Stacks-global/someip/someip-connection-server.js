
'use strict';

const StackApi = require('stack-api');
const SomeipConnectionServerOptions = require('./someip-connection-server-options');
const SomeipEncoder = require('./someip-encoder');
const SomeipDecoder = require('./someip-decoder');


class SomeipConnectionServer extends StackApi.ServerConnection {
  constructor(id, actor, connectionOptions) {
    super(id, actor, 'someip', StackApi.NetworkType.TCP, connectionOptions, SomeipConnectionServerOptions);
  }
  
  send(msg) {
    this.sendMessage(new SomeipEncoder(msg));
  }
  
  receive() {
    this.receiveMessage(new SomeipDecoder());
  }
}

module.exports = SomeipConnectionServer;
