
'use strict';

const StackApi = require('stack-api');
const SomeipConnectionClientOptions = require('./someip-connection-client-options');
const SomeipEncoder = require('./someip-encoder');
const SomeipDecoder = require('./someip-decoder');


class SomeipConnectionClient extends StackApi.ClientConnection {
  constructor(id, actor, connectionOptions) {
    super(id, actor, 'someip', StackApi.NetworkType.TCP, connectionOptions, SomeipConnectionClientOptions);
  }
  
  send(msg) {
    this.sendMessage(new SomeipEncoder(msg));
  }
  
  receive() {
    this.receiveMessage(new SomeipDecoder());
  }
}

module.exports = SomeipConnectionClient;
