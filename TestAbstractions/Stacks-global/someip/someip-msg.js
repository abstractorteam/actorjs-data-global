
'use strict';


class SomeipMsg {
  constructor(serviceId, methodIdEventId, length, clientId, sessionId, protocolVersion, interfaceVersion, messageType, returnCode, payload) {
    this.serviceId = serviceId;
    this.methodIdEventId = methodIdEventId;
    this.length = length;
    this.clientId = clientId;
    this.sessionId = sessionId;
    this.protocolVersion = protocolVersion;
    this.interfaceVersion = interfaceVersion;
    this.messageType = messageType;
    this.returnCode = returnCode;
    this.payload = payload;
  }
}

module.exports = SomeipMsg;
