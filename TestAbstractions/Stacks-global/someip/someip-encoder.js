
'use strict';

const StackApi = require('stack-api');
const SomeipConst = require('./someip-const');


class SomeipEncoder extends StackApi.Encoder {
  constructor(msg) {
    super();
    this.msg = msg;
    this.buffer = null;
  }
  
  *encode() {
    this.buffer = Buffer.allocUnsafe(8 + this.msg.length);
    if(!this.isLogIp) {
      yield* this._encode();
    }
    else {
      yield* this._encodeAndLog();
      this.logMessage();
    }
  }
  
  *_encode() {
    const msg = this.msg;
    const buffer = this.buffer;
    buffer.writeUInt16BE(this.getServiceId(), 0);
    buffer.writeUInt16BE(msg.methodIdEventId, 2);
    buffer.writeUInt32BE(msg.length, 4);
    buffer.writeUInt16BE(this.getClientId(), 8);
    buffer.writeUInt16BE(msg.sessionId, 10);
    buffer.writeUInt8(this.getProtocolVersion(), 12);
    buffer.writeUInt8(this.getInterfaceVersion(), 13);
    buffer.writeUInt8(msg.messageType, 14);
    buffer.writeUInt8(msg.returnCode, 15);
    buffer.write(msg.payload, 16);
    yield* this.send(buffer);
  }
    
  *_encodeAndLog() {
    let msg = this.msg;
    let buffer = this.buffer;
    let serviceId = this.getServiceId();
    let clientId = this.getClientId();
    let protocolVersion = this.getProtocolVersion();
    let interfaceVersion = this.getInterfaceVersion();
    buffer.writeUInt16BE(serviceId, 0);
    buffer.writeUInt16BE(msg.methodIdEventId, 2);
    buffer.writeUInt32BE(msg.length, 4);
    buffer.writeUInt16BE(clientId, 8);
    buffer.writeUInt16BE(msg.sessionId, 10);
    buffer.writeUInt8(protocolVersion, 12);
    buffer.writeUInt8(interfaceVersion, 13);
    buffer.writeUInt8(msg.messageType, 14);
    buffer.writeUInt8(msg.returnCode, 15);
    buffer.write(msg.payload, 16);
    yield* this.send(buffer);
    
    process.nextTick(() => {
      let headersInnerLog = new StackApi.LogInner(`SOMEIP - [header]`);
      this.ipLogs.push(headersInnerLog);
      let messageIdLog = headersInnerLog.add(new StackApi.LogInner(`Message ID [32bit]: 0x${serviceId.toString(16).padStart(4, '0')} ${msg.methodIdEventId.toString(16).padStart(4, '0')} = ${(0x10000 * serviceId + msg.methodIdEventId).toString()}`));
      messageIdLog.add(new StackApi.LogInner(`Service ID [16bit]: 0x${serviceId.toString(16).padStart(4, '0')} = ${serviceId}`));
      let methodIdLog = messageIdLog.add(new StackApi.LogInner(`Method ID [16bit]: 0x${msg.methodIdEventId.toString(16).padStart(4, '0')} = ${msg.methodIdEventId}`));
      let firstBit = 0x8000 & msg.methodIdEventId;
      methodIdLog.add(new StackApi.LogInner(`[1bit]: 0x${firstBit} - ${0 === firstBit ? 0 : 1} - ${0 === firstBit ? 'Method' : 'Event'}`));
      let methodIdEventId15Bit = msg.methodIdEventId & 0x7fff;
      methodIdLog.add(new StackApi.LogInner(`[15bit]: 0x${(methodIdEventId15Bit).toString(16).padStart(4, '0')} = ${methodIdEventId15Bit}`));
    
      headersInnerLog.add(new StackApi.LogInner(`Length [32bit]: 0x${msg.length.toString(16).padStart(8, '0')} = ${msg.length}`));
    
      let requestIdLog = headersInnerLog.add(new StackApi.LogInner(`Request ID [32bit]: 0x${clientId.toString(16).padStart(4, '0')} ${msg.sessionId.toString(16).padStart(4, '0')} = ${(0x10000 * clientId + msg.sessionId).toString()}`));
      requestIdLog.add(new StackApi.LogInner(`ClientId ID [16bit]: 0x${clientId.toString(16).padStart(4, '0')} = ${clientId}`));
      requestIdLog.add(new StackApi.LogInner(`Session ID [16bit]: 0x${msg.sessionId.toString(16).padStart(4, '0')} = ${msg.sessionId}`));

      headersInnerLog.add(new StackApi.LogInner(`Protocol Version [8bit]: 0x${protocolVersion.toString(16)} = ${protocolVersion.toString()}`));
      headersInnerLog.add(new StackApi.LogInner(`Interface Version [8bit]: 0x${interfaceVersion.toString(16)} = ${interfaceVersion.toString()}`));
      headersInnerLog.add(new StackApi.LogInner(`Message Type [8bit]: 0x${msg.messageType.toString(16)} = ${msg.messageType.toString()} = ${SomeipConst.getMessageTypeConst(msg.messageType)}`));
      headersInnerLog.add(new StackApi.LogInner(`Return code [8bit]: 0x${msg.returnCode.toString(16)} = ${msg.returnCode.toString()} = ${SomeipConst.getReturnCodeConst(msg.returnCode)}`));
    
      let headersPayloadLog = new StackApi.LogInner(`SOMEIP - [payload]`);
      this.ipLogs.push(headersPayloadLog);
      headersPayloadLog.add(new StackApi.LogInner(msg.payload));
    });
  }
  
  getServiceId() {
    if(undefined != this.msg.serviceId) {
      return this.msg.serviceId;
    }
    else if(null != this.connectionOptions && undefined != this.connectionOptions.serviceId) {
      return this.connectionOptions.serviceId;
    }
    else {
      return 0x01;
    }
  }
  
  getClientId() {
    if(undefined != this.msg.clientId) {
      return this.msg.clientId;
    }
    else if(null != this.connectionOptions && undefined != this.connectionOptions.clientId) {
      return this.connectionOptions.clientId;
    }
    else {
      return 0x01;
    }
  }
  
  getProtocolVersion() {
    if(undefined != this.msg.protocolVersion) {
      return this.msg.protocolVersion;
    }
    else if(null != this.connectionOptions && undefined != this.connectionOptions.protocolVersion) {
      return this.connectionOptions.protocolVersion;
    }
    else {
      return 0x01;
    }
  }
  
  getInterfaceVersion() {
    if(undefined != this.msg.interfaceVersion) {
      return this.msg.interfaceVersion;
    }
    else if(null != this.connectionOptions && undefined != this.connectionOptions.interfaceVersion) {
      return this.connectionOptions.interfaceVersion;
    }
    else {
      return 0x01;
    }
  }
}

module.exports = SomeipEncoder;
