
'use strict';

const SomeipConst = require('./someip-const');
const SomeipMsg = require('./someip-msg');


class SomeipMsgRequest extends SomeipMsg {
  constructor(serviceId, methodIdEventId, clientId, sessionId, protocolVersion, interfaceVersion, payload) {
    super(serviceId, methodIdEventId, 8 + (undefined != payload ? Buffer.byteLength(payload) : 0), clientId, sessionId, protocolVersion, interfaceVersion, SomeipConst.MSG_TYPE__REQUEST, SomeipConst.E_OK, payload);
  }
}

module.exports = SomeipMsgRequest;
