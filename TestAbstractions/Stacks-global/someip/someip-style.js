
'use strict';


class SomeipStyle {
  constructor(index) {
    this.index = index;
    this.textColor= 'black';
    this.textProtocolColor = 'black';
    this.protocolColor = 'blue';
    this.protocolBackgroundColor = 'rgba(0, 0, 255, 0.3)';
  }
}

module.exports = SomeipStyle;
