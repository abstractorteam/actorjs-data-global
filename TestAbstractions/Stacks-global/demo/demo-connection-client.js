
'use strict';

const StackApi = require('stack-api');
const DemoConnectionClientOptions = require('./demo-connection-client-options');
const DemoEncoderText = require('./demo-encoder-text');
const DemoDecoderText = require('./demo-decoder-text');


class DemoConnectionClient extends StackApi.ClientConnection {
  constructor(id, actor, connectionOptions, name = 'demo') {
    super(id, actor, name, StackApi.NetworkType.TCP, connectionOptions, DemoConnectionClientOptions);
  }

  sendText(msg) {
    this.sendMessage(new DemoEncoderText(msg));
  }
  
  receiveText(msg) {
    this.receiveMessage(new DemoDecoderText(msg));
  }
}

module.exports = DemoConnectionClient;
