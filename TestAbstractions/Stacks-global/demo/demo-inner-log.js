
'use strict';

const StackApi = require('stack-api');
const DemoConst = require('./demo-const');


class DemoInnerLog {
  static generateLog(msg, ipLogs) {
    const innerLog = new StackApi.LogInner(msg.msgId);
    ipLogs.push(innerLog);
   
    if('INVITE' === msg.msgId) {
      const innerParts = [];
      innerParts.push(new StackApi.LogPartText('msg: '));
      innerParts.push(new StackApi.LogPartText(msg.msg));
      innerParts.push(new StackApi.LogPartText(' - see InnerLog help: '));
      innerParts.push(new StackApi.LogPartRef('InnerLog help', '/documentation/stack-api/log-inner'));
      innerLog.add(new StackApi.LogInner(innerParts));
    }
    else {
      const innerParts = [];
      innerParts.push(new StackApi.LogPartText('msg: '));
      innerParts.push(new StackApi.LogPartText(msg.msg));
      innerParts.push(new StackApi.LogPartText(' - see LogPartRef help: '));
      innerParts.push(new StackApi.LogPartRef('LogPartRef help', '/documentation/stack-api/log-part-ref'));
      innerLog.add(new StackApi.LogInner(innerParts));
    }
    return msg.msgId;
  }
}

module.exports = DemoInnerLog;
