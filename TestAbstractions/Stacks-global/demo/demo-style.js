
'use strict';


class DemoStyle {
  constructor(index) {
    this.index = index;
    this.textColor= 'MistyRose';
    this.textProtocolColor = 'MistyRose';
    this.protocolColor = 'DeepPink';
    this.protocolBackgroundColor = 'rgba(255, 20, 147, 0.3)';
  }
}


module.exports = DemoStyle;
