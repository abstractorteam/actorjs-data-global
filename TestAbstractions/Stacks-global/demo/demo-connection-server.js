
'use strict';

const StackApi = require('stack-api');
const DemoConnectionServerOptions = require('./demo-connection-server-options');
const DemoEncoderText = require('./demo-encoder-text');
const DemoDecoderText = require('./demo-decoder-text');


class DemoConnectionServer extends StackApi.ServerConnection {
  constructor(id, actor, connectionOptions, name = 'demo') {
    super(id, actor, name, StackApi.NetworkType.TCP, connectionOptions, DemoConnectionServerOptions);
  }

  receiveText(msg) {
    this.receiveMessage(new DemoDecoderText(msg));
  }

  sendText(msg) {
    this.sendMessage(new DemoEncoderText(msg));
  }
}

module.exports = DemoConnectionServer;
