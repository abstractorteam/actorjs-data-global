
'use strict';


class DemoMsg {
  constructor(msgId, msg) {
    this.msgId = msgId;
    this.msg = msg;
  }
}

module.exports = DemoMsg;
