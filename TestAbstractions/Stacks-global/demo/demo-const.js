
'use strict';


class DemoConst {
  constructor() {
    
  }
}

DemoConst.MAX_CAPTION_SIZE = 30;
DemoConst.BREAK_CAPTION_SIZE = DemoConst.MAX_CAPTION_SIZE - 3;


module.exports = DemoConst;
