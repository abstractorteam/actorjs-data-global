
'use strict';

const HttpConstHeader = require('./http-const-header');
const HttpConstMethod = require('./http-const-method');
const HttpConstVersion = require('./http-const-version');
const HttpConstStatusCode = require('./http-const-status-code');
const HttpConstReasonPhrase = require('./http-const-reason-phrase');
const HttpMsgRequest = require('./http-msg-request');
const HttpMsgResponse = require('./http-msg-response');

const HttpStyle = require('./http-style');


const exportsObject = {
  Header: HttpConstHeader,
  Method: HttpConstMethod,
  Version: HttpConstVersion,
  StatusCode: HttpConstStatusCode,
  ReasonPhrase: HttpConstReasonPhrase,
  Request: HttpMsgRequest,
  Response: HttpMsgResponse,
  Style: HttpStyle
};

module.exports = exportsObject;
