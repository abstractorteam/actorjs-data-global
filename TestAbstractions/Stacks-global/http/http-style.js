
'use strict';


class HttpStyle {
  constructor(index) {
    this.index = index;
    this.textColor= 'black';
    this.textProtocolColor = 'black';
    this.protocolColor = '#90EE90';
    this.protocolBackgroundColor = 'rgba(144, 238, 144, 0.3)';
  }
}


module.exports = HttpStyle;
