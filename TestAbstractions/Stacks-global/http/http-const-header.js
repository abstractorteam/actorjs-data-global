
'use strict';


class HttpConstHeader {}

// --- Representation Metadata ---

HttpConstHeader.CONTENT_TYPE = 'Content-Type';
HttpConstHeader.CONTENT_ENCODING = 'Content-Encoding';
HttpConstHeader.CONTENT_LANGUAGE = 'Content-Language';
HttpConstHeader.CONTENT_LOCATION = 'Content-Location';
  
// --- Payload Semantics ---
  
HttpConstHeader.CONTENT_LENGTH = 'Content-Length';
HttpConstHeader.CONTENT_RANGE = 'Content-Range';
HttpConstHeader.TRAILER = 'Trailer';
HttpConstHeader.TRANSFER_ENCODING = 'Transfer-Encoding';

// *** REQUEST HEADER FIELDS ***
  
// --- CONTROLS ---
  
HttpConstHeader.CACHE_CONTROL = 'Cache-Control';
HttpConstHeader.EXPECT = 'Expect';
HttpConstHeader.HOST = 'Host';
HttpConstHeader.MAX_FORWARDS = 'Max-Forwards';
HttpConstHeader.PRAGMA = 'Pragma';
HttpConstHeader.RANGE = 'Range';
HttpConstHeader.TE = 'TE';

// --- Conditionals ---
  
HttpConstHeader.IF_MATCH = 'If-Match';
HttpConstHeader.IF_NONE_MATCH = 'If-None-Match';
HttpConstHeader.IF_MODIFIED_SINCE = 'If-Modified-Since';
HttpConstHeader.IF_UNMODIFIED_SINCE = 'If-Unmodified-Since';
HttpConstHeader.IF_RANGE = 'If-Range';

// --- Content Negotiation ---
    
HttpConstHeader.ACCEPT = 'Accept';
HttpConstHeader.ACCEPT_CHARSET = 'Accept-Charset';
HttpConstHeader.ACCEPT_ENCODING = 'Accept-Encoding';
HttpConstHeader.ACCEPT_LANGUAGE = 'Accept-Language';
  
// --- Authentication Credentials ---
    
HttpConstHeader.AUTHORIZATION = 'Authorization';
HttpConstHeader.PROXY_AUTHORIZATION = 'Proxy-Authorization';
    
// --- Request Context ---
  
HttpConstHeader.FROM = 'From';
HttpConstHeader.REFERER = 'Referer';
HttpConstHeader.USER_AGENT = 'User-Agent';

// --- Header Field Registration ---
HttpConstHeader.CONNECTION = 'Connection';
HttpConstHeader.PROXY_CONNECTION = 'Proxy-Connection';
HttpConstHeader.UPGRADE = 'Upgrade';
HttpConstHeader.VIA = 'Via';
   
// *** RESONSE HEADER FIELDS ***
 
// --- Control Data ---
  
HttpConstHeader.AGE = 'Age';
HttpConstHeader.CACHE_CONTROL = 'Cache-Control';
HttpConstHeader.EXPIRES = 'Expires';
HttpConstHeader.DATE = 'Date';
HttpConstHeader.LOCATION = 'Location';
HttpConstHeader.RETRY_AFTER = 'Retry-After';
HttpConstHeader.VARY = 'Vary';
HttpConstHeader.WARNING = 'Warning';
  
// --- Validator Header Fields ---
  
HttpConstHeader.ETAG = 'ETag';
HttpConstHeader.LAST_MODIFIED = 'Last-Modified';
    
// --- Authentication Challenges ---
  
HttpConstHeader.WWW_AUTHENTICATE = 'WWW-Authenticate';
HttpConstHeader.PROXY_AUTHENTICATE = 'Proxy-Authenticate';
    
// --- Response Context ---
  
HttpConstHeader.ACCEPT_RANGES = 'Accept-Ranges';
HttpConstHeader.ALLOW = 'Allow';
HttpConstHeader.SERVER = 'Server';


HttpConstHeader.HEADER_DATA = new Map([
  // --- Representation Metadata ---
  [HttpConstHeader.CONTENT_TYPE, {links: ['rfc7231#section-3.1.1.5', 'rfc7231#section-3.1']}],
  [HttpConstHeader.CONTENT_ENCODING, {links: ['rfc7231#section-3.1.2.2', 'rfc7231#section-3.1']}],
  [HttpConstHeader.CONTENT_LANGUAGE, {links: ['rfc7231#section-3.1.3.2', 'rfc7231#section-3.1']}],
  [HttpConstHeader.CONTENT_LOCATION, {links: ['rfc7231#section-3.1.4.2', 'rfc7231#section-3.1']}],
  // --- Payload Semantics ---
  [HttpConstHeader.CONTENT_LENGTH, {links: ['rfc7230#section-3.3.2', 'rfc7231#section-3.3']}],
  [HttpConstHeader.CONTENT_RANGE, {links: ['rfc7233#section-4.2', 'rfc7231#section-3.3']}],
  [HttpConstHeader.TRAILER, {links: ['rfc7230#section-4.4', 'rfc7231#section-3.3']}],
  [HttpConstHeader.TRANSFER_ENCODING, {links: ['rfc7230#section-3.3.1', 'rfc7231#section-3.3']}],
  // *** REQUEST HEADER FIELDS ***
  // --- CONTROLS ---
  [HttpConstHeader.CACHE_CONTROL, {links: ['rfc7234#section-5.2', 'rfc7231#section-5.1']}],
  [HttpConstHeader.EXPECT, {links: ['rfc7231#section-5.1.1', 'rfc7231#section-5.1']}],
  [HttpConstHeader.HOST, {links: ['rfc7230#section-5.4', 'rfc7231#section-5.1']}],
  [HttpConstHeader.MAX_FORWARDS, {links: ['rfc7231#section-5.1.2', 'rfc7231#section-5.1']}],
  [HttpConstHeader.PRAGMA, {links: ['rfc7234#section-5.4', 'rfc7231#section-5.1']}],
  [HttpConstHeader.RANGE, {links: ['rfc7233#section-3.1', 'rfc7231#section-5.1']}],
  [HttpConstHeader.TE, {links: ['rfc7230#section-4.3', 'rfc7231#section-5.1']}],
  // --- Conditionals ---
  [HttpConstHeader.IF_MATCH, {links: ['rfc7232#section-3.1', 'rfc7231#section-5.2']}],
  [HttpConstHeader.IF_NONE_MATCH, {links: ['rfc7232#section-3.2', 'rfc7231#section-5.2']}],
  [HttpConstHeader.IF_MODIFIED_SINCE, {links: ['rfc7232#section-3.3', 'rfc7231#section-5.2']}],
  [HttpConstHeader.IF_UNMODIFIED_SINCE, {links: ['rfc7232#section-3.4', 'rfc7231#section-5.2']}],
  [HttpConstHeader.IF_RANGE, {links: ['rfc7233#section-3.2', 'rfc7231#section-5.2']}],
  // --- Content Negotiation ---
  [HttpConstHeader.ACCEPT, {links: ['rfc7231#section-5.3.2', 'rfc7231#section-5.3']}],
  [HttpConstHeader.ACCEPT_CHARSET, {links: ['rfc7231#section-5.3.3', 'rfc7231#section-5.3']}],
  [HttpConstHeader.ACCEPT_ENCODING, {links: ['rfc7231#section-5.3.4', 'rfc7231#section-5.3']}],
  [HttpConstHeader.ACCEPT_LANGUAGE, {links: ['rfc7231#section-5.3.5', 'https//tools.ietf.org/html/rfc7231#section-5.3']}],
  // --- Authentication Credentials ---
  [HttpConstHeader.AUTHORIZATION, {links: ['rfc7235#section-4.2', 'rfc7231#section-5.4']}],
  [HttpConstHeader.PROXY_AUTHORIZATION, {links: ['rfc7235#section-4.4', 'rfc7231#section-5.4']}],
  // --- Request Context ---
  [HttpConstHeader.FROM, {links: ['rfc7231#section-5.5.1', 'rfc7231#section-5.5']}],
  [HttpConstHeader.REFERER, {links: ['rfc7231#section-5.5.2', 'rfc7231#section-5.5']}],
  [HttpConstHeader.USER_AGENT, {links: ['rfc7231#section-5.5.3', 'rfc7231#section-5.5']}],
  // --- Header Field Registration ---
  [HttpConstHeader.CONNECTION, {links: ['rfc7230#section-6.1', 'rfc7230#section-6']}],
  [HttpConstHeader.PROXY_CONNECTION, {links: ['rfc7230#appendix-A.1.2', 'rfc7230#appendix-A']}],
  
  [HttpConstHeader.UPGRADE, {links: ['rfc7230#section-6.7', 'rfc7230#section-6.7']}],
  [HttpConstHeader.VIA, {links: ['rfc7230#section-5.7.1', 'rfc7230#section-5.7']}],
  // *** RESONSE HEADER FIELDS ***
  // --- Control Data ---
  [HttpConstHeader.AGE, {links: ['rfc7234#section-5.1', 'rfc7231#section-7.1']}],
  [HttpConstHeader.CACHE_CONTROL, {links: ['rfc7234#section-5.2', 'rfc7231#section-7.1']}],
  [HttpConstHeader.EXPIRES, {links: ['rfc7234#section-5.3', 'rfc7231#section-7.1']}],
  [HttpConstHeader.DATE, {links: ['rfc7231#section-7.1.1.2', 'rfc7231#section-7.1']}],
  [HttpConstHeader.LOCATION, {links: ['rfc7231#section-7.1.2', 'rfc7231#section-7.1']}],
  [HttpConstHeader.RETRY_AFTER, {links: ['rfc7231#section-7.1.3', 'rfc7231#section-7.1']}],
  [HttpConstHeader.VARY, {links: ['rfc7231#section-7.1.4', 'rfc7231#section-7.1']}],
  [HttpConstHeader.WARNING, {links: ['rfc7234#section-5.5', 'rfc7231#section-7.1']}],
  // --- Validator Header Fields ---
  [HttpConstHeader.ETAG, {links: ['rfc7232#section-2.3', 'rfc7231#section-7.2']}],
  [HttpConstHeader.LAST_MODIFIED, {links: ['rfc7232#section-2.2', 'rfc7231#section-7.2']}],
  // --- Authentication Challenges ---
  [HttpConstHeader.WWW_AUTHENTICATE, {links: ['rfc7235#section-4.1', 'rfc7231#section-7.3']}],
  [HttpConstHeader.PROXY_AUTHENTICATE, {links: ['rfc7235#section-4.3', 'rfc7231#section-7.3']}],
  // --- Response Context ---
  [HttpConstHeader.ACCEPT_RANGES, {links: ['rfc7233#section-2.3', 'rfc7231#section-7.4']}],
  [HttpConstHeader.ALLOW, {links: ['rfc7231#section-7.4.1', 'rfc7231#section-7.4']}],
  [HttpConstHeader.SERVER, {links: ['rfc7231#section-7.4.2', 'rfc7231#section-7.4']}],
]);

module.exports = HttpConstHeader;
