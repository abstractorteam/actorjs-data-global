
'use strict';

const StackApi = require('stack-api');
const HttpEncoder = require('./http-encoder');
const HttpConst = require('./http-const');
const HttpInnerLog = require('./http-inner-log');


class HttpEncoderResponse extends HttpEncoder {
  constructor(msg, command = HttpEncoder.SEND_ALL) {
    super(msg, command);
  }

  encodeFirstLine() {
    const msg = this.msg;
    this.offset += this.buffer.write(`${msg.httpVersion} ${msg.statusCodeString} ${msg.reasonPhrase}${HttpConst.CR_CL}`, this.offset);
    if(this.isLogIp) {
      process.nextTick(() => {
        const innerLogHttpResponse = HttpInnerLog.createStatusLine(this.msg);
        if(innerLogHttpResponse.logParts[0]) {
          const log = innerLogHttpResponse.logParts[0].text;
          this.caption = log.substring(log.indexOf(' ') + 1);
        }
        this.ipLogs.push(innerLogHttpResponse);
      });
    }
  }
    
  getFirstLineLength() {
    const msg = this.msg;
    return msg.httpVersion.length + msg.statusCodeString.length + msg.reasonPhrase.length + HttpConst.FIRST_LINE;
  }
}


module.exports = HttpEncoderResponse;
