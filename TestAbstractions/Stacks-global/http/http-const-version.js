
'use strict';


class HttpConstVersion {}

HttpConstVersion.HTTP_1_0 = 'HTTP/1.0',
HttpConstVersion.HTTP_1_1 = 'HTTP/1.1',

HttpConstVersion.HTTP_VERSION_DATA = {links: ['rfc7230#section-2.6']};

module.exports = HttpConstVersion;
