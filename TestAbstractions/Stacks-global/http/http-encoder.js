
'use strict';

const StackApi = require('stack-api');
const HttpConst = require('./http-const');
const HttpConstHeader = require('./http-const-header');
const HttpInnerLog = require('./http-inner-log');


class HttpEncoder extends StackApi.Encoder {
  constructor(msg, command) {
    super();
    this.msg = msg;
    this.command = command;
    this.offset = 0;
    this.buffer = null;
    this.buffers = [];
    this.bodySize = 0;
  }
  
  setCommand(command, options) {
    this.command = command;
    this.options = options;
    return this;
  }
  
  *encode() {
    switch(this.command) {
      case HttpEncoder.SEND_ALL:
        yield* this._sendAll();
        break;
      case HttpEncoder.SEND_FIRST_LINE:
        yield* this._sendFirstLine();
        break;
      case HttpEncoder.SEND_FIRST_LINE_AND_HEADERS:
        yield* this._sendFirstLineAndHeaders();
        break;
      case HttpEncoder.SEND_HEADERS:
        yield* this._sendHeaders();
        break;
      case HttpEncoder.SEND_BODY:
        yield* this._sendBody();
        break;
      case HttpEncoder.SEND_BODY_CHUNK:
        throw new Error('NOT IMPLEMENTD');
        break;
    };
  }
  
  encodeHeaders() {
    const msg = this.msg;
    msg.headers.forEach((values, name) => {
      values.forEach((value) => {
        this.offset += this.buffer.write(name, this.offset);
        this.offset += this.buffer.write(': ', this.offset);
        this.offset += this.buffer.write(value, this.offset);
        this.offset += this.buffer.write(HttpConst.CR_CL, this.offset);
      });
    });
    this.offset += this.buffer.write(HttpConst.CR_CL, this.offset);
    if(this.isLogIp) {
      process.nextTick(() => {
        this.ipLogs.push(HttpInnerLog.createHeaders(this.msg));
      });
    }
  }
  
  getHeadersLength() {
    let length = 0;
    const msg = this.msg;
    msg.headers.forEach((values, name) => {
      values.forEach((value) => {
        length += name.length + value.length + HttpConst.HEADER_LINE;
      });
    });
    length += HttpConst.CR_CL_LENGTH;
    return length;
  }
    
  getBodyLength() {
    const msg = this.msg;
    let size = 0;
    msg.contents.forEach((content) => {
      size += content.size;
    });
    return size;
  }

  *_sendAll() {
    if(0 !== this._createBuffer(this.getFirstLineLength() + this.getHeadersLength())) {
      this.encodeFirstLine();
      this.encodeHeaders();
      while(0 !== this.buffers.length) {
        yield* this.send(this.buffers.shift());
      }
      if(this.isLogIp) {
        process.nextTick(() => {
          this.logMessage();
        });
      }
      const msg = this.msg;
      for(let c = 0; c < msg.contents.length; ++c) {
        const content = msg.contents[c];
        let buffer;
        while(null !== (buffer = content.getBuffer())) {
          yield* this.send(buffer);
          if(this.isLogIp) {
            process.nextTick(() => {
              const contentType = this.msg.getHeader(HttpConstHeader.CONTENT_TYPE);
              const contentEncoding = this.msg.getHeader(HttpConstHeader.CONTENT_ENCODING);
              this.ipLogs.push(HttpInnerLog.createBody(buffer, contentType, contentEncoding));
              this.caption = '';
              this.logMessage();
            });
          }
        }
      }
    }
  }
  
  *_sendFirstLineAndHeaders() {
    if(0 !== this._createBuffer(this.getFirstLineLength() + this.getHeadersLength())) {
      this.encodeFirstLine();
      this.encodeHeaders();
      while(0 !== this.buffers.length) {
        yield* this.send(this.buffers.shift());
      }
      if(this.isLogIp) {
        process.nextTick(() => {
          this.logMessage();
        });
      }
    }
  }
  
  *_sendFirstLine() {
    if(0 !== this._createBuffer(this.getFirstLineLength())) {
      this.encodeFirstLine();
      while(0 !== this.buffers.length) {
        yield* this.send(this.buffers.shift());
      }
      if(this.isLogIp) {
        process.nextTick(() => {
          this.logMessage();
        });
      }
    }
  }

  *_sendHeaders() {
  	if(0 !== this._createBuffer(this.getHeadersLength())) {
      this.encodeHeaders();
      while(0 !== this.buffers.length) {
        yield* this.send(this.buffers.shift());
      }
      if(this.isLogIp) {
        process.nextTick(() => {
          this.logMessage();
        });
      }
    }
  }

  *_sendBody() {
    const msg = this.msg;
    for(let c = 0; c < msg.contents.length; ++c) {
      const content = msg.contents[c];
      let buffer;
      while(null !== (buffer = content.getBuffer())) {
        yield* this.send(buffer);
        if(this.isLogIp) {
          process.nextTick(() => {
            this.logMessage();
          });
        }
      }
    }
  }
  
  _createBuffer(size) {
    if(0 !== size) {
      this.offset = 0;
      this.buffer = Buffer.allocUnsafe(size);
      this.buffers.push(this.buffer);
      return size;
    }
    else {
      return 0;
    }
  }
}


HttpEncoder.SEND_ALL = 0;
HttpEncoder.SEND_FIRST_LINE = 1;
HttpEncoder.SEND_FIRST_LINE_AND_HEADERS = 2;
HttpEncoder.SEND_HEADERS = 3;
HttpEncoder.SEND_BODY = 4;
HttpEncoder.SEND_BODY_CHUNK = 5;


module.exports = HttpEncoder;
