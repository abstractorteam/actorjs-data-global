
'use strict';


class HttpConstStatusCode {}

HttpConstStatusCode.Continue = 100;
HttpConstStatusCode.SwitchingProtocols = 101;

HttpConstStatusCode.OK = 200;
HttpConstStatusCode.Created = 201;
HttpConstStatusCode.Accepted = 202;
HttpConstStatusCode.NonAuthoritativeInformation = 203;
HttpConstStatusCode.NoContent = 204;
HttpConstStatusCode.ResetContent = 205;
HttpConstStatusCode.PartialContent = 206;

HttpConstStatusCode.MultipleChoices = 300;
HttpConstStatusCode.MovedPermanently = 301;
HttpConstStatusCode.Found = 302;
HttpConstStatusCode.SeeOther = 303;
HttpConstStatusCode.NotModified = 304;
HttpConstStatusCode.UseProxy = 305;
HttpConstStatusCode.TemporaryRedirect = 307;

HttpConstStatusCode.BadRequest = 400;
HttpConstStatusCode.Unauthorized = 401;
HttpConstStatusCode.PaymentRequired = 402;
HttpConstStatusCode.Forbidden = 403;
HttpConstStatusCode.NotFound = 404;
HttpConstStatusCode.MethodNotAllowed = 405;
HttpConstStatusCode.NotAcceptable = 406;
HttpConstStatusCode.ProxyAuthenticationRequired = 407;
HttpConstStatusCode.RequestTimeout = 408;
HttpConstStatusCode.Conflict = 409;
HttpConstStatusCode.Gone = 410;
HttpConstStatusCode.LengthRequired = 411;
HttpConstStatusCode.PreconditionFailed = 412;
HttpConstStatusCode.PayloadTooLarge = 413;
HttpConstStatusCode.URITooLong = 414;
HttpConstStatusCode.UnsupportedMediaType = 415;
HttpConstStatusCode.RangeNotSatisfiable = 416;
HttpConstStatusCode.ExpectationFailed = 417;
HttpConstStatusCode.UpgradeRequired = 426;

HttpConstStatusCode.InternalServerError = 500;
HttpConstStatusCode.NotImplemented = 501;
HttpConstStatusCode.BadGateway = 502;
HttpConstStatusCode.ServiceUnavailable = 503;
HttpConstStatusCode.GatewayTimeout = 504;
HttpConstStatusCode.HTTPVersionNotSupported = 505;

HttpConstStatusCode.DATA_STATUS_CODE = {links: ['rfc7231#section-6']};
HttpConstStatusCode.DATA = new Map([
  [HttpConstStatusCode.Continue, {links: ['rfc7231#section-6.2.1', 'rfc7231#section-6']}],
  [HttpConstStatusCode.SwitchingProtocols, {links: ['rfc7231#section-6.2.2', 'rfc7231#section-6']}],

  [HttpConstStatusCode.OK, {links: ['rfc7231#section-6.3.1', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Created, {links: ['rfc7231#section-6.3.2', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Accepted, {links: ['rfc7231#section-6.3.3', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NonAuthoritativeInformation, {links: ['rfc7231#section-6.3.4', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NoContent, {links: ['rfc7231#section-6.3.5', 'rfc7231#section-6']}],
  [HttpConstStatusCode.ResetContent, {links: ['rfc7231#section-6.3.6', 'rfc7231#section-6']}],
  [HttpConstStatusCode.PartialContent, {links: ['rfc7233#section-4.1', 'rfc7231#section-6']}],

  [HttpConstStatusCode.MultipleChoices, {links: ['rfc7231#section-6.4.1', 'rfc7231#section-6']}],
  [HttpConstStatusCode.MovedPermanently, {links: ['rfc7231#section-6.4.2', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Found, {links: ['rfc7231#section-6.4.3', 'rfc7231#section-6']}],
  [HttpConstStatusCode.SeeOther, {links: ['rfc7231#section-6.4.4', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NotModified, {links: ['rfc7232#section-4.1', 'rfc7231#section-6']}],
  [HttpConstStatusCode.UseProxy, {links: ['rfc7231#section-6.4.5', 'rfc7231#section-6']}],
  [HttpConstStatusCode.TemporaryRedirect, {links: ['rfc7231#section-6.4.7', 'rfc7231#section-6']}],

  [HttpConstStatusCode.BadRequest, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Unauthorized, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.PaymentRequired, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Forbidden, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NotFound, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.MethodNotAllowed, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NotAcceptable, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.ProxyAuthenticationRequired, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.RequestTimeout, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Conflict, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Gone, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.LengthRequired, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.PreconditionFailed, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.PayloadTooLarge, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.URITooLong, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.UnsupportedMediaType, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.RangeNotSatisfiable, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.ExpectationFailed, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.UpgradeRequired, {links: ['', 'rfc7231#section-6']}],

  [HttpConstStatusCode.InternalServerError, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NotImplemented, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.BadGateway, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.ServiceUnavailable, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.GatewayTimeout, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.HTTPVersionNotSupported, {links: ['', 'rfc7231#section-6']}],
]);

module.exports = HttpConstStatusCode;
