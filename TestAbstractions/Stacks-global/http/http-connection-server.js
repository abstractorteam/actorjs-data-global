
'use strict';

const StackApi = require('stack-api');
const HttpConnectionServerOptions = require('./http-connection-server-options');
const HttpDecoder = require('./http-decoder');
const HttpEncoder = require('./http-encoder');
const HttpEncoderResponse = require('./http-encoder-response');
const HttpDecoderRequest = require('./http-decoder-request');


class HttpConnectionServer extends StackApi.ServerConnection {
  constructor(id, actor, connectionOptions) {
    super(id, actor, 'http', StackApi.NetworkType.TCP, connectionOptions, HttpConnectionServerOptions);
    this.encoder = null;
  }
  
  receive() {
    this.receiveMessage(new HttpDecoderRequest());
  }
  
  send(msg) {
    this.sendMessage(new HttpEncoderResponse(msg));
  }
  
  // --- PARTS
  
  sendResponseLine(msg) {
    this.sendMessage(this.encoder = new HttpEncoderResponse(msg, HttpEncoder.SEND_FIRST_LINE));
  }
  
  sendResponseLineAndHeaders(msg) {
    this.sendMessage(this.encoder = new HttpEncoderResponse(msg, HttpEncoder.SEND_FIRST_LINE_AND_HEADERS));
  }
  
  sendHeaders() {
    this.sendMessage(this.encoder.setCommand(HttpEncoder.SEND_HEADERS));
  }
  
  sendBody() {
    this.sendMessage(this.encoder.setCommand(HttpEncoder.SEND_BODY));
  }
  
  sendBodyChunk(from=0, to=-1) {
    this.sendMessage(this.encoder.setCommand(HttpEncoder.SEND_BODY_CHUNK, {
      from: from,
      to:to
    }));
  }
}

module.exports = HttpConnectionServer;
