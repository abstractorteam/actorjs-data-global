
'use strict';

const StackApi = require('stack-api');
const HttpConst = require('./http-const');
const HttpConstHeader = require('./http-const-header');
const HttpInnerLog = require('./http-inner-log');


class HttpDecoder extends StackApi.Decoder {
  constructor(msg, command) {
    super();
    this.msg = msg;
    this.command = command;
    this.content = null;
    this.contentLength = -1;
    this.contentLengthToRead = -1;
  }
  
  *decode() {
    switch(this.command) {
      case HttpDecoder.RECEIVE_ALL:
        yield* this._receiveAll();
    }
    return this.msg;
  }
  
  *decodeHeaders() {
    while(this._parseHeaderLine(yield* this.receiveLine()));
    if(this.isLogIp) {
      process.nextTick(() => {
        this.ipLogs.push(HttpInnerLog.createHeaders(this.msg));
      });
    }
  }
  
  *decodeBody() {
    if(this.msg.hasHeader(HttpConstHeader.CONTENT_LENGTH)) { // ONE BODY, LENGTH KNOWN
      this._setContentLength(Number.parseInt(this.msg.getHeader(HttpConstHeader.CONTENT_LENGTH)));
      if(0 < this.contentLength) {
        this.content = new StackApi.ContentBinary();
        this.msg.addBody(this.content);
        while(this._parseBody(yield* this.receiveSize(this._getNextRead())));
        if(this.ipLogs) {
          process.nextTick(() => {
            const bodyInnerLog = new StackApi.LogInner(`[body]`);
            this.ipLogs.push(bodyInnerLog);
          });
        }
      }
    }
    else {
      // TODO: 
    }
  }
  
  _parseHeaderLine(headerLine) {
    if(0 !== headerLine.length) {
      let parameters = headerLine.split(HttpConst.COLON_SP);
      this.msg.addHeader(...parameters);
      return true;
    }
    else {
      return false;
    }
  }
  
  _setContentLength(contentLength) {
    this.contentLength = contentLength;
    this.contentLengthToRead = contentLength;
  }
  
  _getNextRead() {
    const nextRead = Math.min(this.contentLengthToRead, HttpConst.MAX_CONTENT_CHUNK_SIZE);
    this.contentLengthToRead -= nextRead;
    return nextRead;
  }
  
  _parseBody(buffer) {
    this.content.addBuffer(buffer);
    return this.contentLengthToRead > 0; 
  }
  
  *_receiveAll() {
    yield* this.decodeFirstLine();
    yield* this.decodeHeaders();
    yield* this.decodeBody();
    if(this.isLogIp) {
      process.nextTick(() => {
        this.logMessage();
      });
    }
    return this.msg;
  }
}

HttpDecoder.RECEIVE_ALL = 0;
HttpDecoder.RECEIVE_FIRST_LINE = 1;
HttpDecoder.RECEIVE_FIRST_LINE_AND_HEADERS = 2;
HttpDecoder.RECEIVE_HEADERS = 3;
HttpDecoder.RECEIVE_BODY = 4;


module.exports = HttpDecoder;
