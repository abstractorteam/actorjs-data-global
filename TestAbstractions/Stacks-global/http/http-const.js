
'use strict';


class HttpConst {}

HttpConst.CR_CL = '\r\n';
HttpConst.SP = ' ';
HttpConst.COLON_SP = ': ';
HttpConst.COMMA_SP = ': ';

HttpConst.CR_CL_LENGTH = HttpConst.CR_CL.length;
HttpConst.SP_LENGTH = HttpConst.SP.length;
HttpConst.COLON_SP_LENGTH = HttpConst.COLON_SP.length;
HttpConst.COMMA_SP_LENGTH = HttpConst.COMMA_SP.length;

HttpConst.FIRST_LINE = HttpConst.SP_LENGTH + HttpConst.SP_LENGTH + HttpConst.CR_CL_LENGTH;
HttpConst.HEADER_LINE = HttpConst.COLON_SP_LENGTH + HttpConst.CR_CL_LENGTH;


HttpConst.REQUEST_LINE_HREF = "https://tools.ietf.org/html/rfc7230#section-3.1.1";
HttpConst.DOCUMENTATION_LINK_ROOT = 'https://tools.ietf.org/html/';

HttpConst.MAX_CONTENT_CHUNK_SIZE = 16384;

module.exports = HttpConst;
