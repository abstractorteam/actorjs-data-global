# **Http**

## **Description**
The Http Stack implements Http 1.1. 

## **Components**
* [HttpConnectionClient]([[stack-http]]/client-connection)
* [HttpConnectionServer]([[stack-http]]/server-connection)
* [HttpMsg]([[stack-http]]/http-message)

### **Specifications**
The aim is to implement the requirements in: [RFC7230](https://tools.ietf.org/html/rfc7230), [RFC7231](https://tools.ietf.org/html/rfc7231), [RFC7232](https://tools.ietf.org/html/rfc7232), [RFC7233](https://tools.ietf.org/html/rfc7233), [RFC7234](https://tools.ietf.org/html/rfc7234), [RFC7235](https://tools.ietf.org/html/rfc7235).



