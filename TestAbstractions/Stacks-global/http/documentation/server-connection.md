<a name="topOfPage"></a>
# HttpServerConnection
In the ***initServer()** and ***run()** method it is possible for an [Actor](actors) with server capacity to start listen to a socket. The implementation of the HttpServerConnection API starts in [http-connection-server.js](/stack-editor/Stacks-global/http/http-connection-server.js).

## API
[send]()
[receive]()

[sendResponseLine]()
[sendResponseLineAndHeaders]()
[sendHeaders]()
[sendBody]()
[sendBodyChunk]()