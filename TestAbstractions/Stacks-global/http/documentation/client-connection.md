<a name="topOfPage"></a>
# **HttpConnectionClient**
In the ***initClient()** and ***run()** method it is possible for an [Actor](actors) with client capacity to connect to a socket. The implementation of the HttpClientConnection API starts in [http-connection-client.js](/stack-editor/Stacks-global/http/http-connection-client.js).

## **API**
[send]()
[receive]()

[sendRequestLine]()
[sendRequestLineAndHeaders]()
[sendHeaders]()
[sendBody]()
[sendBodyChunk]()