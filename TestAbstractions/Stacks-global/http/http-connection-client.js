
'use strict';

const StackApi = require('stack-api');
const HttpConnectionClientOptions = require('./http-connection-client-options');
const HttpEncoder = require('./http-encoder');
const HttpEncoderRequest = require('./http-encoder-request');
const HttpDecoderResponse = require('./http-decoder-response');


class HttpConnectionClient extends StackApi.ClientConnection {
  constructor(id, actor, connectionOptions) {
    super(id, actor, 'http', StackApi.NetworkType.TCP, connectionOptions, HttpConnectionClientOptions);
    this.encoder = null;
  }
  
  send(msg) {
    this.sendMessage(new HttpEncoderRequest(msg));
  }
  
  receive() {
    this.receiveMessage(new HttpDecoderResponse());
  }
  
  // --- PARTS
  
  sendRequestLine(msg) {
    this.sendMessage(this.encoder = new HttpEncoderRequest(msg, HttpEncoder.SEND_FIRST_LINE));
  }
  
  sendRequestLineAndHeaders(msg) {
    this.sendMessage(this.encoder = new HttpEncoderRequest(msg, HttpEncoder.SEND_FIRST_LINE_AND_HEADERS));
  }
  
  sendHeaders() {
    this.sendMessage(this.encoder.setCommand(HttpEncoder.SEND_HEADERS));
  }
  
  sendBody() {
    this.sendMessage(this.encoder.setCommand(HttpEncoder.SEND_BODY));
  }
  
  sendBodyChunk(from=0, to=-1) {
    this.sendMessage(this.encoder.setCommand(HttpEncoder.SEND_BODY_CHUNK, {
      from: from,
      to:to
    }));
  }
}

module.exports = HttpConnectionClient;
