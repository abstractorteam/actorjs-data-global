
'use strict';


class HttpConstMethod {}

HttpConstMethod.GET = 'GET';
HttpConstMethod.HEAD = 'HEAD';
HttpConstMethod.POST = 'POST';
HttpConstMethod.PUT = 'PUT';
HttpConstMethod.DELETE = 'DELETE';
HttpConstMethod.CONNECT = 'CONNECT';
HttpConstMethod.OPTIONS = 'OPTIONS';
HttpConstMethod.TRACE = 'TRACE';

HttpConstMethod.METHOD_DATA = new Map([
  [HttpConstMethod.GET, {links: ['rfc7231#section-4.3.1']}],
  [HttpConstMethod.HEAD, {links: ['rfc7231#section-4.3.2']}],
  [HttpConstMethod.POST, {links: ['rfc7231#section-4.3.3']}],
  [HttpConstMethod.PUT, {links: ['rfc7231#section-4.3.4']}],
  [HttpConstMethod.DELETE, {links: ['rfc7231#section-4.3.5']}],
  [HttpConstMethod.CONNECT, {links: ['rfc7231#section-4.3.6']}],
  [HttpConstMethod.OPTIONS, {links: ['rfc7231#section-4.3.7']}],
  [HttpConstMethod.TRACE, {links: ['rfc7231#section-4.3.8']}]
]);

HttpConstMethod.METHOD = {links: ['rfc7231#section-4']};
HttpConstMethod.REQUEST_TARGET = {links: ['rfc7230#section-5.3']};


module.exports = HttpConstMethod;
