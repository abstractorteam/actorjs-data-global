
'use strict';

const StackApi = require('stack-api');
const WebsocketConnectionClientOptions = require('./websocket-connection-client-options');
const WebsocketEncoder = require('./websocket-encoder');
const WebsocketDecoder = require('./websocket-decoder');


class WebsocketConnectionClient extends StackApi.ClientConnection {
  constructor(id, cbActorCallbacks, connectionOptions, name = 'websocket') {
    super(id, cbActorCallbacks, name, StackApi.NetworkType.TCP, connectionOptions, WebsocketConnectionClientOptions);
  }

  send(msg) {
    this.sendMessage(new WebsocketEncoder(msg));
  }
  
  receive(msg) {
    this.receiveMessage(new WebsocketDecoder(msg));
  }
}

module.exports = WebsocketConnectionClient;
