
'use strict';

const StackApi = require('stack-api');
const WebsocketInnerLog = require('./websocket-inner-log');


class WebsocketEncoder extends StackApi.Encoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }
  
  *encode() {

  }
}

module.exports = WebsocketEncoder;
