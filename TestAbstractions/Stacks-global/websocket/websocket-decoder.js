
'use strict';

const StackApi = require('stack-api');
const WebsocketInnerLog = require('./websocket-inner-log');


class WebsocketDecoder extends StackApi.Decoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }
}

module.exports = WebsocketDecoder;
