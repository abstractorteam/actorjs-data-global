
'use strict';

const StackApi = require('stack-api');
const WebsocketConnectionServerOptions = require('./websocket-connection-server-options');
const WebsocketEncoder = require('./websocket-encoder');
const WebsocketDecoder = require('./websocket-decoder');


class WebsocketConnectionServer extends StackApi.ServerConnection {
  constructor(id, cbActorCallbacks, connectionOptions, name = 'websocket') {
    super(id, cbActorCallbacks, name, StackApi.NetworkType.TCP, connectionOptions, WebsocketConnectionServerOptions);
  }

  receive(msg) {
    this.receiveMessage(new WebsocketDecoder(msg));
  }

  send(msg) {
    this.sendMessage(new WebsocketEncoder(msg));
  }
}

module.exports = WebsocketConnectionServer;
