
'use strict';


class WebsocketStyle {
  constructor(index) {
    this.index = index;
    this.textColor= 'black';
    this.textProtocolColor = 'black';
    this.protocolColor = 'RoyalBlue';
    this.protocolBackgroundColor = 'CornflowerBlue';
  }
}


module.exports = WebsocketStyle;
