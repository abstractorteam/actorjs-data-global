
'use strict';

const WebsocketConnectionClientOptions = require('./websocket-connection-client-options');
const WebsocketConnectionServerOptions = require('./websocket-connection-server-options');
const WebsocketMsg = require('./websocket-msg');
const WebsocketConst = require('./websocket-const');
const WebsocketStyle = require('./websocket-style');


const exportsObject = {
  WebsocketConnectionClientOptions: WebsocketConnectionClientOptions,
  WebsocketConnectionServerOptions: WebsocketConnectionServerOptions,
  WebsocketMsg: WebsocketMsg,
  WebsocketConst: WebsocketConst,
  WebsocketStyle: WebsocketStyle
}


module.exports = exportsObject;
