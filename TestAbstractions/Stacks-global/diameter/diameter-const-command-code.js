
'use strict';


class DiameterConstCommandCode {
  static get(commandCode) {
    const commandCodeData = DiameterConstCommandCode.HEADER_DATA.get(commandCode);
    if(undefined !== commandCodeData) {
      return commandCodeData;
    }
    else {
      return DiameterConstCommandCode.UNKNOWN;
    }
  }
}

DiameterConstCommandCode.ABORT_SESSION = 274;
DiameterConstCommandCode.ACCOUNTING = 271;
DiameterConstCommandCode.CAPABILITIES_EXCHANGE = 257;
DiameterConstCommandCode.DEVICE_WATCHDOG = 280;
DiameterConstCommandCode.DISCONNECT_PEER = 282;
DiameterConstCommandCode.RE_AUT = 258;
DiameterConstCommandCode.SESSION_TERMINATION = 275;

DiameterConstCommandCode.HEADER_DATA = new Map([
  [DiameterConstCommandCode.ABORT_SESSION, [{found: true, links: ['rfc6733#section-8.5.2', 'rfc6733#section-8.5'], abbrev: 'ASA', name: 'Abort-Session-Answer'}, {found: true, links: ['rfc6733#section-8.5.1', 'rfc6733#section-8.5'], abbrev: 'ASR', name: 'Abort-Session-Request'}]],
  [DiameterConstCommandCode.ACCOUNTING, [{found: true, links: ['rfc6733#section-9.7.2', 'rfc6733#section-9.7'], abbrev: 'ACA', name: 'Accounting-Answer'}, {found: true, links: ['rfc6733#section-9.7.1', 'rfc6733#section-9.7'], abbrev: 'ACR', name: 'Accounting-Request'}]],
  [DiameterConstCommandCode.CAPABILITIES_EXCHANGE, [{found: true, links: ['rfc6733#section-5.3.2', 'rfc6733#section-5.3'], abbrev: 'CEA', name: 'Capabilities-Exchange-Answer'}, {found: true, links: ['rfc6733#section-5.3.1', 'rfc6733#section-5.3'], abbrev: 'CER', name: 'Capabilities-Exchange-Request'}]],
  [DiameterConstCommandCode.DEVICE_WATCHDOG, [{found: true, links: ['rfc6733#section-5.5.2', 'rfc6733#section-5.5'], abbrev: 'DWA', name: 'Device-Watchdog-Answer'}, {found: true, links: ['rfc6733#section-5.5.1', 'rfc6733#section-5.5'], abbrev: 'DWR', name: 'Device-Watchdog-Request'}]],
  [DiameterConstCommandCode.DISCONNECT_PEER, [{found: true, links: ['rfc6733#section-5.4.2', 'rfc6733#section-5.4'], abbrev: 'DPA', name: 'Disconnect-Peer-Answer'}, {found: true, links: ['rfc6733#section-5.4.1', 'rfc6733#section-5.4'], abbrev: 'DPR', name: 'Disconnect-Peer-Request'}]],
  [DiameterConstCommandCode.RE_AUT, [{found: true, links: ['rfc6733#section-8.3.2', 'rfc6733#section-8.3'], abbrev: 'RAA', name: 'Re-Auth-Answer'}, {found: true, links: ['rfc6733#section-8.3.1', 'rfc6733#section-8.3'], abbrev: 'RAR', name: 'Re-Auth-Request'}]],
  [DiameterConstCommandCode.SESSION_TERMINATION, [{found: true, links: ['rfc6733#section-8.4.2', 'rfc6733#section-8.4'], abbrev: 'STA', name: 'Session-Termination-Answer'}, {found: true, links: ['rfc6733#section-8.4.1', 'rfc6733#section-8.4'], abbrev: 'STR', name: 'Session-Termination-Request'}]]
]);

DiameterConstCommandCode.UNKNOWN = [{found: false, links: ['', ''], abbrev: 'UNKNOWN', name: 'UNKNOWN'}, {found: false, links: ['', ''], abbrev: 'UNKNOWN', name: 'UNKNOWN'}];


module.exports = DiameterConstCommandCode;
