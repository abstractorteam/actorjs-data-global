
'use strict';

const StackApi = require('stack-api');
const DiameterConnectionClientOptions = require('./diameter-connection-client-options');
const DiameterEncoder = require('./diameter-encoder');
const DiameterDecoder = require('./diameter-decoder');


class DiameterConnectionClient extends StackApi.ClientConnection {
  constructor(id, actor, connectionOptions, name = 'diameter') {
    super(id, actor, name, StackApi.NetworkType.TCP, connectionOptions, DiameterConnectionClientOptions);
  }
  
  send(msg) {
    this.sendMessage(new DiameterEncoder(msg));
  }
  
  receive() {
    this.receiveMessage(new DiameterDecoder());
  }
}

module.exports = DiameterConnectionClient;
