
'use strict';

const AcctInterimInterval = require('./avp/acct-interim-interval');
const AccountingRealtimeRequired = require('./avp/accounting-realtime-required');
const AcctMultiSessionId = require('./avp/acct-multi-session-id');
const AccountingRecordNumber = require('./avp/accounting-record-number');
const AccountingRecordType = require('./avp/accounting-record-type');
const AcctSessionId = require('./avp/acct-session-id');
const AccountingSubSessionId = require('./avp/accounting-sub-session-id');
const AcctApplicationId = require('./avp/acct-application-id');
const AuthApplicationId = require('./avp/auth-application-id');
const AuthRequestType = require('./avp/auth-request-type');
const AuthorizationLifetime = require('./avp/authorization-lifetime');
const AuthGracePeriod = require('./avp/auth-grace-period');
const AuthSessionState = require('./avp/auth-session-state');
const ReAuthRequestType = require('./avp/re-auth-request-type');
const Class = require('./avp/class');
const DestinationHost = require('./avp/destination-host');
const DestinationRealm = require('./avp/destination-realm');
const DisconnectCause = require('./avp/disconnect-cause');
const ErrorMessage = require('./avp/error-message');
const ErrorReportingHost = require('./avp/error-reporting-host');
const EventTimestamp = require('./avp/event-timestamp');
const ExperimentalResult = require('./avp/experimental-result');
const ExperimentalResultCode = require('./avp/experimental-result-code');
const FailedAVP = require('./avp/failed-avp');
const FirmwareRevision = require('./avp/firmware-revision');
const HostIPAddress = require('./avp/host-ip-address');
const InbandSecurityId = require('./avp/inband-security-id');
const MultiRoundTimeOut = require('./avp/multi-round-time-out');
const OriginHost = require('./avp/origin-host');
const OriginRealm = require('./avp/origin-realm');
const OriginStateId = require('./avp/origin-state-id');
const ProductName = require('./avp/product-name');
const ProxyHost = require('./avp/proxy-host');
const ProxyInfo = require('./avp/proxy-info');
const ProxyState = require('./avp/proxy-state');
const RedirectHost = require('./avp/redirect-host');
const RedirectHostUsage = require('./avp/redirect-host-usage');
const RedirectMaxCacheTime = require('./avp/redirect-max-cache-time');
const ResultCode = require('./avp/result-code');
const RouteRecord = require('./avp/route-record');
const SessionId = require('./avp/session-id');
const SessionTimeout = require('./avp/session-timeout');
const SessionBinding = require('./avp/session-binding');
const SessionServerFailover = require('./avp/session-server-failover');
const SupportedVendorId = require('./avp/supported-vendor-id');
const TerminationCause = require('./avp/termination-cause');
const UserName = require('./avp/user-name');
const VendorId = require('./avp/vendor-id');
const VendorSpecificApplicationId = require('./avp/vendor-specific-application-id');


class DiameterAvpFactory {
  static create(avpCode) {
    const avpFactory = DiameterAvpFactory.AVP_DATA.get(avpCode);
    if(undefined !== avpFactory) {
      return avpFactory;
    }
  }
}

DiameterAvpFactory.AVP_DATA = new Map([
  [85, AcctInterimInterval],
  [483, AccountingRealtimeRequired],
  [50, AcctMultiSessionId],
  [485, AccountingRecordNumber],
  [480, AccountingRecordType],
  [44, AcctSessionId],
  [287, AccountingSubSessionId],
  [259, AcctApplicationId],
  [258, AuthApplicationId],
  [274, AuthRequestType],
  [291, AuthorizationLifetime],
  [276, AuthGracePeriod],
  [277, AuthSessionState],
  [285, ReAuthRequestType],
  [25, Class],
  [293, DestinationHost],
  [283, DestinationRealm],
  [273, DisconnectCause],
  [281, ErrorMessage],
  [294, ErrorReportingHost],
  [55, EventTimestamp],
  [297, ExperimentalResult],
  [298, ExperimentalResultCode],
  [279, FailedAVP],
  [267, FirmwareRevision],
  [257, HostIPAddress],
  [299, InbandSecurityId],
  [272, MultiRoundTimeOut],
  [264, OriginHost],
  [296, OriginRealm],
  [278, OriginStateId],
  [269, ProductName],
  [280, ProxyHost],
  [284, ProxyInfo],
  [33, ProxyState],
  [292, RedirectHost],
  [261, RedirectHostUsage],
  [262, RedirectMaxCacheTime],
  [268, ResultCode],
  [282, RouteRecord],
  [263, SessionId],
  [27, SessionTimeout],
  [270, SessionBinding],
  [271, SessionServerFailover],
  [265, SupportedVendorId],
  [295, TerminationCause],
  [1, UserName],
  [266, VendorId],
  [260, VendorSpecificApplicationId]
]);


module.exports = DiameterAvpFactory;
