
'use strict';


class DiameterConstHeader {}

DiameterConstHeader.VERSION = '#section-3';
DiameterConstHeader.MESSAGE_LENGTH = '#section-3';
DiameterConstHeader.COMMAND_FLAGS = '#section-3';
DiameterConstHeader.COMMAND_CODE = '#section-3';
DiameterConstHeader.APPLICATION_ID = '#section-3';
DiameterConstHeader.HOP_BY_HOP_IDENTIFIER = '#section-3';
DiameterConstHeader.END_TO_END_IDENTIFIER = '#section-3';

DiameterConstHeader.R_BIT = '#section-3.2';
DiameterConstHeader.P_BIT = '#section-3.2';
DiameterConstHeader.E_BIT = '#section-3.2';
DiameterConstHeader.T_BIT = '#section-3.2';

DiameterConstHeader.R_BIT_TEXT = ['Answer', 'Request'];
DiameterConstHeader.P_BIT_TEXT = ['Not Proxiable', 'Proxiable'];
DiameterConstHeader.E_BIT_TEXT = ['No Error', 'Error'];
DiameterConstHeader.T_BIT_TEXT = ['No retransmitted message', 'Potentially retransmitted message'];


module.exports = DiameterConstHeader;
