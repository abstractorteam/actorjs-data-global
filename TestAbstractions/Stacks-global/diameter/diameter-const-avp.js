
'use strict';


class DiameterConstAvp {
  static get(avpCode) {
    const avpData = DiameterConstAvp.AVP_DATA.get(avpCode);
    if(undefined !== avpData) {
      return avpData;
    }
    else {
      return DiameterConstAvp.UNKNOWN;
    }
  }
}


DiameterConstAvp.AVP_CODE = '#section-4.1';
DiameterConstAvp.AVP_FLAGS = '#section-4.1';
DiameterConstAvp.AVP_LENGTH = '#section-4.1';
DiameterConstAvp.VENDER_ID = '#section-4.1.1';

DiameterConstAvp.FLOAT_32 = 0;
DiameterConstAvp.FLOAT_64 = 1;
DiameterConstAvp.GROUPED = 2;
DiameterConstAvp.INTEGER_32 = 3;
DiameterConstAvp.INTEGER_64 = 4;
DiameterConstAvp.OCTET_STRING = 5;
DiameterConstAvp.UNSIGNED_32 = 6;
DiameterConstAvp.UNSIGNED_64 = 7;

DiameterConstAvp.ADDRESS = 16;
DiameterConstAvp.DIAMETER_IDENTITY = 17;
DiameterConstAvp.DIAMETER_URI = 18;
DiameterConstAvp.ENUMERATED = 19;
DiameterConstAvp.IP_FILTER_RULE = 20;
DiameterConstAvp.TIME = 21;
DiameterConstAvp.UTF_8_STRING = 22;

DiameterConstAvp.V_BIT = '#section-4.1';
DiameterConstAvp.M_BIT = '#section-4.1';
DiameterConstAvp.P_BIT = '#section-4.1';

DiameterConstAvp.V_BIT_TEXT = ['Not Vendor-Specific', 'Vendor-Specific'];
DiameterConstAvp.M_BIT_TEXT = ['Not Mandatory', 'Mandatory'];
DiameterConstAvp.P_BIT_TEXT = ['Reserved for future usage of end-to-end security', 'Reserved for future usage of end-to-end security, SHOULD not be used'];

DiameterConstAvp.AVP_TYPE = new Map([
  ['Float32', {links: ['#section-4.2']}],
  ['Float64', {links: ['#section-4.2']}],
  ['Grouped', {links: ['#section-4.2']}],
  ['Integer32', {links: ['#section-4.2']}],
  ['Integer64', {links: ['#section-4.2']}],
  ['OctetString', {links: ['#section-4.2']}],
  ['Unsigned32', {links: ['#section-4.2']}],
  ['Unsigned64', {links: ['#section-4.2']}],
  
  ['Address', {links: ['#section-4.3.1']}],
  ['DiameterIdentity', {links: ['#section-4.3.1']}],
  ['DiameterURI', {links: ['#section-4.3.1']}],
  ['Enumerated', {links: ['#section-4.3.1']}],
  ['IPFilterRule', {links: ['#section-4.3.1']}],
  ['Time', {links: ['#section-4.3.1']}],
  ['UTF8String', {links: ['#section-4.3.1']}]
]);

DiameterConstAvp.AVP_DATA = new Map([
  [85, {links: ['#section-9.8.2'], name: 'Acct-Interim-Interval', type: 'Unsigned32'}],
  [483, {links: ['#section-9.8.7'], name: 'Accounting-Realtime-Required', type: 'Enumerated', typeBasic: 'Integer32'}],
  [50, {links: ['#section-9.8.5'], name: 'Acct-Multi-Session-Id', type: 'UTF8String', typeBasic: 'OctetString'}],
  [485, {links: ['#section-9.8.3'], name: 'Accounting-Record-Number', type: 'Unsigned32'}],
  [480, {links: ['#section-9.8.1'], name: 'Accounting-Record-Type', type: 'Enumerated', typeBasic: 'Integer32'}],
  [44, {links: ['#section-9.8.4'], name: 'Acct-Session-Id', type: 'OctetString'}],
  [287, {links: ['#section-9.8.6'], name: 'Accounting-Sub-Session-Id', type: 'Unsigned64'}],
  [259, {links: ['#section-6.9'], name: 'Acct-Application-Id', type: 'Unsigned32'}],
  [258, {links: ['#section-6.8'], name: 'Auth-Application-Id', type: 'Unsigned32'}],
  [274, {links: ['#section-8.7'], name: 'Auth-Request-Type', type: 'Enumerated', typeBasic: 'Integer32'}],
  [291, {links: ['#section-8.9'], name: 'Authorization-Lifetime', type: 'Unsigned32'}],
  [276, {links: ['#section-8.10'], name: 'Auth-Grace-Period', type: 'Unsigned32'}],
  [277, {links: ['#section-8.11'], name: 'Auth-Session-State', type: 'Enumerated', typeBasic: 'Integer32'}],
  [285, {links: ['#section-8.12'], name: 'Re-Auth-Request-Type', type: 'Enumerated', typeBasic: 'Integer32'}],
  [25, {links: ['#section-8.20'], name: 'Class', type: 'OctetString'}],
  [293, {links: ['#section-6.5'], name: 'Destination-Host', type: 'DiameterIdentity', typeBasic: 'OctetString'}],
  [283, {links: ['#section-6.6'], name: 'Destination-Realm', type: 'DiameterIdentity', typeBasic: 'OctetString'}],
  [273, {links: ['#section-5.4.3'], name: 'Disconnect-Cause', type: 'Enumerated', typeBasic: 'Integer32'}],
  [281, {links: ['#section-7.3'], name: 'Error-Message', type: 'UTF8String', typeBasic: 'OctetString'}],
  [294, {links: ['#section-7.4'], name: 'Error-Reporting-Host', type: 'DiameterIdentity', typeBasic: 'OctetString'}],
  [55, {links: ['#section-8.21'], name: 'Event-Timestamp', type: 'Time', typeBasic: 'OctetString'}],
  [297, {links: ['#section-7.6'], name: 'Experimental-Result', type: 'Grouped'}],
  [298, {links: ['#section-7.7'], name: 'Experimental-Result-Code', type: 'Unsigned32'}],
  [279, {links: ['#section-7.5'], name: 'Failed-AVP', type: 'Grouped'}],
  [267, {links: ['#section-5.3.4'], name: 'Firmware-Revision', type: 'Unsigned32'}],
  [257, {links: ['#section-5.3.5'], name: 'Host-IP-Address', type: 'Address', typeBasic: 'OctetString'}],
  [299, {links: ['#section-6.10'], name: 'Inband-Security-Id', type: 'Unsigned32'}],
  [272, {links: ['#section-8.19'], name: 'Multi-Round-Time-Out', type: 'Unsigned32'}],
  [264, {links: ['#section-6.3'], name: 'Origin-Host', type: 'DiameterIdentity', typeBasic: 'OctetString'}],
  [296, {links: ['#section-6.4'], name: 'Origin-Realm', type: 'DiameterIdentity', typeBasic: 'OctetString'}],
  [278, {links: ['#section-8.16'], name: 'Origin-State-Id', type: 'Unsigned32'}],
  [269, {links: ['#section-5.3.7'], name: 'Product-Name', type: 'UTF8String', typeBasic: 'OctetString'}],
  [280, {links: ['#section-6.7.3'], name: 'Proxy-Host', type: 'DiameterIdentity', typeBasic: 'OctetString'}],
  [284, {links: ['#section-6.7.2'], name: 'Proxy-Info', type: 'Grouped'}],
  [33, {links: ['#section-6.7.4'], name: 'Proxy-State', type: 'OctetString'}],
  [292, {links: ['#section-6.12'], name: 'Redirect-Host', type: 'DiameterURI', typeBasic: 'OctetString'}],
  [261, {links: ['#section-6.13'], name: 'Redirect-Host-Usage', type: 'Enumerated', typeBasic: 'Integer32'}],
  [262, {links: ['#section-6.14'], name: 'Redirect-Max-Cache-Time', type: 'Unsigned32'}],
  [268, {links: ['#section-7.1'], name: 'Result-Code', type: 'Unsigned32'}],
  [282, {links: ['#section-6.7.1'], name: 'Route-Record', type: 'DiameterIdentity', typeBasic: 'OctetString'}],
  [263, {links: ['#section-8.8'], name: 'Session-Id', type: 'UTF8String', typeBasic: 'OctetString'}],
  [27, {links: ['#section-8.13'], name: 'Session-Timeout', type: 'Unsigned32'}],
  [270, {links: ['#section-8.17'], name: 'Session-Binding', type: 'Unsigned32'}],
  [271, {links: ['#section-8.18'], name: 'Session-Server-Failover', type: 'Enumerated', typeBasic: 'Integer32'}],
  [265, {links: ['#section-5.3.6'], name: 'Supported-Vendor-Id', type: 'Unsigned32'}],
  [295, {links: ['#section-8.15'], name: 'Termination-Cause', type: 'Enumerated', typeBasic: 'Integer32'}],
  [1, {links: ['#section-8.14'], name: 'User-Name', type: 'UTF8String', typeBasic: 'OctetString'}],
  [266, {links: ['#section-5.3.3'], name: 'Vendor-Id', type: 'Unsigned32'}],
  [260, {links: ['#section-6.11'], name: 'Vendor-Specific-Application-Id', type: 'Grouped'}]
]);


DiameterConstAvp.UNKNOWN = {links: [''], name: 'UNKNOWN', type: ''};


module.exports = DiameterConstAvp;
