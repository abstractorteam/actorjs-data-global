
'use strict';


class DiameterStyle {
  constructor(index) {
    this.index = index;
    this.textColor= 'black';
    this.textProtocolColor = 'black';
    this.protocolColor = '#F08080';
    this.protocolBackgroundColor = 'rgba(240, 128, 128, 0.3)';
  }
}

module.exports = DiameterStyle;
