
'use strict';

const Integer32 = require('../avp-basic/integer-32');
const DiameterConstAvp = require('../diameter-const-avp');


class Enumerated extends Integer32 {
  constructor(code, value, mBit=0, vbit=0, pBit=0, vendorId=0) {
    super(code, value, mBit, vbit, pBit, vendorId);
    this.typeDerived = DiameterConstAvp.ENUMERATED;
  }
}

module.exports = Enumerated;
