
'use strict';

const OctetString = require('../avp-basic/octet-string');
const DiameterConstAvp = require('../diameter-const-avp');


class UTF8String extends OctetString {
  constructor(code, value, mBit=0, vbit=0, pBit=0, vendorId=0) {
    super(code, value, mBit, vbit, pBit, vendorId);
    this.typeDerived = DiameterConstAvp.UTF_8_STRING;
  }
}

module.exports = UTF8String;
