
'use strict';

const OctetString = require('../avp-basic/octet-string');
const DiameterConstAvp = require('../diameter-const-avp');


class DiameterURI extends OctetString {
  constructor(code, value, mBit = 0, vbit = 0, pBit = 0, vendorId = 0) {
    super(code, value, mBit, vbit, pBit, vendorId);
    this.typeDerived = DiameterConstAvp.DIAMETER_URI;
  }
}

module.exports = DiameterURI;
