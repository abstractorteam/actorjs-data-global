
'use strict';

const DiameterMsg = require('../diameter-msg');
const DiameterConstCommandCode = require('../diameter-const-command-code');


class DeviceWatchdogRequest extends DiameterMsg {
  constructor(applicationID, version = 1, commandFlags = 0b10000000, messageLength, hopByHopIdentifier, endToEndIdentifier) {
    super(DiameterConstCommandCode.DEVICE_WATCHDOG, applicationID, version, commandFlags, messageLength, hopByHopIdentifier, endToEndIdentifier);
  }
}

module.exports = DeviceWatchdogRequest;
