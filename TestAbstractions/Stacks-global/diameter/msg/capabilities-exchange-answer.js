
'use strict';

const DiameterMsg = require('../diameter-msg');
const DiameterConstCommandCode = require('../diameter-const-command-code');
const ResultCode = require('../avp/result-code');
const OriginHost = require('../avp/origin-host');
const OriginRealm = require('../avp/origin-realm');
const HostIPAddress = require('../avp/host-ip-address');
const VendorId = require('../avp/vendor-id');
const ProductName = require('../avp/product-name');
const OriginStateId = require('../avp/origin-state-id');
const ErrorMessage = require('../avp/error-message');
const FailedAVP = require('../avp/failed-avp');
const SupportedVendorId = require('../avp/supported-vendor-id');
const AuthApplicationId = require('../avp/auth-application-id');
const InbandSecurityId = require('../avp/inband-security-id');
const AcctApplicationId = require('../avp/acct-application-id');
const VendorSpecificApplicationId = require('../avp/vendor-specific-application-id');
const FirmwareRevision = require('../avp/firmware-revision');


class CapabilitiesExchangeAnswer extends DiameterMsg {
  constructor(applicationID, version = 1, commandFlags = 0b00000000, messageLength, hopByHopIdentifier, endToEndIdentifier) {
    super(DiameterConstCommandCode.CAPABILITIES_EXCHANGE, applicationID, version, commandFlags, messageLength, hopByHopIdentifier, endToEndIdentifier);
  }
  
  addAvpResultCode(resultCode) {
    this.addAvp(ResultCode, resultCode);
  }
  
  addAvpOriginHost(originHost) {
    this.addAvp(OriginHost, originHost);
  }
  
  addAvpOriginRealm(originRealm) {
    this.addAvp(OriginRealm, originRealm);
  }

  addAvpHostIPAddress(hostIpAddress) {
    this.addAvp(HostIPAddress, hostIpAddress);
  }

  addAvpVendorId(vendorId) {
    this.addAvp(VendorId, vendorId);
  }

  addAvpProductName(productName) {
    this.addAvp(ProductName, productName);
  }

  addAvpOriginStateId(originStateId) {
    this.addAvp(OriginStateId, originStateId);
  }
  
  addAvpErrorMessage(errorMessage) {
    this.addAvp(ErrorMessage, errorMessage);
  }
  
  addAvpFailedAVP(failedAVP) {
    this.addAvp(FailedAVP, failedAVP);
  }

  addAvpSupportedVendorId(supportedVendorId) {
    this.addAvp(SupportedVendorId, supportedVendorId);
  }

  addAvpAuthApplicationId(authApplicationId) {
    this.addAvp(AuthApplicationId, authApplicationId);
  }

  addAvpInbandSecurityId(inbandSecurityId) {
    this.addAvp(InbandSecurityId, inbandSecurityId);
  }

  addAvpAcctApplicationId(acctApplicationId) {
    this.addAvp(AcctApplicationId, acctApplicationId);
  }

  addAvpVendorSpecificApplicationId(vendorSpecificApplicationId) {
    this.addAvp(VendorSpecificApplicationId, vendorSpecificApplicationId);
  }

  addAvpFirmwareRevision(firmwareRevision) {
    this.addAvp(FirmwareRevision, firmwareRevision);
  }
}

module.exports = CapabilitiesExchangeAnswer;
