
'use strict';

const DiameterMsg = require('../diameter-msg');
const DiameterConstCommandCode = require('../diameter-const-command-code');


class SessionTerminationAnswer extends DiameterMsg {
  constructor(applicationID, version = 1, commandFlags = 0b00000000, messageLength, hopByHopIdentifier, endToEndIdentifier) {
    super(DiameterConstCommandCode.SESSION_TERMINATION, applicationID, version, commandFlags, messageLength, hopByHopIdentifier, endToEndIdentifier);
  }
}

module.exports = SessionTerminationAnswer;
