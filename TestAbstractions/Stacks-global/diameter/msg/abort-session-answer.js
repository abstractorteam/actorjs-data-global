
'use strict';

const DiameterMsg = require('../diameter-msg');
const DiameterConstCommandCode = require('../diameter-const-command-code');


class AbortSessionAnswer extends DiameterMsg {
  constructor(applicationID, version = 1, commandFlags = 0b00000000, messageLength, hopByHopIdentifier, endToEndIdentifier) {
    super(DiameterConstCommandCode.ABORT_SESSION, applicationID, version, commandFlags, messageLength, hopByHopIdentifier, endToEndIdentifier);
  }
}

module.exports = AbortSessionAnswer;
