
'use strict';

const Unsigned64 = require('../avp-basic/unsigned-64');


class AccountingSubSessionId extends Unsigned64 {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(287, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = AccountingSubSessionId;
