
'use strict';

const OctetString = require('../avp-basic/octet-string');


class AcctSessionId extends OctetString {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(44, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = AcctSessionId;
