
'use strict';

const UTF8String = require('../avp-derived/utf-8-string');


class AcctMultiSessionId extends UTF8String {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(85, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = AcctMultiSessionId;
