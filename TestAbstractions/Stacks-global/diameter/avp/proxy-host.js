
'use strict';

const DiameterIdentity = require('../avp-derived/diameter-identity');


class ProxyHost extends DiameterIdentity {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(280, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = ProxyHost;
