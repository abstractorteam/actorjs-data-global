
'use strict';

const UTF8String = require('../avp-derived/utf-8-string');


class ErrorMessage extends UTF8String {
  constructor(value, mBit=0, vbit=0, pBit=0, vendorId=0) {
    super(281, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = ErrorMessage;
