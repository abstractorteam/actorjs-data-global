
'use strict';

const UTF8String = require('../avp-derived/utf-8-string');


class ProductName extends UTF8String {
  constructor(value, mBit=0, vbit=0, pBit=0, vendorId=0) {
    super(269, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = ProductName;
