
'use strict';

const Enumerated = require('../avp-derived/enumerated');


class AuthRequestType extends Enumerated {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(274, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = AuthRequestType;
