
'use strict';

const Unsigned32 = require('../avp-basic/unsigned-32');


class SessionTimeout extends Unsigned32 {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(27, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = SessionTimeout;
