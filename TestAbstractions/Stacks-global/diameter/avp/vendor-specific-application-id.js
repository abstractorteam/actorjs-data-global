
'use strict';

const Grouped = require('../avp-basic/grouped');


class VendorSpecificApplicationId extends Grouped {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(260, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = VendorSpecificApplicationId;
