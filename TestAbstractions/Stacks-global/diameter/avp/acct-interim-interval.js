
'use strict';

const Integer32 = require('../avp-basic/integer-32');


class AcctInterimInterval extends Integer32 {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(85, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = AcctInterimInterval;
