
'use strict';

const UTF8String = require('../avp-derived/utf-8-string');


class UserName extends UTF8String {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(1, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = UserName;
