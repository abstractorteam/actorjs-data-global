
'use strict';

const Unsigned32 = require('../avp-basic/unsigned-32');


class MultiRoundTimeOut extends Unsigned32 {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(272, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = MultiRoundTimeOut;
