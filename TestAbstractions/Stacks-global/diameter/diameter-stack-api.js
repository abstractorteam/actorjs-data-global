
'use strict';

const AvpHeader = require('./avp-basic/avp-header');
const Float32 = require('./avp-basic/float-32');
const Float64 = require('./avp-basic/float-64');
const Grouped = require('./avp-basic/grouped');
const Integer32 = require('./avp-basic/integer-32');
const Integer64 = require('./avp-basic/integer-64');
const OctetString = require('./avp-basic/octet-string');
const Unsigned32 = require('./avp-basic/unsigned-32');
const Unsigned64 = require('./avp-basic/unsigned-64');

const Address = require('./avp-derived/address');
const DiameterIdentity = require('./avp-derived/diameter-identity');
const DiameterURI = require('./avp-derived/diameter-uri');
const Enumerated = require('./avp-derived/enumerated');
const IPFilterRule = require('./avp-derived/ip-filter-rule');
const Time = require('./avp-derived/time');
const UTF8String = require('./avp-derived/utf-8-string');

const AccountingRealtimeRequired = require('./avp/accounting-realtime-required');
const AccountingRecordNumber = require('./avp/accounting-record-number');
const AccountingRecordType = require('./avp/accounting-record-type');
const AccountingSubSessionId = require('./avp/accounting-sub-session-id');
const AcctApplicationId = require('./avp/acct-application-id');
const AcctInterimInterval = require('./avp/acct-interim-interval');
const AcctMultiSessionId = require('./avp/acct-multi-session-id');
const AcctSessionId = require('./avp/acct-session-id');
const AuthApplicationId = require('./avp/auth-application-id');
const AuthGracePeriod = require('./avp/auth-grace-period');
const AuthRequestType = require('./avp/auth-request-type');
const AuthSessionState = require('./avp/auth-session-state');
const AuthorizationLifetime = require('./avp/authorization-lifetime');
const Class = require('./avp/class');
const DestinationHost = require('./avp/destination-host');
const DestinationRealm = require('./avp/destination-realm');
const DisconnectCause = require('./avp/disconnect-cause');
const ErrorMessage = require('./avp/error-message');
const ErrorReportingHost = require('./avp/error-reporting-host');
const EventTimestamp = require('./avp/event-timestamp');
const ExperimentalResultCode = require('./avp/experimental-result-code');
const ExperimentalResult = require('./avp/experimental-result');
const FailedAVP = require('./avp/failed-avp');
const FirmwareRevision = require('./avp/firmware-revision');
const HostIPAddress = require('./avp/host-ip-address');
const InbandSecurityId = require('./avp/inband-security-id');
const MultiRoundTimeOut = require('./avp/multi-round-time-out');
const OriginHost = require('./avp/origin-host');
const OriginRealm = require('./avp/origin-realm');
const OriginStateId = require('./avp/origin-state-id');
const ProductName = require('./avp/product-name');
const ProxyHost = require('./avp/proxy-host');
const ProxyInfo = require('./avp/proxy-info');
const ProxyState = require('./avp/proxy-state');
const ReAuthRequestType = require('./avp/re-auth-request-type');
const RedirectHostUsage = require('./avp/redirect-host-usage');
const RedirectHost = require('./avp/redirect-host');
const RedirectMaxCacheTime = require('./avp/redirect-max-cache-time');
const ResultCode = require('./avp/result-code');
const RouteRecord = require('./avp/route-record');
const SessionBinding = require('./avp/session-binding');
const SessionId = require('./avp/session-id');
const SessionServerFailover = require('./avp/session-server-failover');
const SessionTimeout = require('./avp/session-timeout');
const SupportedVendorId = require('./avp/supported-vendor-id');
const TerminationCause = require('./avp/termination-cause');
const UserName = require('./avp/user-name');
const VendorId = require('./avp/vendor-id');
const VendorSpecificApplicationId = require('./avp/vendor-specific-application-id');

const DiameterConstResultCodes = require('./diameter-const-result-codes');

const CapabilitiesExchangeAnswer = require('./msg/capabilities-exchange-answer');
const CapabilitiesExchangeRequest = require('./msg/capabilities-exchange-request');

const DiameterMsg = require('./diameter-msg');

const DiameterEncoder = require('./diameter-encoder');
const DiameterDecoder = require('./diameter-decoder');

const DiameterStyle = require('./diameter-style');


const exportsObject = {
  avpBasic: {
    AvpHeader: AvpHeader,
    Float32: Float32,
    Float64: Float64,
    Grouped: Grouped,
    Integer32: Integer32,
    Integer64: Integer64,
    OctetString: OctetString,
    Unsigned32: Unsigned32,
    Unsigned64: Unsigned64
  },
  avpDerived: {
    Address: Address,
    DiameterIdentity: DiameterIdentity,
    DiameterURI: DiameterURI,
    Enumerated: Enumerated,
    IPFilterRule: IPFilterRule,
    Time: Time,
    UTF8String: UTF8String
  },
  avp: {
    AccountingRealtimeRequired: AccountingRealtimeRequired,
    AccountingRecordNumber: AccountingRecordNumber,
    AccountingRecordType: AccountingRecordType,
    AccountingSubSessionId: AccountingSubSessionId,
    AcctApplicationId: AcctApplicationId,
    AcctInterimInterval: AcctInterimInterval,
    AcctMultiSessionId: AcctMultiSessionId,
    AcctSessionId: AcctSessionId,
    AuthApplicationId: AuthApplicationId,
    AuthGracePeriod: AuthGracePeriod,
    AuthRequestType: AuthRequestType,
    AuthSessionState: AuthSessionState,
    AuthorizationLifetime: AuthorizationLifetime,
    Class: Class,
    DestinationHost: DestinationHost,
    DestinationRealm: DestinationRealm,
    DisconnectCause: DisconnectCause,
    ErrorMessage: ErrorMessage,
    ErrorReportingHost: ErrorReportingHost,
    EventTimestamp: EventTimestamp,
    ExperimentalResultCode: ExperimentalResultCode,
    ExperimentalResult: ExperimentalResult,
    FailedAVP: FailedAVP,
    FirmwareRevision: FirmwareRevision,
    HostIPAddress: HostIPAddress,
    InbandSecurityId: InbandSecurityId,
    MultiRoundTimeOut: MultiRoundTimeOut,
    OriginHost: OriginHost,
    OriginRealm: OriginRealm,
    OriginStateId: OriginStateId,
    ProductName: ProductName,
    ProxyHost: ProxyHost,
    ProxyInfo: ProxyInfo,
    ProxyState: ProxyState,
    ReAuthRequestType: ReAuthRequestType,
    RedirectHostUsage: RedirectHostUsage,
    RedirectHost: RedirectHost,
    RedirectMaxCacheTime: RedirectMaxCacheTime,
    ResultCode: ResultCode,
    RouteRecord: RouteRecord,
    SessionBinding: SessionBinding,
    SessionId: SessionId,
    SessionServerFailover: SessionServerFailover,
    SessionTimeout: SessionTimeout,
    SupportedVendorId: SupportedVendorId,
    TerminationCause: TerminationCause,
    UserName: UserName,
    VendorId: VendorId,
    VendorSpecificApplicationId: VendorSpecificApplicationId
  },
  msg: {
    CapabilitiesExchangeAnswer: CapabilitiesExchangeAnswer,
    CapabilitiesExchangeRequest: CapabilitiesExchangeRequest
  },
  DiameterConstResultCodes: DiameterConstResultCodes,
  DiameterMsg: DiameterMsg,
  DiameterEncoder: DiameterEncoder,
  DiameterDecoder: DiameterDecoder,
  Style: DiameterStyle
};

module.exports = exportsObject;
