
'use strict';

const AvpHeader = require('./avp-header');
const DiameterConstAvp = require('../diameter-const-avp');


class Integer64 extends AvpHeader {
  constructor(code, value, mBit=0, vbit=0, pBit=0, vendorId=0) {
    super(code, 8, value, mBit, vbit, pBit, vendorId);
    this.typeBasic = DiameterConstAvp.INTEGER_64;
  }
  
  setValue(value) {
    this.value = value;
  }
}


module.exports = Integer64;
