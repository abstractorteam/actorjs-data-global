
'use strict';

const AvpHeader = require('./avp-header');
const DiameterConstAvp = require('../diameter-const-avp');


class Integer32 extends AvpHeader {
  constructor(code, value, mBit=0, vbit=0, pBit=0, vendorId=0) {
    super(code, 4, mBit, vbit, pBit, vendorId);
    this.value = value;
    this.typeBasic = DiameterConstAvp.INTEGER_32;
  }
  
  setValue(value) {
    this.value = value;
  }
}


module.exports = Integer32;
