
'use strict';

//  0                   1                   2                   3
//  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |                           AVP Code                            |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |V M P r r r r r|                  AVP Length                   |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |                        Vendor-ID (opt)                        |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |    Data ...
// +-+-+-+-+-+-+-+-+

class AvpHeader {
  constructor(code, length, value, mBit = 0, vbit = 0, pBit = 0, vendorId = 0) {
    this.code = code;
    this.vBit = vbit;
    this.mBit = mBit;
    this.pBit = pBit;
    this.flags = this.setBits();
    this.length = 0 === vendorId ? length + 8 : length + 12;
    this.value = value;
    this.vendorId = vendorId;
  }
  
  changeLength(length) {
    this.length = 0 === this.vendorId ? length + 8 : length + 12;
  }
  
  isVbit() {
    return 1 === this.vBit;
  }
  
  isMbit() {
    return 1 === this.mBit;
  }
  
  isPbit() {
    return 1 === this.pBit;
  }
  
  setBit(num, bit, value) {
    if(1 === value) {
      return num | 1 << bit;
    }
    else {
      return num;
    }
  }
  
  setBits() {
    let num = 0;
    num = this.setBit(num, 7, this.vBit);
    num = this.setBit(num, 6, this.mBit);
    num = this.setBit(num, 5, this.pBit);
    return num;
  }
}

module.exports = AvpHeader;
