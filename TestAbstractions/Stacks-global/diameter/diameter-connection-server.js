
'use strict';

const StackApi = require('stack-api');
const DiameterConnectionServerOptions = require('./diameter-connection-server-options');
const DiameterEncoder = require('./diameter-encoder');
const DiameterDecoder = require('./diameter-decoder');


class DiameterConnectionServer extends StackApi.ServerConnection {
  constructor(id, actor, connectionOptions) {
    super(id, actor, 'diameter', StackApi.NetworkType.TCP, connectionOptions, DiameterConnectionServerOptions);
  }
  
  receive() {
    this.receiveMessage(new DiameterDecoder());
  }
  
  send(msg) {
    this.sendMessage(new DiameterEncoder(msg));
  }
}

module.exports = DiameterConnectionServer;
