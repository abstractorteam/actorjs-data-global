
'use strict';

const StackApi = require('stack-api');
const SocketLoggerLine = require('./socket-logger-line');


class SocketEncoderLine extends StackApi.Encoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }

  *encode() {
    const buffer = Buffer.allocUnsafe(Buffer.byteLength(this.msg) + SocketEncoderLine.CL_CR_LENGTH);
    let offset = 0;
    offset += buffer.write(this.msg, offset);
    buffer.write(SocketEncoderLine.CL_CR, offset);
    yield* this.send(buffer);
    if(this.isLogIp) {
      process.nextTick(() => {
        this.caption = SocketLoggerLine.generateLog(this.msg, this.ipLogs);
        this.logMessage();
      });
    }
  }
}

SocketEncoderLine.CL_CR = '\r\n';
SocketEncoderLine.CL_CR_LENGTH = Buffer.byteLength(SocketEncoderLine.CL_CR);


module.exports = SocketEncoderLine;
