      
'use strict';

const StackApi = require('stack-api');
const SocketLoggerSize = require('./socket-logger-size');


class SocketEncoder extends StackApi.Encoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }

  *encode() {
    if(this.msg instanceof StackApi.ContentBase) {
      let buffer = null;
      while(null !== (buffer = this.msg.getBuffer())) {
        yield* this.send(buffer);
        if(this.isLogIp) {
          process.nextTick(() => {
            this.caption = SocketLoggerSize.generateLog(buffer, this.ipLogs);
            this.logMessage();
          });
        }
      }
    }
    else {
      yield* this.send(this.msg);
      if(this.isLogIp) {
        process.nextTick(() => {
          this.caption = SocketLoggerSize.generateLog(this.msg, this.ipLogs);
          this.logMessage();
        });
      }
    }
  }
}


module.exports = SocketEncoder;
