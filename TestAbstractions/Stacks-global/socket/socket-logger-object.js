
'use strict';

const StackApi = require('stack-api');
const SocketConst = require('./socket-const');


class SocketLoggerObject {
  static generateLog(object, ipLogs) {
    let innerLog = new StackApi.LogInner('object');
    ipLogs.push(innerLog);
    SocketLoggerObject._addInner(object, innerLog);
    const strings = [];
    const caption = this._makeCaption(object, strings);
    return caption.length <= SocketConst.MAX_CAPTION_SIZE ? caption : caption.substring(0, SocketConst.BREAK_CAPTION_SIZE) + '...';
  }
  
  static _addInner(object, innerLog) {
    let keys = Reflect.ownKeys(object);
    keys.forEach((key) => {
      let value = Reflect.getOwnPropertyDescriptor(object, key).value;
      if("object" === typeof value) {
        let newInnerLog = innerLog.add(new StackApi.LogInner(key));
        SocketLoggerObject._addInner(Reflect.getOwnPropertyDescriptor(object, key).value, newInnerLog);
      }
      else {
        innerLog.add(new StackApi.LogInner(`${key}: ${value}`));
      }
    });
  }
  
  static _makeCaption(object, strings) {
    strings.push('{');
    const keys = Reflect.ownKeys(object);
    let first = true;
    for(let i = 0; i < keys.length; ++i) {
      if(!first) {
        strings.push(`,`);
      }
      first = false;
      strings.push(keys[i]);
      strings.push(':');
      const value = Reflect.get(object, keys[i]);
      if('string' === typeof value) {
        strings.push(`'${value}'`);
      }
      else if('object' === typeof value) {
        this._makeCaption(value, strings);
      }
      else {
        strings.push(value);
      }
    }
    strings.push('}');
    return strings.join('');
  }
}

module.exports = SocketLoggerObject;
