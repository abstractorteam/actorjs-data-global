# **Socket**

## **Description**
The Socket [Stack](stacks) is a [Stack](stacks) which acts like common sockets. There are one client version and one server version. The interface for using them is the same. The only difference is how the engine creates and destroys them.

When testing none standard protocols there are two different was to do it.
* **Writing a new [Stack](stacks)**: If the implementation will be reused a lot of times in different Actors and Test Cases the best way is to write a new stack implementing the protocol.
* **Using the Socket [Stack](stacks)**: If there will be no reuse just use the Socket [Stack](stacks) and implement the encoding and decoding in the Actors.

