
'use strict';

const StackApi = require('stack-api');
const SocketConnectionServerOptions = require('./socket-connection-server-options');
const SocketEncoderLine = require('./socket-encoder-line');
const SocketEncoderSize = require('./socket-encoder-size');
const SocketEncoderObject = require('./socket-encoder-object');
const SocketDecoderLine = require('./socket-decoder-line');
const SocketDecoderSize = require('./socket-decoder-size');
const SocketDecoderObject = require('./socket-decoder-object');


class SocketConnectionServer extends StackApi.ServerConnection {
  constructor(id, actor, connectionOptions) {
    super(id, actor, 'socket', StackApi.NetworkType.TCP, connectionOptions, SocketConnectionServerOptions);
  }
  
  sendLine(msg) {
    this.sendMessage(new SocketEncoderLine(msg));
  }

  send(msg) {
    this.sendMessage(new SocketEncoderSize(msg));
  }
  
  sendObject(object) {
    this.sendMessage(new SocketEncoderObject(object));
  }
  
  receiveLine() {
    this.receiveMessage(new SocketDecoderLine());
  }
  
  receiveSize(size) {
    this.receiveMessage(new SocketDecoderSize(size));
  }
  
  receiveObject() {
    this.receiveMessage(new SocketDecoderObject());
  }
}

module.exports = SocketConnectionServer;
