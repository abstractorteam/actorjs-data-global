
'use strict';

const StackApi = require('stack-api');
const SocketConst = require('./socket-const');
                            

class SocketLoggerSize {
  static generateLog(buffer, ipLogs) {
    if(Buffer.isBuffer(buffer)) {
      return StackApi.BinaryLog.generateLog(buffer, ipLogs);
    }
    else if(typeof buffer === 'string') {
      const asciiMsg = `${StackApi.AsciiDictionary.getSymbolString(buffer)}`;
      ipLogs.push(new StackApi.LogInner(asciiMsg));
      return asciiMsg.length <= SocketConst.MAX_CAPTION_SIZE ? asciiMsg : asciiMsg.substring(0, SocketConst.BREAK_CAPTION_SIZE) + '...';
    }
  }
}


module.exports = SocketLoggerSize;
