
'use strict';

const StackApi = require('stack-api');
const SocketLoggerSize = require('./socket-logger-size');


class SocketDecoderSize extends StackApi.Decoder {
  constructor(size) {
    super();
    this.size = size;
  }
  
  *decode() {
    const msg = yield* this.receiveSize(this.size);
    if(this.isLogIp) {
      process.nextTick(() => {
        this.caption = SocketLoggerSize.generateLog(msg, this.ipLogs);
        this.logMessage();
      });
    }
    return msg;
  }
}

module.exports = SocketDecoderSize;
