
'use strict';

const StackApi = require('stack-api');
const SocketConst = require('./socket-const');


class SocketLoggerLine {
  static generateLog(msg, ipLogs) {
    let readableMsg = `${StackApi.AsciiDictionary.getSymbolString(msg)}`;
    let innerLog = new StackApi.LogInner(`'${readableMsg}'`);
    ipLogs.push(innerLog);
    return readableMsg.length <= SocketConst.MAX_CAPTION_SIZE ? readableMsg : readableMsg.substring(0, SocketConst.BREAK_CAPTION_SIZE) + '...';
  }
}

module.exports = SocketLoggerLine;
