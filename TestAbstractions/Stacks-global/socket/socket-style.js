
'use strict';


class SocketStyle {
  constructor(index) {
    this.index = index;
    this.textColor= 'black';
    this.textProtocolColor = 'black';
    this.protocolColor = '#00BFFF';
    this.protocolBackgroundColor = 'rgba(0, 191, 255, 0.3)';
  }
}

module.exports = SocketStyle;
