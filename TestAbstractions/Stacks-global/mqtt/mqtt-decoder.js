
'use strict';

const StackApi = require('stack-api');


class MqttDecoderDecoder extends StackApi.Decoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }
}

module.exports = MqttDecoderDecoder;
