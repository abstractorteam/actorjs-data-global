
'use strict';


class MqttStyle {
  constructor(index) {
    this.index = index;
    this.textColor= 'black';
    this.textProtocolColor = 'bisque';
    this.protocolColor = 'DarkMagenta';
    this.protocolBackgroundColor = 'rgba(240, 128, 128, 0.3)';
  }
}

module.exports = MqttStyle;
