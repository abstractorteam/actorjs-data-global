
'use strict';


class MqttConst {}

MqttConst.DOCUMENTATION_LINK_ROOT = 'https://docs.oasis-open.org/mqtt/';

MqttConst.FIXED_HEADER = 'mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901021';
MqttConst.FIXED_HEADER_TYPE = 'mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901022';
MqttConst.FIXED_HEADER_FLAGS = 'mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901023';
MqttConst.FIXED_HEADER_REMAINING_LENGTH = 'mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901024';

MqttConst.Reserved = 0;
MqttConst.CONNECT = 1;
MqttConst.CONNACK = 2;
MqttConst.PUBLISH = 3;
MqttConst.PUBACK = 4;
MqttConst.PUBREC = 5;
MqttConst.PUBREL = 6;
MqttConst.PUBCOMP = 7;
MqttConst.SUBSCRIBE = 8;
MqttConst.SUBACK = 9;
MqttConst.UNSUBSCRIBE = 10;
MqttConst.UNSUBACK = 11;
MqttConst.PINGREQ = 12;
MqttConst.PINGRESP = 13;
MqttConst.DISCONNECT = 14;
MqttConst.AUTH = 15


module.exports = MqttConst;
