
'use strict';

const StackApi = require('stack-api');
const MqttConnectionServerOptions = require('./mqtt-connection-server-options');
const MqttEncoder = require('./mqtt-encoder');
const MqttDecoder = require('./mqtt-decoder');


class MqttConnectionServerConnectionServer extends StackApi.ServerConnection {
  constructor(id, actor, connectionOptions, name = 'mqtt') {
    super(id, actor, name, StackApi.NetworkType.TCP, connectionOptions, MqttConnectionServerOptions);
  }
  
  receive() {
    this.receiveMessage(new MqttDecoder());
  }
  
  send(msg) {
    this.sendMessage(new MqttEncoder(msg));
  }
}

module.exports = MqttConnectionServerConnectionServer;
