
'use strict';

const StackApi = require('stack-api');
const MqttConnectionClientOptions = require('./mqtt-connection-client-options');
const MqttEncoder = require('./mqtt-encoder');
const MqttDecoder = require('./mqtt-decoder');


class MqttConnectionClientConnectionClient extends StackApi.ClientConnection {
  constructor(id, actor, connectionOptions, name = 'mqtt') {
    super(id, actor, name, StackApi.NetworkType.TCP, connectionOptions, MqttConnectionClientOptions);
  }
  
  send(msg) {
    this.sendMessage(new MqttEncoder(msg));
  }
  
  receive() {
    this.receiveMessage(new MqttDecoder());
  } 
}

module.exports = MqttConnectionClientConnectionClient;
