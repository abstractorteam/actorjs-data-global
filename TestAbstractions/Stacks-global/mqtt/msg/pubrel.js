
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Pubrel extends MqttMsg {
  constructor(flags = 0b0010) {
    super(MqttConst.PUBREL, flags);
  }
}

module.exports = Pubrel;
