
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Connect extends MqttMsg {
  constructor(flags = 0b0000, connectFlags = 0b11001110, keepAlive = 0) {
    super(MqttConst.CONNECT, flags);
    this.protocolName = 'MQTT';
    this.protocolVersion = 5;
    this.connectFlagss = connectFlags;
    this.keepAlive = keepAlive;
    this.propertyLength = 0;
  }
  
  getFlagCleanStart() {
    return this._getBit(this.connectFlags, 1);
  }
  
  getFlagWill() {
    return this._getBit(this.connectFlags, 2);
  }
  
  getFlagWillRetain() {
    return this._getBit(this.connectFlags, 5);
  }
  
  getFlagPassword() {
    return this._getBit(this.connectFlags, 6);
  }
  
  getFlagUserName() {
    return this._getBit(this.connectFlags, 7);
  }
}

module.exports = Connect;
