
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Pubcomp extends MqttMsg {
  constructor(flags = 0b0000) {
    super(MqttConst.PUBCOMP, flags);
  }
}

module.exports = Pubcomp;
