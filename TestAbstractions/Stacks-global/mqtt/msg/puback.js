
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Puback extends MqttMsg {
  constructor(flags = 0b0000) {
    super(MqttConst.PUBACK, flags);
  }
}

module.exports = Puback;
