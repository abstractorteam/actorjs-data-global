
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Connack extends MqttMsg {
  constructor(flags = 0b0000) {
    super(MqttConst.CONNACK, flags);
  }
}

module.exports = Connack;
