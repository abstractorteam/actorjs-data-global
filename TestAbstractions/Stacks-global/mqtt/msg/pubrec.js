
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Pubrec extends MqttMsg {
  constructor(flags = 0b0000) {
    super(MqttConst.PUBREC, flags);
  }
}

module.exports = Pubrec;
