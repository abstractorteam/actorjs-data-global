
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Unsuback extends MqttMsg {
  constructor(flags = 0b0000) {
    super(MqttConst.UNSUBACK, flags);
  }
}

module.exports = Unsuback;
