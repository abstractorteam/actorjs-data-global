
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Publish extends MqttMsg {
  constructor(flags = 0b0000) {
    super(MqttConst.PUBLISH, flags);
  }
}

module.exports = Publish;
