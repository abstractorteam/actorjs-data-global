
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Subscribe extends MqttMsg {
  constructor(flags = 0b0010) {
    super(MqttConst.SUBSCRIBE, flags);
  }
}

module.exports = Subscribe;
