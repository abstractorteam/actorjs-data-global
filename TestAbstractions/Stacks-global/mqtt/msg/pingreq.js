
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Pingreq extends MqttMsg {
  constructor(flags = 0b0000) {
    super(MqttConst.PINGREQ, flags);
  }
}

module.exports = Pingreq;
