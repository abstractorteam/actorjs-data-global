
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Pingresp extends MqttMsg {
  constructor(flags = 0b0000) {
    super(MqttConst.PINGRESP, flags);
  }
}

module.exports = Pingresp;
