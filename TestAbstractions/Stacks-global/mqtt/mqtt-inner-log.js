
'use strict';

const StackApi = require('stack-api');
const MqttConst = require('./mqtt-const');


class MqttInnerLog {
  static createFixedHeader(msg) {
    const fixedHeader = new StackApi.LogInner('Fixed Header', [], true);
    
    const controlPackageType = [];
    controlPackageType.push(new StackApi.LogPartText('[4bit]'));
    controlPackageType.push(new StackApi.LogPartRef('Type', ` ${MqttConst.DOCUMENTATION_LINK_ROOT}${MqttConst.FIXED_HEADER_TYPE}`));
    controlPackageType.push(new StackApi.LogPartText(`: ${msg.controlPackageType}`));
    fixedHeader.add(new StackApi.LogInner(controlPackageType));
    
    const flags = [];
    const flag = new StackApi.LogInner(flags);
    flags.push(new StackApi.LogPartText('[4bit]'));
    flags.push(new StackApi.LogPartRef('Flags', ` ${MqttConst.DOCUMENTATION_LINK_ROOT}${MqttConst.FIXED_HEADER_FLAGS}`));
    flags.push(new StackApi.LogPartText(`: ${msg.flags} = 0x${msg.flags} = 0b${msg.flags.toString(2).padStart(4, '0')}`));
    fixedHeader.add(flag);
    
    const remainingLength = [];
    remainingLength.push(new StackApi.LogPartRef('Remaining Length', `${MqttConst.DOCUMENTATION_LINK_ROOT}${MqttConst.FIXED_HEADER_REMAINING_LENGTH}`));
    remainingLength.push(new StackApi.LogPartText(`: ${msg.remainingLength}`));
    fixedHeader.add(new StackApi.LogInner(remainingLength));
    
    return fixedHeader;
  }
  
  static createCaption(msg) {
    return `Connect`;
  }
}

module.exports = MqttInnerLog;
