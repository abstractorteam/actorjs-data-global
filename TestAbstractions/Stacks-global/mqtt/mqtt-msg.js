
'use strict';


class MqttMsg {
  constructor(controlPackageType, flags, remainingLength) {
    this.controlPackageType = controlPackageType;
    this.flags = flags;
    this.remainingLength = remainingLength;
  }
  
  _getBit(num, bit) {
    return (num >> bit) % 2 ? 1 : 0;
  }
}

module.exports = MqttMsg;
