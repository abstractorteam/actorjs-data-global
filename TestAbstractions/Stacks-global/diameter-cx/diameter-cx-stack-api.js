
'use strict';

const AllowedWafWwsfIdentities = require('./avp/allowed-waf-wwsf-identities');
const AssociatedIdentities = require('./avp/associated-identities');
const AssociatedRegisteredIdentities = require('./avp/associated-registered-identities');
const CallIdSipHeader = require('./avp/call-id-sip-header');
const ChargingInformation = require('./avp/charging-information');
const ConfidentialityKey = require('./avp/confidentiality-key');
const Contact = require('./avp/contact');
const DeregistrationReason = require('./avp/deregistration-reason');
const DigestAlgorithm = require('./avp/digest-algorithm');
const DigestHa1 = require('./avp/digest-ha1');
const DigestQop = require('./avp/digest-qop');
const DigestRealm = require('./avp/digest-realm');
const Drmp = require('./avp/drmp');
const FeatureListId = require('./avp/feature-list-id');
const FeatureList = require('./avp/feature-list');
const FromSipHeader = require('./avp/from-sip-header');
const IdentityWithEmergencyRegistration = require('./avp/identity-with-emergency-registration');
const InitialCseqSequenceNumber = require('./avp/initial-cseq-sequence-number');
const IntegrityKey = require('./avp/integrity-key');
const LiaFlags = require('./avp/lia-flags');
const Load = require('./avp/load');
const LooseRouteIndication = require('./avp/loose-route-indication');
const MandatoryCapability = require('./avp/mandatory-capability');
const MultipleRegistrationIndication = require('./avp/multiple-registration-indication');
const OcOlr = require('./avp/oc-olr');
const OcSupportedFeatures = require('./avp/oc-supported-features');
const OptionalCapability = require('./avp/optional-capability');
const OriginatingRequest = require('./avp/originating-request');
const PCscfSubscriptionInfo = require('./avp/p-cscf-subscription-info');
const Path = require('./avp/path');
const PrimaryChargingCollectionFunctionName = require('./avp/primary-charging-collection-function-name');
const PrimaryEventChargingFunctionName = require('./avp/primary-event-charging-function-name');
const PriviledgedSenderIndication = require('./avp/priviledged-sender-indication');
const PublicIdentity = require('./avp/public-identity');
const ReasonCode = require('./avp/reason-code');
const ReasonInfo = require('./avp/reason-info');
const RecordRoute = require('./avp/record-route');
const RestorationInfo = require('./avp/restoration-info');
const RtrFlags = require('./avp/rtr-flags');
const SarFlags = require('./avp/sar-flags');
const ScscfRestorationInfo = require('./avp/scscf-restoration-info');
const ServerCapabilities = require('./avp/server-capabilities');
const ServerName = require('./avp/server-name');
const SessionPriority = require('./avp/session-priority');
const SipAuthDataItem = require('./avp/sip-auth-data-item');
const SipAuthenticate = require('./avp/sip-authenticate');
const SipAuthenticationContext = require('./avp/sip-authentication-context');
const SipAuthenticationScheme = require('./avp/sip-authentication-scheme');
const SipAuthorization = require('./avp/sip-authorization');
const SipDigestAuthenticate = require('./avp/sip-digest-authenticate');
const SipItemNumber = require('./avp/sip-item-number');
const SipNumberAuthItems = require('./avp/sip-number-auth-items');
const SubscriptionInfo = require('./avp/subscription-info');
const SupportedApplications = require('./avp/supported-applications');
const SupportedFeatures = require('./avp/supported-features');
const ToSipHeader = require('./avp/to-sip-header');
const UarFlags = require('./avp/uar-flags');
const UserAuthorizationType = require('./avp/user-authorization-type');
const UserDataAlreadyAvailable = require('./avp/user-data-already-available');
const UserData = require('./avp/user-data');
const VisitedNetworkIdentifier = require('./avp/visited-network-identifier');
const WebrtcAuthenticationFunctionName = require('./avp/webrtc-authentication-function-name');
const WebrtcWebServerFunctionName = require('./avp/webrtc-web-server-function-name');
const WildcardedPublicIdentity = require('./avp/wildcarded-public-identity');

const LocationInfoAnswer = require('./msg/location-info-answer');
const LocationInfoRequest = require('./msg/location-info-request');
const MultimediaAuthAnswer = require('./msg/multimedia-auth-answer');
const MultimediaAuthRequest = require('./msg/multimedia-auth-request');
const PushProfileAnswer = require('./msg/push-profile-answer');
const PushProfileRequest = require('./msg/push-profile-request');
const RegistrationTerminationAnswer = require('./msg/registration-termination-answer');
const RegistrationTerminationRequest = require('./msg/registration-termination-request');
const ServerAssignmentAnswer = require('./msg/server-assignment-answer');
const ServerAssignmentRequest = require('./msg/server-assignment-request');
const UserAuthorizationAnswer = require('./msg/user-authorization-answer');
const UserAuthorizationRequest = require('./msg/user-authorization-request');

const DiameterCxStyle = require('./diameter-cx-style');


const exportsObject = {
  avp: {
   AllowedWafWwsfIdentities: AllowedWafWwsfIdentities,
   AssociatedIdentities: AssociatedIdentities,
   AssociatedRegisteredIdentities: AssociatedRegisteredIdentities,
   CallIdSipHeader: CallIdSipHeader,
   ChargingInformation: ChargingInformation,
   ConfidentialityKey: ConfidentialityKey,
   Contact: Contact,
   DeregistrationReason: DeregistrationReason,
   DigestAlgorithm: DigestAlgorithm,
   DigestHa1: DigestHa1,
   DigestQop: DigestQop,
   DigestRealm: DigestRealm,
   Drmp: Drmp,
   FeatureListId: FeatureListId,
   FeatureList: FeatureList,
   FromSipHeader: FromSipHeader,
   IdentityWithEmergencyRegistration: IdentityWithEmergencyRegistration,
   InitialCseqSequenceNumber: InitialCseqSequenceNumber,
   IntegrityKey: IntegrityKey,
   LiaFlags: LiaFlags,
   Load: Load,
   LooseRouteIndication: LooseRouteIndication,
   MandatoryCapability: MandatoryCapability,
   MultipleRegistrationIndication: MultipleRegistrationIndication,
   OcOlr: OcOlr,
   OcSupportedFeatures: OcSupportedFeatures,
   OptionalCapability: OptionalCapability,
   OriginatingRequest: OriginatingRequest,
   PCscfSubscriptionInfo: PCscfSubscriptionInfo,
   Path: Path,
   PrimaryChargingCollectionFunctionName: PrimaryChargingCollectionFunctionName,
   PrimaryEventChargingFunctionName: PrimaryEventChargingFunctionName,
   PriviledgedSenderIndication: PriviledgedSenderIndication,
   PublicIdentity: PublicIdentity,
   ReasonCode: ReasonCode,
   ReasonInfo: ReasonInfo,
   RecordRoute: RecordRoute,
   RestorationInfo: RestorationInfo,
   RtrFlags: RtrFlags,
   SarFlags: SarFlags,
   ScscfRestorationInfo: ScscfRestorationInfo,
   ServerCapabilities: ServerCapabilities,
   ServerName: ServerName,
   SessionPriority: SessionPriority,
   SipAuthDataItem: SipAuthDataItem,
   SipAuthenticate: SipAuthenticate,
   SipAuthenticationContext: SipAuthenticationContext,
   SipAuthenticationScheme: SipAuthenticationScheme,
   SipAuthorization: SipAuthorization,
   SipDigestAuthenticate: SipDigestAuthenticate,
   SipItemNumber: SipItemNumber,
   SipNumberAuthItems: SipNumberAuthItems,
   SubscriptionInfo: SubscriptionInfo,
   SupportedApplications: SupportedApplications,
   SupportedFeatures: SupportedFeatures,
   ToSipHeader:ToSipHeader,
   UarFlags:UarFlags,
   UserAuthorizationType: UserAuthorizationType,
   UserDataAlreadyAvailable:UserDataAlreadyAvailable,
   UserData:UserData,
   VisitedNetworkIdentifier:VisitedNetworkIdentifier,
   WebrtcAuthenticationFunctionName: WebrtcAuthenticationFunctionName,
   WebrtcWebServerFunctionName: WebrtcWebServerFunctionName,
   WildcardedPublicIdentity: WildcardedPublicIdentity
  },
  msg: {
    LocationInfoAnswer: LocationInfoAnswer,
    LocationInfoRequest: LocationInfoRequest,
    MultimediaAuthAnswer: MultimediaAuthAnswer,
    MultimediaAuthRequest: MultimediaAuthRequest,
    PushProfileAnswer: PushProfileAnswer,
    PushProfileRequest: PushProfileRequest,
    RegistrationTerminationAnswer: RegistrationTerminationAnswer,
    RegistrationTerminationRequest: RegistrationTerminationRequest,
    ServerAssignmentAnswer: ServerAssignmentAnswer,
    ServerAssignmentRequest: ServerAssignmentRequest,
    UserAuthorizationAnswer: UserAuthorizationAnswer,
    UserAuthorizationRequest: UserAuthorizationRequest
  },
  Style: DiameterCxStyle
};

module.exports = exportsObject;
