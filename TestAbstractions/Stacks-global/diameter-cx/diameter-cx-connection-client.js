
'use strict';

const StackApi = require('stack-api');
const DiameterApi = require('diameter-stack-api');
const DiameterCxConnectionClientOptions = require('./diameter-cx-connection-client-options');
const DiameterCxConstCommandCode = require('./diameter-cx-const-command-code');
const DiameterCxAvpFactory = require('./diameter-cx-avp-factory');


class DiameterCxConnectionClient extends StackApi.ClientConnection {
  constructor(id, actor, connectionOptions, name = 'diameter-cx') {
    super(id, actor, name, StackApi.NetworkType.TCP, connectionOptions, DiameterCxConnectionClientOptions);
  }
  
  send(msg) {
    this.sendMessage(new DiameterApi.DiameterEncoder(msg, DiameterApi.DiameterEncoder.SEND_ALL, DiameterCxConstCommandCode));
  }
  
  receive() {
    this.receiveMessage(new DiameterApi.DiameterDecoder(DiameterApi.DiameterDecoder.RECEIVE_ALL, {
      avpFactory: DiameterCxAvpFactory,
      commandCode: DiameterCxConstCommandCode
    }));
  }
}

module.exports = DiameterCxConnectionClient;
