
'use strict';

const DiameterApi = require('diameter-stack-api');


class SipAuthenticationContext extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SipAuthenticationContext.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SipAuthenticationContext.CODE = 611;


module.exports = SipAuthenticationContext;
