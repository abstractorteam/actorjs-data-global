
'use strict';

const DiameterApi = require('diameter-stack-api');


class LooseRouteIndication extends DiameterApi.avpDerived.Enumerated {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(LooseRouteIndication.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

LooseRouteIndication.CODE = 638;


module.exports = LooseRouteIndication;
