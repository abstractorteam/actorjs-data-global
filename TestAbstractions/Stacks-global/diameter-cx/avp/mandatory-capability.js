
'use strict';

const DiameterApi = require('diameter-stack-api');


class MandatoryCapability extends DiameterApi.avpBasic.Unsigned32 {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(MandatoryCapability.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

MandatoryCapability.CODE = 604;


module.exports = MandatoryCapability;
