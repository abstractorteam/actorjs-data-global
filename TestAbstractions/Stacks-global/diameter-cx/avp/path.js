
'use strict';

const DiameterApi = require('diameter-stack-api');


class Path extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(Path.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

Path.CODE = 640;


module.exports = Path;
