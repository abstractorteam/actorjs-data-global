
'use strict';

const DiameterApi = require('diameter-stack-api');


class SupportedApplications extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SupportedApplications.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SupportedApplications.CODE = 631;


module.exports = SupportedApplications;
