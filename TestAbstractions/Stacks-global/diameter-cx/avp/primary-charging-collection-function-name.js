
'use strict';

const DiameterApi = require('diameter-stack-api');


class PrimaryChargingCollectionFunctionName extends DiameterApi.avpDerived.DiameterURI {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(PrimaryChargingCollectionFunctionName.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

PrimaryChargingCollectionFunctionName.CODE = 621;


module.exports = PrimaryChargingCollectionFunctionName;
