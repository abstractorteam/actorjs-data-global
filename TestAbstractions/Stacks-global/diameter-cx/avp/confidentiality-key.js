
'use strict';

const DiameterApi = require('diameter-stack-api');


class ConfidentialityKey extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(ConfidentialityKey.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

ConfidentialityKey.CODE = 625;


module.exports = ConfidentialityKey;
