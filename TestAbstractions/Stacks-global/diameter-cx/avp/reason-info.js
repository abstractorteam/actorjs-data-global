
'use strict';

const DiameterApi = require('diameter-stack-api');


class ReasonInfo extends DiameterApi.avpDerived.UTF8String {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(ReasonInfo.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

ReasonInfo.CODE = 617;


module.exports = ReasonInfo;
