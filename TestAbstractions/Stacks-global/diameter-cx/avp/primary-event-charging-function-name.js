
'use strict';

const DiameterApi = require('diameter-stack-api');


class PrimaryEventChargingFunctionName extends DiameterApi.avpDerived.DiameterURI {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(PrimaryEventChargingFunctionName.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

PrimaryEventChargingFunctionName.CODE = 619;


module.exports = PrimaryEventChargingFunctionName;
