
'use strict';

const DiameterApi = require('diameter-stack-api');


class FromSipHeader extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(FromSipHeader.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

FromSipHeader.CODE = 644;


module.exports = FromSipHeader;
