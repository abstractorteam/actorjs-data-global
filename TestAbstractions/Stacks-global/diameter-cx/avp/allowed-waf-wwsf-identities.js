
'use strict';

const DiameterApi = require('diameter-stack-api');


class AllowedWafWwsfIdentities extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(AllowedWafWwsfIdentities.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

AllowedWafWwsfIdentities.CODE = 656;


module.exports = AllowedWafWwsfIdentities;
