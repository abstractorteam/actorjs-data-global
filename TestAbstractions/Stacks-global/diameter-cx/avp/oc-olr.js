
'use strict';

const DiameterApi = require('diameter-stack-api');


class OcOlr extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(OcOlr.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

OcOlr.CODE = 623;


module.exports = OcOlr;
