
'use strict';

const DiameterApi = require('diameter-stack-api');


class UarFlags extends DiameterApi.avpBasic.Unsigned32 {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(UarFlags.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

UarFlags.CODE = 637;


module.exports = UarFlags;
