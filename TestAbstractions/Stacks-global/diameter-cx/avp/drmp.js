
'use strict';

const DiameterApi = require('diameter-stack-api');


class Drmp extends DiameterApi.avpDerived.Enumerated {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(Drmp.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

Drmp.CODE = 301;


module.exports = Drmp;
