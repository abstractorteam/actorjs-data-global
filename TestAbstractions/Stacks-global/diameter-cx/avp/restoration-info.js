
'use strict';

const DiameterApi = require('diameter-stack-api');


class RestorationInfo extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(RestorationInfo.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

RestorationInfo.CODE = 649;


module.exports = RestorationInfo;
