
'use strict';

const DiameterApi = require('diameter-stack-api');


class Load extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(Load.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

Load.CODE = 0; // TODO: Do not know the command code.


module.exports = Load;
