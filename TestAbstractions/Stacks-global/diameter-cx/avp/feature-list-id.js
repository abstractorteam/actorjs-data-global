
'use strict';

const DiameterApi = require('diameter-stack-api');


class FeatureListId extends DiameterApi.avpBasic.Unsigned32 {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(FeatureListId.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

FeatureListId.CODE = 629;


module.exports = FeatureListId;
