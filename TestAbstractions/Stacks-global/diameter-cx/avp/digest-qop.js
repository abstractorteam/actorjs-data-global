
'use strict';

const DiameterApi = require('diameter-stack-api');


class DigestQop extends DiameterApi.avpDerived.UTF8String {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(DigestQop.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

DigestQop.CODE = 110;


module.exports = DigestQop;
