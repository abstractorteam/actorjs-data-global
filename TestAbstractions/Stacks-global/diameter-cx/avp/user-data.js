
'use strict';

const DiameterApi = require('diameter-stack-api');


class UserData extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(UserData.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

UserData.CODE = 606;


module.exports = UserData;
