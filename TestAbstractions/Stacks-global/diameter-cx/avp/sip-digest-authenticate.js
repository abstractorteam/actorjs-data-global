
'use strict';

const DiameterApi = require('diameter-stack-api');


class SipDigestAuthenticate extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SipDigestAuthenticate.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SipDigestAuthenticate.CODE = 635;


module.exports = SipDigestAuthenticate;
