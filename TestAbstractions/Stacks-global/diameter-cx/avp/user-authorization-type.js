
'use strict';

const DiameterApi = require('diameter-stack-api');


class UserAuthorizationType extends DiameterApi.avpDerived.Enumerated {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(UserAuthorizationType.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

UserAuthorizationType.CODE = 623;


module.exports = UserAuthorizationType;
