
'use strict';

const DiameterApi = require('diameter-stack-api');


class SipNumberAuthItems extends DiameterApi.avpBasic.Unsigned32 {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SipNumberAuthItems.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SipNumberAuthItems.CODE = 607;


module.exports = SipNumberAuthItems;
