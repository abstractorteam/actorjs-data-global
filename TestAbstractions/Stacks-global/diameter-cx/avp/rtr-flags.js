
'use strict';

const DiameterApi = require('diameter-stack-api');


class RtrFlags extends DiameterApi.avpBasic.Unsigned32 {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(RtrFlags.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

RtrFlags.CODE = 659;


module.exports = RtrFlags;
