
'use strict';

const DiameterApi = require('diameter-stack-api');


class Contact extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(Contact.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

Contact.CODE = 641;


module.exports = Contact;
