
'use strict';

const DiameterApi = require('diameter-stack-api');


class DeregistrationReason extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(DeregistrationReason.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

DeregistrationReason.CODE = 615;


module.exports = DeregistrationReason;
