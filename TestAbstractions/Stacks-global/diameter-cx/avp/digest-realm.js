
'use strict';

const DiameterApi = require('diameter-stack-api');


class DigestRealm extends DiameterApi.avpDerived.UTF8String {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(DigestRealm.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

DigestRealm.CODE = 104;


module.exports = DigestRealm;
