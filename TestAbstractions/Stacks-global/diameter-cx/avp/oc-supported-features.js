
'use strict';

const DiameterApi = require('diameter-stack-api');


class OcSupportedFeatures extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(OcSupportedFeatures.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

OcSupportedFeatures.CODE = 621;


module.exports = OcSupportedFeatures;
