
'use strict';

const DiameterApi = require('diameter-stack-api');


class PCscfSubscriptionInfo extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(PCscfSubscriptionInfo.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

PCscfSubscriptionInfo.CODE = 660;


module.exports = PCscfSubscriptionInfo;
