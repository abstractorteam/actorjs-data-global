
'use strict';

const DiameterApi = require('diameter-stack-api');


class InitialCseqSequenceNumber extends DiameterApi.avpBasic.Unsigned32 {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(InitialCseqSequenceNumber.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

InitialCseqSequenceNumber.CODE = 654;


module.exports = InitialCseqSequenceNumber;
