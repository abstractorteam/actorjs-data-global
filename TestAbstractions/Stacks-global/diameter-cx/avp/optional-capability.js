
'use strict';

const DiameterApi = require('diameter-stack-api');


class OptionalCapability extends DiameterApi.avpBasic.Unsigned32 {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(OptionalCapability.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

OptionalCapability.CODE = 605;


module.exports = OptionalCapability;
