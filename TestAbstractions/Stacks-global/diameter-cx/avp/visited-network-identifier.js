
'use strict';

const DiameterApi = require('diameter-stack-api');


class VisitedNetworkIdentifier extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(VisitedNetworkIdentifier.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

VisitedNetworkIdentifier.CODE = 600;


module.exports = VisitedNetworkIdentifier;
