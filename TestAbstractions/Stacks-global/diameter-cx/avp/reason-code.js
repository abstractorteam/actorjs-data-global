
'use strict';

const DiameterApi = require('diameter-stack-api');


class ReasonCode extends DiameterApi.avpDerived.Enumerated {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(ReasonCode.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

ReasonCode.CODE = 616;


module.exports = ReasonCode;
