
'use strict';

const DiameterApi = require('diameter-stack-api');


class FeatureList extends DiameterApi.avpBasic.Unsigned32 {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(FeatureList.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

FeatureList.CODE = 630;


module.exports = FeatureList;
