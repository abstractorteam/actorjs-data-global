
'use strict';

const DiameterApi = require('diameter-stack-api');


class OriginatingRequest extends DiameterApi.avpDerived.Enumerated {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(OriginatingRequest.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

OriginatingRequest.CODE = 633;


module.exports = OriginatingRequest;
