
'use strict';

const DiameterApi = require('diameter-stack-api');


class WildcardedPublicIdentity extends DiameterApi.avpDerived.UTF8String {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(WildcardedPublicIdentity.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

WildcardedPublicIdentity.CODE = 634;


module.exports = WildcardedPublicIdentity;
