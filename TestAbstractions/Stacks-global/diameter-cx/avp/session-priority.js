
'use strict';

const DiameterApi = require('diameter-stack-api');


class SessionPriority extends DiameterApi.avpDerived.Enumerated {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SessionPriority.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SessionPriority.CODE = 650;


module.exports = SessionPriority;
