
'use strict';

const DiameterApi = require('diameter-stack-api');


class IdentityWithEmergencyRegistration extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(IdentityWithEmergencyRegistration.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

IdentityWithEmergencyRegistration.CODE = 651;


module.exports = IdentityWithEmergencyRegistration;
