
'use strict';

const DiameterApi = require('diameter-stack-api');


class SipAuthorization extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SipAuthorization.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SipAuthorization.CODE = 648;


module.exports = SipAuthorization;
