
'use strict';

const DiameterApi = require('diameter-stack-api');


class SecondaryChargingCollectionFunctionName extends DiameterApi.avpDerived.DiameterURI {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SecondaryChargingCollectionFunctionName.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SecondaryChargingCollectionFunctionName.CODE = 622;


module.exports = SecondaryChargingCollectionFunctionName;
