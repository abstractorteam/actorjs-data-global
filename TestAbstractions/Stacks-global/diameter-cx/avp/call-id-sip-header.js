
'use strict';

const DiameterApi = require('diameter-stack-api');


class CallIdSipHeader extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(CallIdSipHeader.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

CallIdSipHeader.CODE = 643;


module.exports = CallIdSipHeader;
