
'use strict';

const DiameterApi = require('diameter-stack-api');


class IntegrityKey extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(IntegrityKey.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

IntegrityKey.CODE = 651;


module.exports = IntegrityKey;
