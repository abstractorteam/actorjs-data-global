
'use strict';

const DiameterApi = require('diameter-stack-api');


class PublicIdentity extends DiameterApi.avpDerived.UTF8String {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(PublicIdentity.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

PublicIdentity.CODE = 601;


module.exports = PublicIdentity;
