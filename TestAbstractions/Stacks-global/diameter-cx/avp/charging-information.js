
'use strict';

const DiameterApi = require('diameter-stack-api');


class ChargingInformation extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(ChargingInformation.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

ChargingInformation.CODE = 618;


module.exports = ChargingInformation;
