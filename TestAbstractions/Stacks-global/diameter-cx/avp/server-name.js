
'use strict';

const DiameterApi = require('diameter-stack-api');


class ServerName extends DiameterApi.avpDerived.UTF8String {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(ServerName.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

ServerName.CODE = 602;


module.exports = ServerName;
