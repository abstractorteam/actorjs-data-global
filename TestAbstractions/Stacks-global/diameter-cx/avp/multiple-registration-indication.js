
'use strict';

const DiameterApi = require('diameter-stack-api');


class MultipleRegistrationIndication extends DiameterApi.avpDerived.Enumerated {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(MultipleRegistrationIndication.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

MultipleRegistrationIndication.CODE = 648;


module.exports = MultipleRegistrationIndication;
