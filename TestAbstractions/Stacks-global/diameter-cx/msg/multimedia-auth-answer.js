
'use strict';

const DiameterApi = require('diameter-stack-api');
const DiameterCxConstCommandCode = require('../diameter-cx-const-command-code');


class MultimediaAuthAnswer extends DiameterApi.DiameterMsg {
  constructor(applicationID, version = 1, commandFlags = 0b00000000, messageLength, hopByHopIdentifier, endToEndIdentifier) {
    super(DiameterCxConstCommandCode.MULTIMEDIA_AUTH, applicationID, version, commandFlags, messageLength, hopByHopIdentifier, endToEndIdentifier);
  }
}


module.exports = MultimediaAuthAnswer;
