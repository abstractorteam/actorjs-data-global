
'use strict';

const PuppeteerHttpParser = require('./puppeteer-http-parser');
const PuppeteerWebsocketParser = require('./puppeteer-websocket-parser');
const HttpConst = require('../http/http-const');
const HttpConstMethod = require('../http/http-const-method');
const HttpConstStatusCode = require('../http/http-const-status-code');
const HttpConstHeader = require('../http/http-const-header');


class PuppeteerSocketWorker {
  constructor(id) {
    this.id = id;
    this.connectionHandler = null;
    this.replaceHeaderNames = new Map([['Proxy-Connection', 'Connection']]);
    this.incomming = {
      socket: null,
      parser: new PuppeteerHttpParser(id, PuppeteerHttpParser.REQUEST, this.replaceHeaderNames),
      state: PuppeteerSocketWorker.STATUS_HEADERS
    };
    this.outgoing = {
      socket: null,
      parser: new PuppeteerHttpParser(id, PuppeteerHttpParser.RESPONSE),
      state: PuppeteerSocketWorker.STATUS_HEADERS
    };
    this.parseMethod = this._parseHttp.bind(this);
    this.Connected = false;
  }
  
  setConnectionHandler(connectionHandler) {
    this.connectionHandler = connectionHandler;
  }
  
  setIncommingSocket(socket) {
    this.incomming.socket = socket;
  }
  
  setOutgoingSocket(socket) {
    this.outgoing.socket = socket;
  }
  
  onConnecting(srcAddress, dstAddress) {
    process.nextTick(() =>  {
      this.connectionHandler.sendMessage('LogConnecting', [], srcAddress, dstAddress);
    });
  }
  
  onConnected(err, srcAddress, dstAddress) {
    process.nextTick(() =>  {
      this.connectionHandler.sendMessage('LogConnected', [], err, srcAddress, dstAddress);  
    });
  }
  
  onClosing() {
    process.nextTick(() =>  {
      this.connectionHandler.sendMessage('LogClosing', []);
    });
  }
  
  onClosed() {
    process.nextTick(() =>  {
      this.connectionHandler.sendMessage('LogClosed', []);
    });
  }
  
  onFirstIncommingData(chunk) {
    this.parseMethod(this.incomming, this.outgoing, chunk);
    return true;
  }
  
  onIncommingData(chunk) {
    this.parseMethod(this.incomming, this.outgoing, chunk);
  }
  
  onOutgoingData(chunk) {
    this.parseMethod(this.outgoing, this.incomming, chunk);
  }
  
  _writeHeaders(directionFrom, directionTo, parser) {
    const buffer = this._createHeader(parser);
    const sent = directionTo.socket.write(buffer, () => {
      if(!sent) {
        directionFrom.socket.resume();
      }
      process.nextTick(() =>  {
        if(PuppeteerHttpParser.REQUEST === parser.type) {
          this.connectionHandler.sendMessage('LogMessageSend', [], parser.message);
        }
        else {
          this.connectionHandler.sendMessage('LogMessageReceive', [], parser.message);
        }
      });
    });
    if(!sent) {
      directionFrom.socket.pause();
    }
  }
  
  _writeBodyBuffers(directionFrom, directionTo, rawBody) {
    const buffer = rawBody.shift();
    const sent = directionTo.socket.write(buffer, () => {
      if(!sent) {
        if(0 !== rawBody.length) {
          this._writeBodyBuffers(directionFrom, directionTo, rawBody);
        }
        else {
          directionFrom.socket.resume();
        }
      }
    });
    if(!sent) {
      directionFrom.socket.pause();
    }
    else {
      if(0 !== rawBody.length) {
        this._writeBodyBuffers(directionFrom, directionTo, rawBody);
      }
    }
  }
  
  _writeBody(directionFrom, directionTo, parser) {
    if(0 !== parser.rawBody.length) {
      const rowBody = parser.rawBody;
      parser.rawBody = [];
      this._writeBodyBuffers(directionFrom, directionTo, rowBody);
    }
  }
  
  _createHeader(parser) {
    let size = 0;
    const message = parser.message;
    if(PuppeteerHttpParser.REQUEST === parser.type) {
      size += message.method.length + message.requestTarget.length + message.httpVersion.length + 4;
    }
    else {
      size += message.httpVersion.length + message.statusCodeString.length + message.reasonPhrase.length + 4;
    }
    message.headers.forEach((values, name) => {
      const newName = parser.replaceHeaderNames.get(name);
      if(newName) {
        name = newName;
      }
      size += name.length + 2;
      values.forEach((value) => {
        size += value.length + 2;
      });
    });
    size += 2;
    const buffer = Buffer.allocUnsafeSlow(size);
    let pos = 0;
    if(PuppeteerHttpParser.REQUEST === parser.type) {
      pos += buffer.write(message.method, pos, message.method.length);
      pos += buffer.write(HttpConst.SP, pos, HttpConst.SP_LENGTH);
      pos += buffer.write(message.requestTarget, pos, message.requestTarget.length);
      pos += buffer.write(HttpConst.SP, pos, HttpConst.SP_LENGTH);
      pos += buffer.write(message.httpVersion, pos, message.httpVersion.length);
    }
    else {
      pos += buffer.write(message.httpVersion, pos, message.httpVersion.length);
      pos += buffer.write(HttpConst.SP, pos, HttpConst.SP_LENGTH);
      pos += buffer.write(message.statusCodeString, pos, message.statusCodeString.length);
      pos += buffer.write(HttpConst.SP, pos, HttpConst.SP_LENGTH);
      pos += buffer.write(message.reasonPhrase, pos, message.reasonPhrase.length);
    }
    pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    
    message.headers.forEach((values, name) => {
      const newName = parser.replaceHeaderNames.get(name);
      if(newName) {
        name = newName;
      }
      pos += buffer.write(name, pos, name.length);
      pos += buffer.write(HttpConst.COLON_SP, pos, HttpConst.COLON_SP_LENGTH);
      for(let i = 0; i < values.length - 1; ++i) {
        pos += buffer.write(values[i], pos, values[i].length);
        pos += buffer.write(HttpConst.COMMA_SP, pos, HttpConst.COMMA_SP_LENGTH);
      }
      pos += buffer.write(values[values.length - 1], pos, values[values.length - 1].length);
      pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    });
    pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    return buffer;
  } 
   
  _connectTunnelSockert(directionFrom, directionTo, ready, done) {
    const parser = directionFrom.parser;
    this.connectionHandler.resolveUrl(`http://${parser.message.requestTarget}`, (err, address) => {
      if(err) {
        done(err);
      }
      else {
        this.connectionHandler.connect(address, (err, outgoingSocket) => {
          if(err) {
            done(err);
          }
          else {
            this.Connected = true;
            this.setOutgoingSocket(outgoingSocket);
            done(err, outgoingSocket);
          }
        });
      }
    });
  }
  
  _connectProxySocket(directionFrom, directionTo, ready) {
    const parser = directionFrom.parser;
    this.connectionHandler.resolveUrl(parser.message.requestTarget, (err, address) => {
      if(err) {
        directionFrom.socket.write('HTTP/1.1 503 Service Unavailable\r\nProxy-agent: ActorJs-Proxy\r\n\r\n');
        directionFrom.socket.destroy();
        return;
      }
      this.connectionHandler.connect(address, (err, outgoingSocket) => {
        if(err) {
          directionFrom.socket.write('HTTP/1.1 503 Service Unavailable\r\nProxy-agent: ActorJs-Proxy\r\n\r\n');
          directionFrom.socket.destroy();
        }
        else {
          this.setOutgoingSocket(outgoingSocket);
          this._parseHttpFirstPart(directionFrom, directionTo, ready);
        }
      });
    });
  }
  
  _parseHttpFirstPart(directionFrom, directionTo, ready) {
    this._writeHeaders(directionFrom, directionTo, directionFrom.parser);
    this._writeBody(directionFrom, directionTo, directionFrom.parser);
    if(ready) {
      directionFrom.parser.ready();
    }
    else {
      directionFrom.state = PuppeteerSocketWorker.STATUS_BODY;
    }
  }
  
  _parseHttpConnected(directionFrom, directionTo, ready) {
    if(PuppeteerHttpParser.REQUEST === directionFrom.type) {
      this._parseHttpFirstPart(directionFrom, directionTo, ready);
    }
    else {
      const response = directionFrom.parser.message;
      if(HttpConstStatusCode.SwitchingProtocols === response.statusCode) {
        if('Upgrade' === response.getHeader(HttpConstHeader.CONNECTION) && 'websocket' === response.getHeader(HttpConstHeader.UPGRADE)) {
          this.parseMethod = this._parseWebSocket.bind(this);
          this._parseHttpFirstPart(directionFrom, directionTo, ready);
          this.outgoing.parser = new PuppeteerWebsocketParser(this.id, PuppeteerWebsocketParser.SERVER);
          this.incomming.parser = new PuppeteerWebsocketParser(this.id, PuppeteerWebsocketParser.CLIENT);
        }
      }
      else {
        this._parseHttpFirstPart(directionFrom, directionTo, ready);
      }
    }
  }
  
  _parseHttp(directionFrom, directionTo, chunk) {
    const parser = directionFrom.parser;
    const socketFrom = directionFrom.socket;
    const socketTo = directionTo.socket;
    const ready = parser.parse(chunk);
    if(PuppeteerSocketWorker.STATUS_HEADERS === directionFrom.state && parser.isHeadersReady()) {
      if(null === socketTo) {
        if(HttpConstMethod.CONNECT === parser.message.method) {
          if(!parser.message.requestTarget.endsWith('443')) {
            this._connectTunnelSockert(directionFrom, directionTo, ready, (err) => {
              if(err) {
                directionFrom.socket.write('HTTP/1.1 503 Service Unavailable\r\nProxy-agent: ActorJs-Proxy\r\n\r\n');
                directionFrom.socket.destroy();
              }
              else {
                socketFrom.write('HTTP/1.1 200 Ok\r\nProxy-agent: ActorJs-Proxy\r\n\r\n');
                parser.ready();
              }
            });
          }
          else {
            socketFrom.write('HTTP/1.1 405 Method Not Allowed\r\nProxy-agent: ActorJs-Proxy\r\n\r\n');
            directionFrom.socket.destroy();
          }
        }
        else {
          this._connectProxySocket(directionFrom, directionTo, ready);
        }
      }
      else { // CONNECTED
        if(!this.Connected) {
          this._parseHttpFirstPart(directionFrom, directionTo, ready);
        }
        else {
          this._parseHttpConnected(directionFrom, directionTo, ready);
        }
      }
    }
    else if(PuppeteerSocketWorker.STATUS_BODY === directionFrom.state) {
      this._writeBody(directionFrom, directionTo, parser);
      if(ready) {
        parser.ready();
        directionFrom.state = PuppeteerSocketWorker.STATUS_HEADERS;
      }
    }
  }
  
  _parseWebSocket(directionFrom, directionTo, chunk) {
    const ready = directionFrom.parser.parse(chunk);
    const sent = directionTo.socket.write(chunk, () => {
      if(!sent) {
        directionFrom.socket.resume();
      }
      process.nextTick(() =>  {
        if(ready) {
          if(PuppeteerWebsocketParser.CLIENT === directionFrom.parser.type) {
            this.connectionHandler.sendMessage('LogMessageSendWebSocket', [], directionFrom.parser.message);
          }
          else {
            this.connectionHandler.sendMessage('LogMessageReceiveWebSocket', [], directionFrom.parser.message);
          }
        }
      });
    });
    if(!sent) {
      directionFrom.socket.pause();
    }
  }
}

PuppeteerSocketWorker.STATUS_HEADERS = 0;
PuppeteerSocketWorker.STATUS_BODY = 1;
PuppeteerSocketWorker.STATUS = ['STATUS_HEADERS', 'STATUS_BODY'];

module.exports = PuppeteerSocketWorker;
