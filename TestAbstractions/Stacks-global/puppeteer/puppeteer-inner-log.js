
'use strict';

const HttpInnerLog = require('../http/http-inner-log');
const WebSocketInnerLog = require('../websocket/websocket-inner-log');
const WebsocketConst = require('../websocket/websocket-const');


class PuppeteerInnerLog {
  constructor(connection) {
    this.connection = connection;
    this.name = this.connection.name;
  }
  
  logConnecting(connectionData) {
    this.connection.logIp(() => this.connection.createIpMessage(`${this.connection.name}.connection [${connectionData.connectionDataLocal.getId()} ==> ${connectionData.connectionDataRemote.getId()}] CONNECTING`, undefined, {
      action: 'connecting',
      stack: this.name,
      local: connectionData.connectionDataLocal,
      remote: connectionData.connectionDataRemote
    }), 'b1bed13f-1469-4de5-ab3b-6d80696d4be3');
  }
  
  logConnected(connectionData) {
    this.connection.logIp(() => this.connection.createIpMessage(`${this.connection.name}.connection [${connectionData.connectionDataLocal.getId()} ==> ${connectionData.connectionDataRemote.getId()}] CONNECTED`, undefined, {
      action: 'connected',
      stack: this.name,
      local: connectionData.connectionDataLocal,
      remote: connectionData.connectionDataRemote
    }), '061b9b35-4087-44d5-bd01-6844b3431a9d');
  }
  
  logNotConnected(connectionData, err) {
    this.connection.logIp(() => this.connection.createIpMessage(`${this.connection.name}.connection [${connectionData.connectionDataLocal.getId()} ==> ${connectionData.connectionDataRemote.getId()}] CONNECTED`, undefined, {
      action: '!connected',
      stack: this.name,
      result: err,
      local: connectionData.connectionDataLocal,
      remote: connectionData.connectionDataRemote
    }), 'fcd47f83-f0de-45d8-9fbb-48951039f132');
  }
  
  logClosing(connectionData) {
    this.connection.logIp(() => (this.connection.createIpMessage(`${this.connection.name}.connection [${connectionData.connectionDataLocal.getId()} ==> ${connectionData.connectionDataRemote.getId()}] CLOSING`, undefined, {
      action: 'closing',
      stack: this.name,
      local: connectionData.connectionDataLocal,
      remote: connectionData.connectionDataRemote
    })), '6fe7c77f-59ae-4ca9-acac-a31ee0c034b1');
  }
  
  logClosed(connectionData) {
    this.connection.logIp(() => (this.connection.createIpMessage(`${this.connection.name}.connection [${connectionData.connectionDataLocal.getId()} ==> ${connectionData.connectionDataRemote.getId()}] CLOSED`, undefined, {
      action: 'closed',
      stack: this.name,
      local: connectionData.connectionDataLocal,
      remote: connectionData.connectionDataRemote
    })), 'bb4a0e61-07f5-494e-93ca-b65e056a3268');
  }
  
  logMessageSend(msg, connectionData) {
    if(this.connection.isLogIp()) {
      const ipLogs = [];
      let caption = 'UNKNOWN';
      const innerLogHttpRequest = HttpInnerLog.createRequestLine(msg);
      if(innerLogHttpRequest.logParts[0]) {
        const log = innerLogHttpRequest.logParts[0].text;
        caption = log.substring(0, log.lastIndexOf(' '));
      }
      ipLogs.push(innerLogHttpRequest);
      ipLogs.push(HttpInnerLog.createHeaders(msg));
      this.connection.logIp(() => (this.connection.createIpMessage(`${this.name}.connection [${connectionData.connectionDataLocal.getId()}] => [${connectionData.connectionDataRemote.getId()}] SEND MSG`, ipLogs, {
        action: 'send',
        caption: caption,
        stack: 'http',
        local: connectionData.connectionDataLocal,
        remote: connectionData.connectionDataRemote
      })), '15e6f51e-8dfb-4eb7-b926-56b42c82ff00');
    }
  }
  
  logMessageReceive(msg, connectionData) {
    if(this.connection.isLogIp()) {
      const ipLogs = [];
      let caption = 'UNKNOWN';
      const innerLogHttpResponse = HttpInnerLog.createStatusLine(msg);
      if(innerLogHttpResponse.logParts[0]) {
        const log = innerLogHttpResponse.logParts[0].text;
        caption = log.substring(log.indexOf(' ') + 1);
      }
      ipLogs.push(innerLogHttpResponse);
      ipLogs.push(HttpInnerLog.createHeaders(msg));
      this.connection.logIp(() => (this.connection.createIpMessage(`${this.name}.connection [${connectionData.connectionDataLocal.getId()}] >= [${connectionData.connectionDataRemote.getId()}] RECEIVE MSG`, ipLogs, {
        action: 'receive',
        caption: caption,
        stack: 'http',
        local: connectionData.connectionDataLocal,
        remote: connectionData.connectionDataRemote
      })), '7eaf6f99-bac6-4504-abce-a8db804b2df2');
    }
  }
  
  logMessageSendWebSocket(msg, connectionData) {
    if(this.connection.isLogIp()) {
      const ipLogs = [];
      let caption = WebsocketConst.OPCODES[msg.opCode];
      const webSocketInnerLog = WebSocketInnerLog.createHeader(msg);
      ipLogs.push(webSocketInnerLog);
      const webSocketPayload = WebSocketInnerLog.createPayload(msg);
      ipLogs.push(webSocketPayload);
   //   console.log('connection', Buffer.from(msg.dataPayload).toString());
      this.connection.logIp(() => (this.connection.createIpMessage(`${this.name}.connection [${connectionData.connectionDataLocal.getId()}] >= [${connectionData.connectionDataRemote.getId()}] RECEIVE MSG`, ipLogs, {
        action: 'send',
        caption: caption,
        stack: 'websocket',
        local: connectionData.connectionDataLocal,
        remote: connectionData.connectionDataRemote
      })), 'cdf8404c-548b-47e1-bc27-4c2495b9e0a4');
    }
  }
  
  logMessageReceiveWebSocket(msg, connectionData) {
    if(this.connection.isLogIp()) {
      const ipLogs = [];
      let caption = WebsocketConst.OPCODES[msg.opCode];
      const webSocketInnerLog = WebSocketInnerLog.createHeader(msg);
      ipLogs.push(webSocketInnerLog);
      const webSocketPayload = WebSocketInnerLog.createPayload(msg);
      ipLogs.push(webSocketPayload);
    //  console.log('logMessageReceiveWebSocket', Buffer.from(msg.dataPayload).toString());
      this.connection.logIp(() => (this.connection.createIpMessage(`${this.name}.connection [${connectionData.connectionDataLocal.getId()}] >= [${connectionData.connectionDataRemote.getId()}] RECEIVE MSG`, ipLogs, {
        action: 'receive',
        caption: caption,
        stack: 'websocket',
        local: connectionData.connectionDataLocal,
        remote: connectionData.connectionDataRemote
      })), 'f06adf40-33fd-4bf5-8852-3a1e7ee91128');
    }
  }
  
  
  /*logBrowser(log) {
    if(this.connection.isLogGui()) {
      this.connection.logGui(() => (this.connection.createGuiMessage(log, undefined, {
        action: 'browser_created',
        stack: this.name
      })), '51dbc112-012a-4eb5-9c1e-f4203fd9c248');
    }
  }*/
}


module.exports = PuppeteerInnerLog;
