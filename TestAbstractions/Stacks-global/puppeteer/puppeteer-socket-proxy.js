
'use strict';

const HttpInnerLog = require('../http/http-inner-log');
const SocketWorker = require('z-abs-corelayer-server/project/server/socket-worker/socket-worker');
const Net = require('net');


class PuppeteerSocketProxy {
  constructor(connection, puppeteerLog) {
    this.connection = connection;
    this.puppeteerLog = puppeteerLog;
    this.connectionId = 0;
    this.srcAddress = null;
    this.serverSocket = null;
    this.sockets = new Map();
    this.connections = new Map();
  }
  
  init(host, srcAddress, done) {
    this.srcAddress = srcAddress;
    let pendings = 2;
    let initErrors = [];
    this._resolveSrcAddress((err) => {
      if(err) {
        initErrors.push(err);
      }
      if(0 === --pendings) {
        done(0 !== initErrors.length ? initErrors : null, this.serverSocket.address().port);
      }
    });
    this.serverSocket = Net.createServer((incommingSocket) => {
      const connectionId = ++this.connectionId;
      const socketData = {
        incommingSocket: incommingSocket,
        socketWorker: null,
        outgoingSocket: null,
        connectionId: connectionId
      };
      this.connections.set(connectionId, {
        connectionId: connectionId,
        connectionDataLocal: null,
        connectionDataRemote: null
      });
      this.sockets.set(incommingSocket, socketData);
      incommingSocket.pause();
      socketData.socketWorker = new SocketWorker(require.resolve('./puppeteer-socket-worker'), (errors, port) => {
        if(errors) {
          // MAKE AN ANSWER TO PUPPETEER
          return;
        }
        const address = {
          host: 'localhost',
          port: port,
          localAddress: '127.0.0.1',
          localPort: 0
        };
        const outgoingSocket = Net.connect(address, () => {
          outgoingSocket.on('end', () => {
            if(null !== socketData.incommingSocket) {
              socketData.incommingSocket.end();
            }            
          });
          outgoingSocket.on('close', () => {
            if(null !== socketData.incommingSocket) {
              socketData.incommingSocket.destroy();
            }
            socketData.outgoingSocket = null;
          });
          outgoingSocket.on('error', (err) => {});
          socketData.outgoingSocket = outgoingSocket;
          if(null !== socketData.outgoingSocket && null !== socketData.incommingSocket) {
            socketData.outgoingSocket.pipe(socketData.incommingSocket);
            socketData.incommingSocket.pipe(socketData.outgoingSocket);
            incommingSocket.resume();
          }
        });
      });
      socketData.socketWorker.run(socketData.connectionId, 'localhost', this.srcAddress, this);
      incommingSocket.on('end', () => {
        if(null !== socketData.outgoingSocket) {
          socketData.outgoingSocket.end();
        }
      });
      incommingSocket.on('close', () => {
        if(null !== socketData.outgoingSocket) {
          socketData.outgoingSocket.destroy();
        }
        socketData.incommingSocket = null;
        this.connections.delete(incommingSocket.connectionId);
        this.sockets.delete(incommingSocket);
      });
      incommingSocket.on('error', (err) => {});
    });
    this.serverSocket.listen({
      host: 'localhost',
      port: 0
    }, (err) => {
      if(err) {
        initErrors.push(err);
        this.connection.logError(err, '18f996bb-6b1e-4413-8423-87bc9442088d');
      }
      done(err, this.serverSocket.address().port);
    });
  }
  
  close(done) {
    if(null !== this.serverSocket) {
      this.serverSocket.close(() => {
        this.serverSocket = null;
        done();
      });
      this.sockets.forEach((socketData, socket) => {
        if(null !== socketData.outgoingSocket) {
          socketData.outgoingSocket.destroy();
        }
        if(null !== socketData.incommingSocket) {
          socketData.incommingSocket.destroy();
        }
      });
    }
    else {
      done();
    }
  }
  
  _resolveSrcAddress(done) {
    this.srcAddress.resolve((err) => {
      if(err) {
        this.connection.logError(err, 'f99fb94e-e6ac-4f27-bea2-f6bbb19f0910');
      }
      done(err);
    });
  }
  
  onDnsGet(done, url) {
    this.connection.dnsUrlCache.get(url, (err, address) => {
      done(err, address);
    });
  }
  
  onLogConnecting(connectionId, srcAddress, dstAddress) {
    const connectionData = this.connections.get(connectionId);
    connectionData.connectionDataLocal = this.connection.createConnectionData(srcAddress);
    connectionData.connectionDataRemote = this.connection.createConnectionData(dstAddress);
    this.puppeteerLog.logConnecting(connectionData);
  }
  
  onLogConnected(connectionId, err, srcAddress, dstAddress) {
    const connectionData = this.connections.get(connectionId);
    connectionData.connectionDataLocal = this.connection.createConnectionData(srcAddress);
    if(!err) {
      this.puppeteerLog.logConnected(connectionData);
    }
    else {
      this.puppeteerLog.logNotConnected(connectionData, err);
    }
  }
  
  onLogClosing(connectionId) {
    const connectionData = this.connections.get(connectionId);
    this.puppeteerLog.logClosing(connectionData);
  }
  
  onLogClosed(connectionId) {
    const connectionData = this.connections.get(connectionId);
    this.puppeteerLog.logClosed(connectionData);
  }
  
  onLogMessageSend(connectionId, httpMessage) {
    const connectionData = this.connections.get(connectionId);
    this.puppeteerLog.logMessageSend(httpMessage, connectionData);
  }
  
  onLogMessageReceive(connectionId, httpMessage) {
    const connectionData = this.connections.get(connectionId);
    this.puppeteerLog.logMessageReceive(httpMessage, connectionData);
  }
  
  onLogMessageSendWebSocket(connectionId, weboscketMessage) {
    const connectionData = this.connections.get(connectionId);
    this.puppeteerLog.logMessageSendWebSocket(weboscketMessage, connectionData);
  }
  
  onLogMessageReceiveWebSocket(connectionId, weboscketMessage) {
    const connectionData = this.connections.get(connectionId);
    this.puppeteerLog.logMessageReceiveWebSocket(weboscketMessage, connectionData);
  }
}


module.exports = PuppeteerSocketProxy;
