
'use strict';

const PuppeteerStyle = require('./puppeteer-style');


const exportsObject = {
  Style: PuppeteerStyle
};


module.exports = exportsObject;
