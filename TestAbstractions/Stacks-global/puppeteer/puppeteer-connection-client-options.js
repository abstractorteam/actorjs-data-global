
'use strict';


class PuppeteerConnectionClientOptions {
  constructor() {
    this.launchOptions = undefined;
    this.proxy = false;
  }
  
  clone() {
    return new PuppeteerConnectionClientOptions();
  }
}

module.exports = new PuppeteerConnectionClientOptions();
