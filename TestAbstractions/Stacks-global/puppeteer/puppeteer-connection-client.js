
'use strict';

const StackApi = require('stack-api');
const Puppeteer = require('puppeteer');
const PuppeteerInnerLog = require('./puppeteer-inner-log');
const PuppeteerConnectionClientOptions = require('./puppeteer-connection-client-options');
//const PuppeteerProxy = require('./puppeteer-http-proxy');
//const PuppeteerProxy = require('./puppeteer-proxy');
const PuppeteerProxy = require('./puppeteer-socket-proxy');


class PuppeteerConnectionClient extends StackApi.WebConnection {
  constructor(id, actor, connectionOptions) {
    super(id, actor, 'puppeteer', connectionOptions, PuppeteerConnectionClientOptions);
    this.browser = null;
    this.browserAsync = null;
    this.browserName = '';
    this.browserContext = null;
    this.browserContextName = '';
    this.page = null;
    this.pageAsync = null;
    this.pageName = '';
    this.proxyArgsGenerated = false;
    this.puppeteerLog = new PuppeteerInnerLog(this);
  }
  
  onConnect(browserData, done) {
    if(null === this.browser) {
      this._getBrowser(browserData, done);
    }
    else if(null === this.browserContext && undefined !== browserData.srcAddress.incognitoBrowser) {
      this._getContext(browserData, done);
    }
    else if(null === this.page) {
      this._launchPage(browserData, done);
    }
  }
  
  _getBrowser(browserData, done) {
    const browser = PuppeteerConnectionClient.browsers.get(`${browserData.srcAddress.name}:${browserData.srcAddress.browser}`);
    if(undefined !== browser) {
      if(browser.ready) {
        this.logIp(() => `getBrowser => Browser['${browser.name}']`, '6d2ea64f-ac17-4150-8940-77958004fbc6');
        this.browser = browser.targetProxy;
        this.browserAsync = browser.target;
        this.browserName = browser.name;
        this._getContext(browserData, done);
      }
      else {
        browser.queue.push({
          browserData: browserData,
          done: done
        });
      }
    }
    else {
      this.logDebug(() => `... _getBrowser 5, ${browserData}`, 'Puppeteer', '7c8322d6-ebba-4ba0-b701-020b063ccd90');
      this._launchBrowser(browserData, done);
    }
  }
  
  _launchPuppeteer(browser, browserData, done) {
    const formattedArgs = this._arguments(this.connectionOptions.launchOptions);
    this.logGui(() => `Puppeteer.launch(${formattedArgs}) ${PuppeteerConnectionClient.CALL}`, 'Puppeteer', '9c99c773-6750-4ff1-a920-b7e1bddd3768');
    
    Puppeteer.launch(this.connectionOptions.launchOptions).then((newBrowser) => {
      this.logGui(() => `Puppeteer.launch(${formattedArgs}) ${PuppeteerConnectionClient.SUCCESS}`, 'Puppeteer', '6c023b85-981a-4253-a998-bb3e46885554');
      browser.ready = true;
      browser.target = newBrowser;
      browser.targetProxy = this._createNewObject(newBrowser, `('${browser.name}')`);
      this.browser = browser.targetProxy;
      this.browserAsync = browser.target;
      this.browserName = browser.name;
      PuppeteerConnectionClient.browsers.set(`${browserData.srcAddress.name}:${browserData.srcAddress.browser}`, browser);
      this._getContext(browserData, done);
      browser.queue.forEach((launch) => {
        launch.done(false);
      });
      browser.queue = [];
    }, (reason) => {
      this.logGui(() => `Puppeteer.launch(${formattedArgs}) ${PuppeteerConnectionClient.FAILURE}: ${reason}`, 'Puppeteer', '4d12c327-2c4b-48b7-83ce-fffc3d6d75b4');
      done(true, reason);
    });
  }
  
  _launchBrowser(browserData, done) {
    const proxy = new PuppeteerProxy(this, this.puppeteerLog);
    const browser = {
      ready: false,
      target: null,
      targetProxy: null,
      name: `${browserData.srcAddress.addressName}.${browserData.srcAddress.browser}`,
      queue: [],
      proxy: proxy
    };
    if(this.connectionOptions.proxy) {
      proxy.init('localhost', browserData.srcAddress, (err, port) => {
        if(err) {
          done(true, err);
        }
        else {
          if(undefined !== this.connectionOptions.launchOptions.args) {
           if(!this.proxyArgsGenerated) {
             // TODO: find out if they are already set?
           }
         }
          else {
            this.connectionOptions.launchOptions.args = [
              `--proxy-server=http://127.0.0.1:${port};ws://127.0.0.1:${port}`,
              '--proxy-bypass-list=<-loopback>'
            ];
            this.connectionOptions.launchOptions.defaultViewport = {
              width: 900,
              height: 900
            };
          }
          this._launchPuppeteer(browser, browserData, done);
        }
      });
    }
    else {
      this._launchPuppeteer(browser, browserData, done);
    }
  }
  
  _getContext(browserData, done) {
    if(browserData.srcAddress.incognitoBrowser) {
      const context = PuppeteerConnectionClient.incognitoBrowserContexts.get(browserData.srcAddress.incognitoBrowser);
      if(undefined !== context) {
        if(context.ready) {
          this.browserContext = context.target;
          this._launchPage(browserData, done);
        }
        else {
          context.queue.push({
            browserData: browserData,
            done: done
          });
        }
      }
      else {
        this._launchContext(browserData, done);
      }
    }
    else {
      this._launchPage(browserData, done);
    }
  }
  
  _launchContext(browserData, done) {
    const context = {
      ready: false,
      target: null,
      queue: []
    };
    PuppeteerConnectionClient.incognitoBrowserContexts.set(browserData.srcAddress.incognitoBrowser, context);
  }
  
  _arguments(args, first = true) {
    if(Array.isArray(args)) {
      const formattedArgs = [];
      args.forEach((arg) => {
        formattedArgs.push(this._arguments(arg, false));
      });
      if(first) {
        return formattedArgs.join(', ');
      }
      else {
        return ['[', formattedArgs.join(', '), ']'].join('');
      }
    }
    else {
      if(typeof args === 'string') {
        return `'${args}'`;
      }
      else if(typeof args === 'object') {
        const formattedArgs = [];
        for(let property in args) {
          formattedArgs.push(`${property}: ${args[property]}`);
        }
        return ['{', formattedArgs.join(', '), '}'].join('');
      }
      else {
        return `${args}`;
      }
    }
  }
  
  _createIncognitoBrowserContext(context) {
    const self = this;
    return new Proxy(context, {
      get: (target, property, receiver) => {
        return function (...args) {
          const formattedArgs = self._arguments(args);
          self.logIp(() => `BrowserContext['${self.browserContextName}'].${property}(${formattedArgs})`, '4411bebf-4c37-4ec1-a07f-fa56a4600e01');
          const result = target[property](...args);
          if(result instanceof Promise) {
            let cancel = false;
            const externalPendingId = self.actor.setPending(() => {
              self.logIp(() => `BrowserContext['${self.browserContextName}'].${property}(${formattedArgs}) --- cancel`, '98999d14-c6d9-425a-9dcc-0bba7053ba44');
              cancel = true;
            });
            result.then((value) => {
              if(!cancel) {
                self.logIp(() => `BrowserContext['${self.browserContextName}'].${property}(${formattedArgs}) --- done`, 'ddcd57a0-5a71-4eb4-a823-03f249629b35');
                if('newPage' === property) {
                  self.actor.pendingSuccess(externalPendingId, self._createNewObject(value));
                }
                else {
                  self.cbActorCallbacks.cbDone(externalPendingId, value);
                }
              }
            }, (reason) => {
              if(!cancel) {
                self.logIp(() => `BrowserContext['${self.browserContextName}'].${property}(${formattedArgs}) --- error: ${reason}`, '666af965-9130-49c3-a365-5f70a6c1debd');
                self.actor.pendingError(externalPendingId, reason);
              }
            });
          }
          else {
            return result;
          }
        };
      }
    });
  }
  
  async _launchPage(browserData, done) {
    this.logDebug(() => `... _launchPage, ${browserData}`, 'Puppeteer', 'dc721c4d-9ddd-41b1-9afe-26d237821797');
    try {
      const newPage = await this.browserAsync.newPage();
      newPage.setDefaultTimeout(0);
      /*newPage.on('console', (consoleMessage) => {
      console.log(consoleMessage.text());
      });*/
      /*await newPage.setRequestInterception(true);
      newPage.on('request', (request) => {
        console.log(request.method());
        console.log(request.headers());
        request.continue();
      });*/
      //newPage.on('response', (response) => {});
      this.page = this._createNewObject(newPage, `('${browserData.dstAddress.uri}')`);
      this.pageAsync = newPage;
      this.pageName = browserData.dstAddress.uri;
    }
    catch(e) {
      return done(true, e);
    }
    try {
      this.logGui(`Page['${browserData.dstAddress.uri}'].goto('${browserData.dstAddress.uri}') ${PuppeteerConnectionClient.CALL}`, 'Puppeteer', '982196d4-60eb-437d-a00f-532ecd6e62fa');
      await this.pageAsync.goto(browserData.dstAddress.uri, {
        waitUntil: [
          'domcontentloaded',
          'load',
        ]
      });
      PuppeteerConnectionClient.pages.push({
        name: `Page['${browserData.dstAddress.uri}']`,
        page: this.pageAsync
      });
      this.logGui(() => `Page['${browserData.dstAddress.uri}'].goto('${browserData.dstAddress.uri}') ${PuppeteerConnectionClient.SUCCESS}`, 'Puppeteer', 'a1455f35-40f6-4f57-8bd2-460796ace0d8');
    }
    catch(e) {
      this.logGui(() => `Page['${browserData.dstAddress.uri}'].goto('${browserData.dstAddress.uri}')${PuppeteerConnectionClient.FAILURE}: ${e}`, 'Puppeteer', 'efcbe242-f2c1-4943-bc03-698ebce0ff16');
      return done(true, e);
    }
    done(true);
  }
  
  _createNewObject(object, log) {
    const self = this;
    const objectName = object.constructor.name;
    switch(objectName) {
      case 'Browser': {
        if(null !== this.browserAsync) {
          throw new Error('Creating a new browser is not allowed. Create a new PuppeterConnection if you need a new browser.');
        }
        break;
      }
      case 'Page': {
        if(null !== this.pageAsync) {
          throw new Error('Creating a new page is not allowed. Create a new PuppeterConnection if you need a new page.');
        }
        break;
      }
      default: {
          log = `('${this.pageAsync.url()}')`;
        break;
      }
    }
    self.logGui(() => `new ${objectName}${log ? log : ''}`, objectName, 'ae546d6a-0921-466b-be65-ab209ec63b34', 1);
    return new Proxy(object, {
      get: (target, property, receiver) => {
        if('function' === typeof target[property]) {
          return function (...args) {
            const formattedArgs = self._arguments(args);
            self.logGui(() => `${objectName}['${self.pageAsync.url()}'].${property}(${formattedArgs}) ${PuppeteerConnectionClient.CALL}`, objectName, 'ae546d6a-0921-466b-be65-ab209ec63b34', 1);
            const result = target[property](...args);
            if(result instanceof Promise) {
              let cancel = false;
              const externalPendingId = self.actor.setPending(() => {
                self.logGui(() => `${objectName}['${self.pageAsync.url()}'].${property}(${formattedArgs}) ${PuppeteerConnectionClient.CANCEL}`, objectName, '24613f40-34e1-46d8-a904-b468aab20958', 1);
                cancel = true;
              });
              result.then((value) => {
                if(!cancel) {
                  self.logGui(() => `${objectName}['${self.pageAsync.url()}'].${property}(${formattedArgs}) ${PuppeteerConnectionClient.SUCCESS}`, objectName, '6860aee5-0f03-4d35-a42b-47f3f3e4bb0c', 1);
                  self.actor.pendingSuccess(externalPendingId, value);
                }
              }, (reason) => {
                if(!cancel) {
                  self.logGui(() => `${objectName}['${self.pageAsync.url()}'].${property}(${formattedArgs}) ${PuppeteerConnectionClient.FAILURE}: ${reason}`, objectName, '7b4e047d-d7f8-451e-b97b-78007bc9095b', 1);
                  self.actor.pendingError(externalPendingId, reason);
                }
              });
            }
            else {
              return result;
            }
          };
        }
        else if('object' === typeof target[property]) {
          return self._createNewObject(target[property]);
        }
        else {
          return target[property];
        }
      }
    });
  }
  
  async clear(done) {
    const pagePromises = [];
    PuppeteerConnectionClient.pages.forEach((page) => {
      if(!page.page.isClosed()) {
        this.logGui(() => `'${page.name}'].close()`, 'Puppeteer', 'f228dbed-8ae1-498e-9521-eccd2a150b3e', 1);
        pagePromises.push(page.page.close());
      }
    });
    try {
      await Promise.all(pagePromises);
    }
    catch(e) {
      console.log('EEEE Close - pages', e);
    }
    const incognitoBrowserContextPromises = [];
    PuppeteerConnectionClient.incognitoBrowserContexts.forEach((incognitoBrowserContext) => {
      incognitoBrowserContextPromises.push(incognitoBrowserContext.target.close());
    });
    try {
      await Promise.all(incognitoBrowserContextPromises);
    }
    catch(e) {
      console.log('EEEE Close - incognito', e);
    }
    const browserPromises = [];
    PuppeteerConnectionClient.browsers.forEach((browser) => {
      this.logGui(() => `Browser['${browser.name}'].close`, 'Puppeteer', '67bcf70e-b031-494f-be7d-e50582198983', 1);
      browserPromises.push(browser.target.close());
      browserPromises.push(new Promise((resolve, reject) => {
        this.logGui(() => `Browser.proxy['${browser.name}'].close ${PuppeteerConnectionClient.CALL}`, 'Puppeteer', 'c06a6e3d-b875-4259-af53-3f07975c1c84', 1);
        browser.proxy.close((err) => {
          if(err) {
            this.logGui(() => `Browser.proxy['${browser.name}'].close ${PuppeteerConnectionClient.FAILURE}: ${err}`, 'Puppeteer', '0b866189-83d2-41cc-ba50-d00ad5267805', 1);
            reject(err);
          }
          else {
            this.logGui(() => `Browser.proxy['${browser.name}'].close ${PuppeteerConnectionClient.SUCCESS}`, 'Puppeteer', '1161c7ec-71a0-4566-96ae-2e3ea5f7277f', 1);
            resolve();
          }
        });
      }));
    });
    try {
      await Promise.all(browserPromises);
    }
    catch(e) {
      console.log('EEEE Close - browser.target', e);
    }
    this.logGui('All Browsers[].close --- done', 'Puppeteer', 'f291131a-aea7-463b-b96a-518a754f10a1', 1);
    
    PuppeteerConnectionClient.browsers.clear();
    PuppeteerConnectionClient.incognitoBrowserContexts.clear();
    PuppeteerConnectionClient.pages = [];
    done();
  }
}


PuppeteerConnectionClient.browsers = new Map();
PuppeteerConnectionClient.incognitoBrowserContexts = new Map();
PuppeteerConnectionClient.pages = [];

PuppeteerConnectionClient.CALL = ' -- call';
PuppeteerConnectionClient.SUCCESS = ' -- success';
PuppeteerConnectionClient.FAILURE = ' -- failure';
PuppeteerConnectionClient.CANCEL =  ' -- cancel';


module.exports = PuppeteerConnectionClient;
