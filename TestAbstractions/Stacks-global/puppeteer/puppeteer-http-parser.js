
'use strict';

const StackApi = require('stack-api');
const HttpConst = require('../http/http-const');
const HttpConstHeader = require('../http/http-const-header'); 
const HttpMsgRequest = require('../http/http-msg-request');
const HttpMsgResponse = require('../http/http-msg-response');


class PuppeteerHttpParser {
  constructor(id, type, replaceHeaderNames
               = new Map()) {
    this.id = id;
    this.type = type;
    this.replaceHeaderNames = replaceHeaderNames;
    this.bufferManager = new StackApi.BufferManager(this.id, true);
    this.state = PuppeteerHttpParser.FIRST_LINE;
    this.size = 0;
    this.sizeLeft = 0;
    this.chunks = null;
    this.message = null;
    this.rawBody = [];
  }
  
  parse(chunk) {
    this.bufferManager.receiveBuffer(chunk);
    switch(this.state) {
      case PuppeteerHttpParser.FIRST_LINE: {
        if(!this.bufferManager.findLine()) {
          break;
        }
        else {
          this.size = 0;
          this.sizeLeft = 0;
          this.rawBody = [];
          this.chunks = null;
          const line = this.bufferManager.getLine();
          if(PuppeteerHttpParser.REQUEST === this.type) {
            this.message = new HttpMsgRequest();
            this.message.addRequestLine(...line.split(' '));
          }
          else {
            this.message = new HttpMsgResponse();
            this.message.addStatusLine(...line.split(' '));
          }
          this.state = PuppeteerHttpParser.HEADERS;
        }
      }
      case PuppeteerHttpParser.HEADERS: {
        let allHeaders = false;
        while(this.bufferManager.findLine()) {
          const line = this.bufferManager.getLine();
          if(0 === line.length) {
            this.state = PuppeteerHttpParser.BODY;
            allHeaders = true;
            break;
          }
          const delimiter = line.indexOf(':');
          if(-1 === delimiter) {
            console.log('parse Error:', delimiter);
          }
          this.message.addHeader(line.substring(0, delimiter), line.substring(delimiter + 1).trimStart());
        }
        if(!allHeaders) {
          break;
        }
      }
      case PuppeteerHttpParser.BODY: {
        const size = this.message.getHeader(HttpConstHeader.CONTENT_LENGTH);
        if(size) {
          this.size = this.sizeLeft = Number.parseInt(size);
          this.state = PuppeteerHttpParser.BODY_CONTENT_LENGTH;
        }
        else {
          const transferEncoding = this.message.getHeader(HttpConstHeader.TRANSFER_ENCODING);
          if(transferEncoding) {
            this.state = PuppeteerHttpParser.BODY_TRANSFER_ENCODING;
            return this._parseTransferEncoding(chunk);
          }
          else {
            this.state = PuppeteerHttpParser.DONE;
            return true;
          }
        }
      }
      case PuppeteerHttpParser.BODY_CONTENT_LENGTH: {
        if(0 === this.size) {
          this.state = PuppeteerHttpParser.DONE;
          return true;
        }
        const buffer = this.bufferManager.receiveSize(this.size);
        if(undefined !== buffer) {
          this.sizeLeft -= buffer.length;
          this.rawBody.push(buffer);
          if(0 === this.sizeLeft) {
            this.state = PuppeteerHttpParser.DONE;
            return true;
          }
        }
        break;
      }
      case PuppeteerHttpParser.BODY_TRANSFER_ENCODING: {
        return this._parseTransferEncoding(chunk);
      }
      default: {
        
      }
    }
    return false;
  }
  
  _parseTransferEncoding(chunk) {
    if(null === this.chunks) {
      this.chunks = {
        size: 0,
        chunkLines: [],
        chunks: [],
        trailers: []
      };
    }
    
    // 1) Get Data
    while(true) {
      if(0 === this.chunks.size) {
        if(!this.bufferManager.findLine()) {
          return false;
        }
        const line = this.bufferManager.getLine();
        this.chunks.chunkLines.push(line);
        const chunkLineIndex = line.indexOf(HttpConst.COMMA_SP);
        if(-1 === chunkLineIndex) {
          this.chunks.size = Number.parseInt(line, 16);
        }
        else {
          this.chunks.size = Number.parseInt(line.substring(0, chunkLineIndex), 16);
        }
      }
      if(0 !== this.chunks.size) {
        if(Number.isNaN(this.chunks.size)) {
          return true;
        }
        const buffer = this.bufferManager.receiveSize(this.chunks.size);
        if(undefined !== buffer) {
          this.chunks.chunks.push(buffer);
          this.chunks.size = 0;
          if(!this.bufferManager.findLine()) {
            return false;
          }
          const line = this.bufferManager.getLine();
        }
        else {
          return false;
        }
      }
      else {
        break;
      }
    }
    
    // 2) Get Trailers
    while(true) {
      if(!this.bufferManager.findLine()) {
        return false;
      }
      const line = this.bufferManager.getLine();
      if(0 === line.length) {
        break;
      }
      this.chunks.trailers.push(line);
    }
    
    // 3) Calculate size
    let size = 0;
    this.chunks.chunkLines.forEach((line) => {
      size += line.length + 2;
    });
    this.chunks.chunks.forEach((chunk) => {
      size += chunk.length + 2;
    });
    this.chunks.trailers.forEach((trailer) => {
      size += trailer.length + 2;
    });
    size += 2;
    
    // 4) Render chunk
    const buffer = Buffer.allocUnsafeSlow(size);
    let pos = 0;
    for(let i = 0; i < this.chunks.chunkLines.length - 1; ++i) {
      const line = this.chunks.chunkLines[i];
      pos += buffer.write(line, pos, line.length);
      pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
      pos += this.chunks.chunks[i].copy(buffer, pos);
      pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    }
    const line = this.chunks.chunkLines[this.chunks.chunkLines.length - 1];
    pos += buffer.write(line, pos, line.length);
    pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    this.chunks.trailers.forEach((trailer) => {
      pos += buffer.write(trailer, pos, trailer.length);
      pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    });
    pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    this.rawBody.push(buffer);
    
    this.state = PuppeteerHttpParser.DONE;
    return true;
  }
  
  ready() {
    this.state = PuppeteerHttpParser.FIRST_LINE;    
  }
  
  isHeadersReady() {
    return this.state > PuppeteerHttpParser.HEADERS;
  }
}

PuppeteerHttpParser.REQUEST = 0;
PuppeteerHttpParser.RESPONSE = 1;

PuppeteerHttpParser.FIRST_LINE = 0;
PuppeteerHttpParser.HEADERS = 1;
PuppeteerHttpParser.BODY = 2;
PuppeteerHttpParser.BODY_CONTENT_LENGTH = 3;
PuppeteerHttpParser.BODY_TRANSFER_ENCODING = 4;
PuppeteerHttpParser.DONE = 5;

PuppeteerHttpParser.STATUS = [
  'FIRST_LINE',
  'HEADERS',
  'BODY',
  'BODY_CONTENT_LENGTH',
  'BODY_TRANSFER_ENCODING',
  'DONE'
];

module.exports = PuppeteerHttpParser;
