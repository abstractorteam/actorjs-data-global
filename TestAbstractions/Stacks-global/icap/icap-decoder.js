
'use strict';

const StackApi = require('stack-api');
const IcapInnerLog = require('./icap-inner-log');


class IcapDecoder extends StackApi.Decoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }
}

module.exports = IcapDecoder;
