
'use strict';


class IcapStyle {
  constructor(index) {
    this.index = index;
    this.textColor= 'black';
    this.textProtocolColor = 'black';
    this.protocolColor = 'ForestGreen';
    this.protocolBackgroundColor = 'rgba(144, 238, 144, 0.3)';
  }
}


module.exports = IcapStyle;
