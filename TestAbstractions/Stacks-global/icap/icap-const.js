
'use strict';


class IcapConst {
  constructor() {
    
  }
}

IcapConst.MAX_CAPTION_SIZE = 30;
IcapConst.BREAK_CAPTION_SIZE = IcapConst.MAX_CAPTION_SIZE - 3;


module.exports = IcapConst;
