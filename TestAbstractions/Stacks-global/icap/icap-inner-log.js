
'use strict';

const StackApi = require('stack-api');
const IcapConst = require('./icap-const');


class IcapInnerLog {
  static generateLog(msg, ipLogs) {
    const readableMsg = `${StackApi.AsciiDictionary.getSymbolString(msg)}`;
    const innerLog = new StackApi.LogInner(`'${readableMsg}'`);
    ipLogs.push(innerLog);
    return readableMsg.length <= IcapConst.MAX_CAPTION_SIZE ? readableMsg : readableMsg.substring(0, IcapConst.BREAK_CAPTION_SIZE) + '...';
  }
}

module.exports = IcapInnerLog;
