
'use strict';

const StackApi = require('stack-api');
const IcapConnectionServerOptions = require('./icap-connection-server-options');
const IcapEncoder = require('./icap-encoder');
const IcapDecoder = require('./icap-decoder');


class IcapConnectionServer extends StackApi.ServerConnection {
  constructor(id, actor, connectionOptions, name = 'icap') {
    super(id, actor, name, StackApi.NetworkType.TCP, connectionOptions, IcapConnectionServerOptions);
  }

  receive(msg) {
    this.receiveMessage(new IcapDecoder(msg));
  }

  send(msg) {
    this.sendMessage(new IcapEncoder(msg));
  }
}

module.exports = IcapConnectionServer;
