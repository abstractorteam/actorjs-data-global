
'use strict';

const StackApi = require('stack-api');
const IcapConnectionClientOptions = require('./icap-connection-client-options');
const IcapEncoder = require('./icap-encoder');
const IcapDecoder = require('./icap-decoder');


class IcapConnectionClient extends StackApi.ClientConnection {
  constructor(id, actor, connectionOptions, name = 'icap') {
    super(id, actor, name, StackApi.NetworkType.TCP, connectionOptions, IcapConnectionClientOptions);
  }

  send(msg) {
    this.sendMessage(new IcapEncoder(msg));
  }
  
  receive(msg) {
    this.receiveMessage(new IcapDecoder(msg));
  }
}

module.exports = IcapConnectionClient;
